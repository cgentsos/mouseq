-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_nx_clkgen.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it generates the
-- 90 degree phase-shifted clocks for the slow (self-correcting) ECC
-- memory instantiation, for nanoXplore devices.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library nx;
use nx.nxpackage.all;

entity mouseq_nx_clkgen is
  port (
    clk_i   : in std_logic;             -- 25 MHz input clock
    clk_o   : out std_logic;            -- 25 MHz output clock
    clk90_o : out std_logic;            -- 25 MHz, 90deg-shifted output clock
    rst_o   : out std_logic
    );
end mouseq_nx_clkgen;

architecture behavioral of mouseq_nx_clkgen is

  signal vco : std_logic;
  signal sync_wfg : std_logic;

  signal rdy : std_logic;
  signal rst1 : std_logic;

  signal FBK : std_logic;

begin

  pll_inst : NX_PLL
    generic map(
      vco_range    => 0,                -- 200 to 425 MHz
      ref_div_on   => '0',              -- bypass :: %2
      fbk_div_on   => '0',              -- bypass :: %2

      -- External feedback is used
      ext_fbk_on   => '1',              -- 0: disabled - 1: enabled
      -- External feedback is not used
      fbk_intdiv   => 4,                -- freq. multiplier x2 (here we have x8)

      fbk_delay_on => '0',              -- 0: no delay - 1: delay
      fbk_delay    => 0,                -- 0 to 63

      clk_outdiv1  => 0,                -- 0 to 7 (%1 to %2^7) (here we have /2^3)
      clk_outdiv2  => 0,                -- 0 to 7 (%1 to %2^7) (here we have /2^3)
      clk_outdiv3  => 0                 -- 0 to 7 (%1 to %2^7)
      )
    port map(
      REF => clk_i,                     -- 25 MHz input clock
      FBK => fbk,
      VCO => vco,                       -- 100 MHz x 4(WFG_FBK) = 400 MHz
      D1  => open,
      D2  => open,
      D3  => open,
      OSC => open,

      RDY => rdy
      );

  wfg1 : NX_WFG
    generic map(
      wfg_edge    => '0',               -- 0: no invert / Rising
      mode        => '1',               -- 0: no pattern - 1: pattern
      pattern_end => 7,                 -- 0: to 15 (1 step to 16 steps)
      pattern     => "11110000"&"00000000", -- Divide by 8 for 200 MHz / 8 = 25 MHz on FBK and CLKOUT_25
      delay_on    => '0',               -- 0: no delay
      delay       => 0                  -- 0 to 63 (1 unit to 64 unit)
      )
    port map(
      RDY => open,
      SI  => sync_wfg,
      ZI  => vco,
      SO  => sync_wfg,
      ZO  => fbk
      );

  clk_o <= fbk;

  wfg2 : NX_WFG
    generic map(
      wfg_edge    => '0',               -- 0: no invert / Rising
      mode        => '1',               -- 0: no pattern - 1: pattern
      pattern_end => 7,                 -- 0: to 15 (1 step to 16 steps)
      pattern     => "00111100"&"00000000", -- pattern p0 ... p15    -- Divide by 16 for 400 MHz / 16 = 25 MHz on FBK and CLKOUT_25
      delay_on    => '0',               -- 0: no delay - 1: delay
      delay       => 0                  -- 0 to 63 (1 unit to 64 unit)
      )
    port map(
      RDY => rdy,
      SI => sync_wfg,
      ZI => vco,
      SO => open,
      ZO => clk90_o
      );

  rst_proc: process(fbk)
  begin
    if rising_edge(fbk) then
      rst1  <= not rdy;
      rst_o <= rst1;
    end if;
  end process;



end behavioral;
