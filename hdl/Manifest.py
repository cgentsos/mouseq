files = ['mouseq_alu.vhd',
         'mouseq_fsm.vhd',
         'mouseq_mem.mif',
         'mouseq_pkg_gen.vhd',
         'mouseq_progmem.vhd',
         'mouseq_simple_reg_file.vhd',
         'mouseq_sreg_progmem.vhd',
         'mouseq_sreg_reg_file.vhd',
         'mouseq_timer.vhd',
         'mouseq.vhd']

modules = {'git': ['git:https://ohwr.org/project/general-cores.git']}

fetchto = '../ip_cores'

if target == "nx":
    files += ['mouseq_nx_clkgen.vhd',
              'mouseq_nx_progmem.vhd']
else:
    files += ['mouseq_nx_progmem_fake.vhd']
