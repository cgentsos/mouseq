-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_sreg_progmem.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it implements the program
-- memory by taking the design principle of the register file and combining it
-- with TMR and scrubbing. Like the plain program memory implementation, the
-- scrubbing happens when an address is accessed, counting on the looping
-- nature of the monitoring sequence programs. For details of the shift
-- register-based implementation, see mouseq_sreg_reg_file.vhd.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_sreg_progmem is
  port (
    clk_i   : in  std_logic;
    rst_n_i : in  std_logic;

    start_i : in  std_logic;            -- start a transaction
    addr_i  : in  std_logic_vector(MEMABITS-1 downto 0);

    data_o  : out std_logic_vector(MEMWIDTH-1 downto 0);
    done_o  : out std_logic             -- transaction over
    );
end entity mouseq_sreg_progmem;

architecture behavioral of mouseq_sreg_progmem is

  type state_t is (IDLE, ROT_BEFORE, DO);

  signal state : state_t;

  signal latch_addr : unsigned(MEMABITS-1 downto 0);

  signal rot_count : unsigned(MEMABITS-1 downto 0);

  signal progmem1 : RamType := InitRamFromFile(PROG_FILENAME);
  signal progmem2 : RamType := InitRamFromFile(PROG_FILENAME);
  signal progmem3 : RamType := InitRamFromFile(PROG_FILENAME);

  signal progmem1_out : std_logic_vector(MEMWIDTH-1 downto 0);
  signal progmem2_out : std_logic_vector(MEMWIDTH-1 downto 0);
  signal progmem3_out : std_logic_vector(MEMWIDTH-1 downto 0);

  signal progmem_vote_out : std_logic_vector(MEMWIDTH-1 downto 0);

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  attribute syn_radhardlevel of progmem1: signal is "none";
  attribute syn_radhardlevel of progmem2: signal is "none";
  attribute syn_radhardlevel of progmem3: signal is "none";

  attribute syn_ramstyle : string;
  attribute syn_ramstyle of progmem1 : signal is "registers";
  attribute syn_ramstyle of progmem2 : signal is "registers";
  attribute syn_ramstyle of progmem3 : signal is "registers";

  attribute syn_preserve : boolean;
  attribute syn_preserve of progmem1 : signal is TMR;
  attribute syn_preserve of progmem2 : signal is TMR;
  attribute syn_preserve of progmem3 : signal is TMR;

  signal rf_rot : std_logic;

begin  -- architecture behavioral

  gen_tmr: if TMR generate
    process(clk_i)
    begin
      if rising_edge(clk_i) then
        if (FAMILY = "PROASIC3") and (rst_n_i = '0') then
          progmem1 <= InitRamFromFile(PROG_FILENAME);
          progmem2 <= InitRamFromFile(PROG_FILENAME);
          progmem3 <= InitRamFromFile(PROG_FILENAME);
        else
          if (rf_rot = '1') then
            progmem1(progmem1'high) <= progmem_vote_out;
            progmem2(progmem2'high) <= progmem_vote_out;
            progmem3(progmem3'high) <= progmem_vote_out;
            for i in 0 to progmem1'high-1 loop
              progmem1(i) <= progmem1(i+1);
              progmem2(i) <= progmem2(i+1);
              progmem3(i) <= progmem3(i+1);
            end loop;
          end if;
        end if;
      end if;
    end process;
    progmem1_out <= progmem1(0);
    progmem2_out <= progmem2(0);
    progmem3_out <= progmem3(0);

    -- generate the voters for the triplicated register file
    gen_voters: for I in 0 to MEMWIDTH-1 generate
      progmem_vote_out(I) <= vote(progmem1_out(I), progmem2_out(I), progmem3_out(I));
    end generate gen_voters;
  end generate gen_tmr;

  gen_notmr: if not TMR generate
    process(clk_i)
    begin
      if rising_edge(clk_i) then
        if (FAMILY = "PROASIC3") and (rst_n_i = '0') then
          progmem1 <= InitRamFromFile(PROG_FILENAME);
        else
          if (rf_rot = '1') then
            progmem1(progmem1'high) <= progmem_vote_out;
            for i in 0 to progmem1'high-1 loop
              progmem1(i) <= progmem1(i+1);
            end loop;
          end if;
        end if;
      end if;
    end process;
    progmem1_out <= progmem1(0);

    progmem_vote_out <= progmem1_out;
  end generate gen_notmr;

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        data_o <= (others => '0');
        done_o <= '0';
        latch_addr <= (others => '0');
        rf_rot <= '0';
        rot_count <= (others => '0');
        state <= IDLE;
      else
        case state is
          when IDLE =>
            done_o <= '0';

            if (start_i = '1') then
              latch_addr  <= unsigned(addr_i);

              state <= ROT_BEFORE;
            end if;

          when ROT_BEFORE =>
            if (rot_count /= latch_addr) then
              rf_rot <= '1';

              if (rot_count < MEMSIZE-1) then
                rot_count <= rot_count + 1;
              else
                rot_count <= (others => '0');
              end if;
              
            else
              rf_rot  <= '0';

              state   <= DO;
            end if;

          when DO =>
            rf_rot <= '0';

            data_o <= progmem_vote_out;

            done_o <= '1';

            state <= IDLE;

          when others =>
            state <= IDLE;

        end case;
      end if;
    end if;
  end process;

end architecture behavioral;
