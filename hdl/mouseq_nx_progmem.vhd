-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_nx_progmem.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it implements the program
-- memory as an slow (self-correcting) ECC memory instantiation, for
-- nanoXplore devices.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library nx;
use nx.nxpackage.all;

use work.mouseq_pkg.all;

entity mouseq_nx_progmem is
  port (
    clk_i : in std_logic;
    clk90_i : in std_logic;
    rst_n_i : in std_logic;

    start_i : in  std_logic;
    addr_i : in std_logic_vector(7 downto 0);

    data_o : out std_logic_vector(MEMWIDTH-1 downto 0);
    done_o : out std_logic;

    ecc_err_o  : out std_logic;
    ecc_cor_o  : out std_logic);
end entity mouseq_nx_progmem;

architecture behavioral of mouseq_nx_progmem is

  signal ecc_err_int : std_logic;
  signal ecc_cor_int : std_logic;

begin  -- architecture behavioral

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      done_o <= start_i;                -- reads always take one cycle

      ecc_err_o  <= ecc_err_int; -- the ECC signals may give spurious pulses,
      ecc_cor_o  <= ecc_cor_int; -- register before sending them out
    end if;
  end process;

  prog_mem_inst : NX_RAM
    generic map (
      std_mode => "SLOW_2kx18"
      , mem_ctxt => NX_MEM_INIT
      )
    port map (
      ACK  => clk_i
      , ACKC => clk_i
      , ACKD => clk90_i
      , ACKR => '0'
      , AI1  => '0'
      , AI2  => '0'
      , AI3  => '0'
      , AI4  => '0'
      , AI5  => '0'
      , AI6  => '0'
      , AI7  => '0'
      , AI8  => '0'
      , AI9  => '0'
      , AI10 => '0'
      , AI11 => '0'
      , AI12 => '0'
      , AI13 => '0'
      , AI14 => '0'
      , AI15 => '0'
      , AI16 => '0'
      , AI17 => '0'
      , AI18 => '0'
      , AI19 => '0'
      , AI20 => '0'
      , AI21 => '0'
      , AI22 => '0'
      , AI23 => '0'
      , AI24 => '0'
      , ACOR => ecc_cor_int
      , AERR => ecc_err_int
      , AO1  => data_o(0)
      , AO2  => data_o(1)
      , AO3  => data_o(2)
      , AO4  => data_o(3)
      , AO5  => data_o(4)
      , AO6  => data_o(5)
      , AO7  => data_o(6)
      , AO8  => data_o(7)
      , AO9  => data_o(8)
      , AO10 => data_o(9)
      , AO11 => data_o(10)
      , AO12 => data_o(11)
      , AO13 => data_o(12)
      , AO14 => data_o(13)
      , AO15 => data_o(14)
      , AO16 => data_o(15)
      , AO17 => open
      , AO18 => open
      , AO19 => open
      , AO20 => open
      , AO21 => open
      , AO22 => open
      , AO23 => open
      , AO24 => open
      , AA1  => addr_i(0)
      , AA2  => addr_i(1)
      , AA3  => addr_i(2)
      , AA4  => addr_i(3)
      , AA5  => addr_i(4)
      , AA6  => addr_i(5)
      , AA7  => addr_i(6)
      , AA8  => addr_i(7)
      , AA9  => '0'
      , AA10 => '0'
      , AA11 => '0'
      , AA12 => '0'
      , AA13 => '0'
      , AA14 => '0'
      , AA15 => '0'
      , AA16 => '0'
      , ACS  => '1'
      , AWE  => '0'
      , AR   => '0'
      , BCK  => clk_i
      , BCKC => clk_i
      , BCKD => clk90_i
      , BCKR => '0'
      , BI1  => '0'
      , BI2  => '0'
      , BI3  => '0'
      , BI4  => '0'
      , BI5  => '0'
      , BI6  => '0'
      , BI7  => '0'
      , BI8  => '0'
      , BI9  => '0'
      , BI10 => '0'
      , BI11 => '0'
      , BI12 => '0'
      , BI13 => '0'
      , BI14 => '0'
      , BI15 => '0'
      , BI16 => '0'
      , BI17 => '0'
      , BI18 => '0'
      , BI19 => '0'
      , BI20 => '0'
      , BI21 => '0'
      , BI22 => '0'
      , BI23 => '0'
      , BI24 => '0'
      , BCOR => open
      , BERR => open
      , BO1  => open
      , BO2  => open
      , BO3  => open
      , BO4  => open
      , BO5  => open
      , BO6  => open
      , BO7  => open
      , BO8  => open
      , BO9  => open
      , BO10 => open
      , BO11 => open
      , BO12 => open
      , BO13 => open
      , BO14 => open
      , BO15 => open
      , BO16 => open
      , BO17 => open
      , BO18 => open
      , BO19 => open
      , BO20 => open
      , BO21 => open
      , BO22 => open
      , BO23 => open
      , BO24 => open
      , BA1  => '0'
      , BA2  => '0'
      , BA3  => '0'
      , BA4  => '0'
      , BA5  => '0'
      , BA6  => '0'
      , BA7  => '0'
      , BA8  => '0'
      , BA9  => '0'
      , BA10 => '0'
      , BA11 => '0'
      , BA12 => '0'
      , BA13 => '0'
      , BA14 => '0'
      , BA15 => '0'
      , BA16 => '0'
      , BCS  => '0'
      , BWE  => '0'
      , BR   => '0'
      );

end architecture behavioral;
