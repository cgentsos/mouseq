-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_sreg_reg_file.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it implements a shift
-- register-based register file. A normal register file that allows random
-- access to all its registers needs some relatively complex decoding and mux
-- logic; since we don't need high performance in this applications, we can
-- use an alternative approach that takes up less resources.
--
-- Here a shift register is employed, of which we can only access (read or
-- write) one element; that, in combination with a "base pointer" counter, can
-- provide access to any element of the register file by shifting it the right
-- number of places. Sequential accesses take minimal performance penalty as
-- only one shift is required, with backward accesses representing the
-- worst-case scenario - which is still acceptable as the register file will
-- typically not be more than a few tens of words.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_sreg_reg_file is
  port (
    clk_i   : in  std_logic;
    rst_n_i : in  std_logic;

    start_i : in  std_logic;            -- start a transaction
    write_i : in  std_logic;            -- read or write?
    addr_i  : in  std_logic_vector(REGABITS-1 downto 0);
    data_i  : in  std_logic_vector(WORDWIDTH-1 downto 0);

    data_o : out std_logic_vector(WORDWIDTH-1 downto 0);
    err_o  : out std_logic;
    done_o : out std_logic             -- transaction over
    );
end entity mouseq_sreg_reg_file;

architecture behavioral of mouseq_sreg_reg_file is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  type state_t is (IDLE, ROT_BEFORE, DO, ROT_AFTER);

  signal state : state_t;

  signal latch_write : std_logic;
  signal latch_addr : unsigned(REGABITS-1 downto 0);
  signal latch_data : std_logic_vector(WORDWIDTH-1 downto 0);

  signal rot_count : unsigned(REGABITS-1 downto 0);

  signal reg_file : RegType;

  attribute syn_radhardlevel of reg_file: signal is "none";

  signal rf_rot : std_logic;
  signal rf_load : std_logic;
  signal rf_din : std_logic_vector(WORDWIDTH-1 downto 0);

begin  -- architecture behavioral

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        reg_file <= (others => (others => '0'));
      else
        if (rf_rot = '1') then
          reg_file(reg_file'high) <= reg_file(0);
          for i in 0 to reg_file'high-1 loop
            reg_file(i) <= reg_file(i+1);
          end loop;
        elsif (rf_load = '1') then
          reg_file(0) <= rf_din;
        end if;
      end if;
    end if;
  end process;

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        data_o <= (others => '0');
        done_o <= '0';
        err_o  <= '0';
        latch_write <= '0';
        latch_addr <= (others => '0');
        latch_data <= (others => '0');
        rf_rot <= '0';
        rf_load <= '0';
        rf_din <= (others => '0');
        rot_count <= (others => '0');
        state <= IDLE;
      else
        case state is
          when IDLE =>
            done_o <= '0';
            err_o  <= '0';

            if (start_i = '1') then
              if (unsigned(addr_i) < REGSIZE) then
                latch_write <= write_i;
                latch_addr  <= unsigned(addr_i);
                latch_data  <= data_i;

                state <= ROT_BEFORE;
              else
                done_o <= '1';
                err_o  <= '1';
              end if;
            end if;

          when ROT_BEFORE =>
            if (rot_count /= latch_addr) then
              rf_rot <= '1';

              if (rot_count < REGSIZE-1) then
                rot_count <= rot_count + 1;
              else
                rot_count <= (others => '0');
              end if;
              
            else
              rf_rot  <= '0';
              rf_load <= latch_write;
              rf_din  <= latch_data;

              state   <= DO;
            end if;

          when DO =>
            rf_load <= '0';

            rf_rot <= '0';

            data_o <= reg_file(0);

            done_o <= '1';

            latch_write <= '0';

            state <= IDLE;

          when others =>
            state <= IDLE;

        end case;
      end if;
    end if;
  end process;

end architecture behavioral;
