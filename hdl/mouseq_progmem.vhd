-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_progmem.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it implements the program
-- memory as a simple TMR'd register file with scrubbing. The scrubbing
-- happens when an address is accessed, and it uses the voter result of the
-- three values (from the TMR copies of the register file). This counts on the
-- looping nature of the monitoring sequence programs; initialization commands
-- we use once so we don't care if they get corrupted, the main loop gets
-- executed often enough such that it gets corrected periodically.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_progmem is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    start_i : in  std_logic;
    addr_i : in std_logic_vector(MEMABITS-1 downto 0);

    data_o : out std_logic_vector(MEMWIDTH-1 downto 0);
    done_o : out std_logic);
end entity mouseq_progmem;

architecture behavioral of mouseq_progmem is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  -- main program memory
  signal mem1   : RamType := InitRamFromFile(PROG_FILENAME);
  signal mem2   : RamType := InitRamFromFile(PROG_FILENAME);
  signal mem3   : RamType := InitRamFromFile(PROG_FILENAME);

  attribute syn_radhardlevel of mem1: signal is "none";
  attribute syn_radhardlevel of mem2: signal is "none";
  attribute syn_radhardlevel of mem3: signal is "none";

  attribute syn_ramstyle : string;
  attribute syn_ramstyle of mem1 : signal is "registers";
  attribute syn_ramstyle of mem2 : signal is "registers";
  attribute syn_ramstyle of mem3 : signal is "registers";

  attribute syn_preserve : boolean;
  attribute syn_preserve of mem1 : signal is TMR;
  attribute syn_preserve of mem2 : signal is TMR;
  attribute syn_preserve of mem3 : signal is TMR;

  signal addr_i_r : std_logic_vector(MEMABITS-1 downto 0);

  signal mem1_out : std_logic_vector(MEMWIDTH-1 downto 0) := InitRamFirstCellFromFile(PROG_FILENAME);
  signal mem2_out : std_logic_vector(MEMWIDTH-1 downto 0) := InitRamFirstCellFromFile(PROG_FILENAME);
  signal mem3_out : std_logic_vector(MEMWIDTH-1 downto 0) := InitRamFirstCellFromFile(PROG_FILENAME);
  signal data_o_int : std_logic_vector(MEMWIDTH-1 downto 0);

begin  -- architecture behavioral


  gen_tmr: if TMR generate
    process(clk_i)
    begin
      if rising_edge(clk_i) then
        if (FAMILY = "PROASIC3") and (rst_n_i = '0') then
          mem1_out <= InitRamFirstCellFromFile(PROG_FILENAME);
          mem2_out <= InitRamFirstCellFromFile(PROG_FILENAME);
          mem3_out <= InitRamFirstCellFromFile(PROG_FILENAME);

          mem1 <= InitRamFromFile(PROG_FILENAME);
          mem2 <= InitRamFromFile(PROG_FILENAME);
          mem3 <= InitRamFromFile(PROG_FILENAME);

          addr_i_r <= (others => '0');

          done_o <= '0';
        else
          -- the triplicated register file
          mem1_out <= mem1(to_integer(unsigned(addr_i)));
          mem2_out <= mem2(to_integer(unsigned(addr_i)));
          mem3_out <= mem3(to_integer(unsigned(addr_i)));

          -- write the voted results back to the register file
          addr_i_r <= addr_i;
          mem1(to_integer(unsigned(addr_i_r))) <= data_o_int;
          mem2(to_integer(unsigned(addr_i_r))) <= data_o_int;
          mem3(to_integer(unsigned(addr_i_r))) <= data_o_int;

          done_o <= start_i;                -- reads always take one cycle
        end if;
      end if;
    end process;

    -- generate the voters for the triplicated register file
    gen_voters: for I in 0 to MEMWIDTH-1 generate
      data_o_int(I) <= vote(mem1_out(I), mem2_out(I), mem3_out(I));
    end generate gen_voters;
  end generate gen_tmr;

  gen_notmr: if not TMR generate
    process(clk_i)
    begin
      if rising_edge(clk_i) then
        if (FAMILY = "PROASIC3") and (rst_n_i = '0') then
          mem1_out <= InitRamFirstCellFromFile(PROG_FILENAME);

          mem1 <= InitRamFromFile(PROG_FILENAME);

          addr_i_r <= (others => '0');

          done_o <= '0';
        else
          -- the triplicated register file
          mem1_out <= mem1(to_integer(unsigned(addr_i)));

          -- write the voted results back to the register file
          addr_i_r <= addr_i;

          done_o <= start_i;                -- reads always take one cycle
        end if;
      end if;
    end process;

    data_o_int <= mem1_out;
  end generate gen_notmr;




  -- output gets the voter results
  data_o <= data_o_int;
  

end architecture behavioral;
