-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_timer.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
-- This is a utility module for the mouseq IP core, it implements a timer that
-- can either count in microseconds or in milliseconds.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity mouseq_timer is
  generic (
    CLK_PERIOD : natural := 10_000;     -- clock period in ps
    DISABLE_MS : boolean := false;      -- disable ms counting
    TOTAL_COUNT_BITS : natural := 10    -- counter width
    );
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    start_i    : in std_logic;          -- start the timer
    count_ms_i : in std_logic;          -- count ms, not microseconds
    how_long_i : in std_logic_vector(TOTAL_COUNT_BITS-1 downto 0); -- us or ms to count

    done_o : out std_logic              -- the wait is over
  );
end entity mouseq_timer;

architecture behavioral of mouseq_timer is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  function necessary_bits (
    constant MS_DISABLED : boolean)
    return natural is
  begin  -- function necessary_bits
    if (MS_DISABLED) then
      return integer(ceil(log2(real(1_000_000 / CLK_PERIOD))));
    else
      return integer(ceil(log2(real(1_000_000_000 / CLK_PERIOD))));
    end if;
  end function necessary_bits;

  constant c_UNIT_COUNT_BITS : natural := necessary_bits(DISABLE_MS);

  constant c_US_MAX_COUNT : unsigned(c_UNIT_COUNT_BITS-1 downto 0) :=
    to_unsigned(1_000_000 / CLK_PERIOD - 2, c_UNIT_COUNT_BITS);
  constant c_MS_MAX_COUNT : unsigned(c_UNIT_COUNT_BITS-1 downto 0) :=
    to_unsigned(1_000_000_000 / CLK_PERIOD - 2, c_UNIT_COUNT_BITS);

  signal unit_count_start : std_logic := '0'; -- count towards one us or ms
  signal unit_counter: unsigned(c_UNIT_COUNT_BITS-1 downto 0) := (others => '0');
  signal unit_count_done : std_logic := '0';
  signal unit_counting_us : std_logic := '0';
  signal unit_counting_ms : std_logic := '0';

  type state_t is (IDLE, COUNTING);
  signal state: state_t := IDLE;

  signal total_counting_ms : std_logic := '0';
  signal total_counter: unsigned(TOTAL_COUNT_BITS-1 downto 0) := (others => '0');
  signal total_counter_goal: unsigned(TOTAL_COUNT_BITS-1 downto 0) := (others => '0');

begin  -- architecture behavioral

  unit_counter_proc: process (clk_i) is
  begin
    if rising_edge(clk_i) then          -- rising clock edge
      if (rst_n_i = '0') then           -- synchronous reset (active low)
        unit_counter     <= (others => '0');
        unit_count_done  <= '0';
        unit_counting_us <= '0';
        unit_counting_ms <= '0';
      else
        if ((unit_counting_us = '1' and unit_counter = c_US_MAX_COUNT) or
            (unit_counting_ms = '1' and unit_counter = c_MS_MAX_COUNT)) then
          -- reached the max count for a unit of time (us or ms)
          unit_counter     <= (others => '0');
          unit_count_done  <= '1';
          unit_counting_us <= '0';
          unit_counting_ms <= '0';
        elsif (unit_counting_us = '1' or unit_counting_ms = '1') then
          -- is counting something
          unit_counter     <= unit_counter + 1;
        elsif (unit_count_start = '1') then
          unit_counting_us <= not total_counting_ms;
          unit_counting_ms <= total_counting_ms;
          unit_counter     <= unit_counter + 1;
        else
          unit_count_done  <= '0';
        end if;
      end if;
    end if;
  end process unit_counter_proc;

  count_proc: process (clk_i) is
  begin
    if rising_edge(clk_i) then          -- rising clock edge
      if (rst_n_i = '0') then           -- synchronous reset (active low)
        state              <= IDLE;

        unit_count_start   <= '0';

        total_counting_ms  <= '0';
        total_counter      <= (others => '0');
        total_counter_goal <= (others => '0');

        done_o             <= '0';
      else
        case (state) is
          when IDLE =>
            done_o <= '0';

            if (start_i = '1') then
              state <= COUNTING;

              unit_count_start <= '1';

              total_counting_ms  <= count_ms_i;
              total_counter      <= to_unsigned(1, TOTAL_COUNT_BITS);
              total_counter_goal <= unsigned(how_long_i);
            end if;

          when COUNTING =>
            unit_count_start <= '0';

            if (total_counter = total_counter_goal and unit_count_done = '1') then
              state <= IDLE;

              total_counter <= (others => '0');

              done_o <= '1';
            elsif (unit_count_done = '1') then
              unit_count_start <= '1';

              total_counter <= total_counter + 1;

              done_o <= '0';
            end if;
        end case;
      end if;
    end if;
  end process count_proc;

end architecture behavioral;
