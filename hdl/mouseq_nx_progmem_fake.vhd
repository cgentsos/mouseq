-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_nx_progmem_fake.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This dummy entity has been added for families other than nanoXplore
-- that somehow need the mouseq_nx_progmem entity in the work library
-- to complete synthesis, even though it won't be used at all.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_nx_progmem is
  port (
    clk_i : in std_logic;
    clk90_i : in std_logic;
    rst_n_i : in std_logic;

    start_i : in  std_logic;
    addr_i : in std_logic_vector(7 downto 0);

    data_o : out std_logic_vector(MEMWIDTH-1 downto 0);
    done_o : out std_logic;

    ecc_err_o  : out std_logic;
    ecc_cor_o  : out std_logic);
end entity mouseq_nx_progmem;

architecture behavioral of mouseq_nx_progmem is

begin  -- architecture behavioral

  data_o    <= (others => '0');
  done_o    <= '0';
  ecc_err_o <= '0';
  ecc_cor_o <= '0';

end architecture behavioral;
