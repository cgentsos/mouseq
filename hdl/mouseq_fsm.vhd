-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_fsm.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is the main FSM for the mouseq IP core.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_fsm is
  port (
    clk_i : in std_logic;
    rst_i : in std_logic;

    -- watchdog reset start address
    start_at_wd_rst_addr_i : in std_logic;

    -- external trigger
    ext_trig_i : in std_logic;

    -- i2c signals
    i2c_done_i    : in std_logic;
    i2c_rxack_n_i : in std_logic;
    i2c_rxbyte_i  : in std_logic_vector(7 downto 0);
    i2c_en_o      : out std_logic;
    i2c_start_o   : out std_logic;
    i2c_stop_o    : out std_logic;
    i2c_rd_o      : out std_logic;
    i2c_wr_o      : out std_logic;
    i2c_txack_o   : out std_logic;
    i2c_txbyte_o  : out std_logic_vector(7 downto 0);

    -- program memory signals
    pmem_dout_i  : in std_logic_vector(MEMWIDTH-1 downto 0);
    pmem_dv_i    : in std_logic;
    pmem_fetch_o : out std_logic;
    pmem_addr_o  : out std_logic_vector(MEMABITS-1 downto 0);

    -- register file signals
    rf_dout_i  : in std_logic_vector(WORDWIDTH-1 downto 0);
    rf_done_i  : in std_logic;
    rf_start_o : out std_logic;
    rf_write_o : out std_logic;
    rf_addr_o  : out std_logic_vector(REGABITS-1 downto 0);
    rf_din_o   : out std_logic_vector(WORDWIDTH-1 downto 0);

    -- timer signals
    timer_done_i     : in std_logic;
    timer_start_o    : out std_logic;
    timer_count_ms_o : out std_logic;
    timer_how_long_o : out std_logic_vector(9 downto 0);

    -- ALU signals
    alu_result_i : in std_logic_vector(WORDWIDTH-1 downto 0);
    alu_iszero_i : in std_logic;
    alu_res_v_i  : in std_logic;
    alu_in1_o    : out std_logic_vector(WORDWIDTH-1 downto 0);
    alu_in2_o    : out std_logic_vector(WORDWIDTH-1 downto 0);
    alu_cmd_o    : out std_logic_vector(3 downto 0);
    alu_op_v_o   : out std_logic;

    -- state outputs
    state_is_same_o    : out std_logic;
    state_is_fetch_o   : out std_logic;
    state_is_wait_o    : out std_logic;
    state_is_startup_o : out std_logic;

    -- error outputs
    err_decode_o     : out std_logic;
    err_noack_o      : out std_logic;

    -- I/O
    regs_i : in std_logic_vector(IWORDS*WORDWIDTH-1 downto 0);
    regs_o : out std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
    syncio_o : out std_logic
    );
end mouseq_fsm;

architecture behavioral of mouseq_fsm is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  -- processor opcodes
  constant c_op_WRITE1    : std_logic_vector(3 downto 0) := "0010";
  constant c_op_WRITE2    : std_logic_vector(3 downto 0) := "0011";
  constant c_op_READ1     : std_logic_vector(3 downto 0) := "0100";
  constant c_op_READ2     : std_logic_vector(3 downto 0) := "0101";
  constant c_op_STOP      : std_logic_vector(3 downto 0) := "0110";
  constant c_op_ALU2      : std_logic_vector(3 downto 0) := "1000";
  constant c_op_GOTO      : std_logic_vector(3 downto 0) := "1001";
  constant c_op_WAIT      : std_logic_vector(3 downto 0) := "1010";
  constant c_op_WAIT_TRIG : std_logic_vector(3 downto 0) := "1011";
  constant c_op_MOV       : std_logic_vector(3 downto 0) := "1100";
  constant c_op_SYNCIO    : std_logic_vector(3 downto 0) := "1101";
  constant c_op_LOAD      : std_logic_vector(3 downto 0) := "1110";
  constant c_op_ALU1      : std_logic_vector(3 downto 0) := "1111";
  constant c_op_NOP       : std_logic_vector(3 downto 0) := "0000";

  -- FSM states (description in the code)
  type state_t is (
    IDLE,
    STARTUP,
    FETCH,
    FETCH_av,
    DECODE,
    FETCH_WRITE_dv,
    FETCH_READ_dv,
    EXEC_WRITE,
    EXEC_WRITE_ADDR,
    EXEC_WRITE_REG,
    EXEC_WRITE_DATA,
    EXEC_WRITE_CHECK_SIZE,
    EXEC_WRITE_WAIT_ADJ_UPD,
    EXEC_WRITE_FETCH_NEXT_DATA,
    EXEC_WRITE_WAIT_NEXT_DATA,
    EXEC_WRITE_SEND_STOP,
    EXEC_READ,
    EXEC_READ_PNT,
    EXEC_READ_PNT_WAIT,
    EXEC_READ_PNT_GO,
    EXEC_READ_REG,
    EXEC_READ_NOTFIRST,
    EXEC_READ_DATA,
    EXEC_READ_STORE_DATA,
    EXEC_READ_CHECK_SIZE,
    EXEC_READ_WAIT_ACK_DONE,
    EXEC_READ_WAIT_ADJ_UPD,
    EXEC_READ_SEND_STOP,
    EXEC_STOP,
    EXEC_STOP_2,
    EXEC_GOTO,
    EXEC_WAIT,
    EXEC_WAIT_2,
    EXEC_WAIT_TRIG,
    EXEC_MOV,
    EXEC_MOV_WAIT_R,
    EXEC_MOV_WAIT_W,
    FETCH_LOAD_av,
    EXEC_LOAD,
    EXEC_LOAD_WAIT,
    EXEC_LOAD_RP,
    EXEC_ALU2_LOAD,
    EXEC_ALU2_WAIT_R,
    EXEC_ALU1,
    EXEC_ALU1_WAIT_R,
    EXEC_ALU1_WAIT_RES,
    EXEC_ALU1_WAIT_W,
    EXEC_SYNCIO,
    EXEC_SYNCIO_READIN_WAIT,
    EXEC_SYNCIO_WRITEOUT,
    EXEC_SYNCIO_WRITEOUT_WAIT
    );

  signal state   : state_t;
  signal state_r : state_t;

  -- program counter
  signal pc : unsigned(MEMABITS-1 downto 0);

  -- function return pointer
  signal retpnt : unsigned(MEMABITS-1 downto 0);

  -- 18-bit command word
  signal cmd_word : std_logic_vector(MEMWIDTH-1 downto 0);

  -- second command word, for write and read operations
  signal cmd_word_2 : std_logic_vector(MEMWIDTH-1 downto 0);

  -- command opcode
  signal cmd_opcode : std_logic_vector(3 downto 0);

  -- various aliases for different command arguments
  signal cmd_local_reg_addr_or_literal : std_logic_vector(7 downto 0);
  signal cmd_local_reg_addr            : unsigned(REGABITS-1 downto 0);
  signal cmd_local_reg_addr_read       : unsigned(REGABITS-1 downto 0);
  signal cmd_reg_size                  : unsigned(3 downto 0);
  signal cmd_dev_addr                  : std_logic_vector(6 downto 0);
  signal cmd_reg_dev_addr              : std_logic_vector(6 downto 0);
  signal cmd_reg_addr                  : std_logic_vector(7 downto 0);
  signal cmd_dev_from_pointer          : std_logic;
  signal cmd_skip_cmd                  : std_logic;
  signal cmd_add_stop                  : std_logic;
  signal cmd_use_literal               : std_logic;
  signal cmd_goto_onjb0                : std_logic;
  signal cmd_goto_onjb1                : std_logic;
  signal cmd_alu_cmd                   : std_logic_vector(2 downto 0);
  signal cmd_loadiscall                : std_logic;
  signal cmd_gotoiscall                : std_logic;
  signal cmd_address                   : unsigned(MEMABITS-1 downto 0);
  signal cmd_time                      : std_logic_vector(9 downto 0);
  signal cmd_ms                        : std_logic;
  signal cmd_src_a                     : unsigned(REGABITS-1 downto 0);
  signal cmd_dst_a                     : unsigned(REGABITS-1 downto 0);
  signal cmd_literal                   : std_logic_vector(8 downto 0);

  -- holds data to write to the bus
  signal data_to_write : std_logic_vector(7 downto 0);

  -- multi-byte transaction counter
  signal size_counter : unsigned(3 downto 0);

  -- adjust for big- and little-endian variants
  signal size_counter_endianness_adj : unsigned(3 downto 0);

  -- command arguments adjusted for endianness for multi-byte transactions
  signal cmd_local_reg_addr_or_literal_adj           : std_logic_vector(REGABITS-1 downto 0);
  signal cmd_local_reg_addr_adj                      : unsigned(REGABITS-1 downto 0);
  signal cmd_local_reg_addr_or_literal_adj_untrimmed : std_logic_vector(7 downto 0);
  signal cmd_local_reg_addr_adj_untrimmed            : unsigned(REGABITS-1 downto 0);

  -- helper counter for the syncio operation
  signal syncio_counter : unsigned(REGABITS-1 downto 0);

  -- control conditional jumps
  signal jump_bit : std_logic;

begin

  pmem_addr_o <= std_logic_vector(pc);

  -- populate the aliases used in all the various commands
  cmd_opcode                    <= cmd_word(15 downto 12);
  cmd_local_reg_addr_or_literal <= cmd_word(12 downto 5);
  cmd_local_reg_addr_read       <= unsigned(cmd_word(7+REGABITS-1 downto 7));
  cmd_local_reg_addr            <= unsigned(cmd_word(6+REGABITS-1 downto 6));
  cmd_reg_size                  <= unsigned(cmd_word(3 downto 0));
  cmd_dev_addr                  <= cmd_word_2(15 downto 9);
  cmd_reg_addr                  <= cmd_word_2(8 downto 1);
  cmd_add_stop                  <= cmd_word_2(0);
  cmd_dev_from_pointer          <= cmd_word(5);
  cmd_skip_cmd                  <= cmd_word(4);
  cmd_use_literal               <= '1' when (cmd_reg_size = "1111") else '0';
  cmd_address                   <= unsigned(cmd_word(3+MEMABITS-1 downto 3));
  cmd_goto_onjb0                <= cmd_word(2);
  cmd_goto_onjb1                <= cmd_word(1);
  cmd_gotoiscall                <= cmd_word(0);
  cmd_alu_cmd                   <= cmd_word(5 downto 3);
  cmd_loadiscall                <= cmd_word(2);
  cmd_time                      <= cmd_word(11 downto 2);
  cmd_ms                        <= cmd_word(1);
  cmd_src_a                     <= unsigned(cmd_word(6+REGABITS-1 downto 6));
  cmd_dst_a                     <= unsigned(cmd_word(REGABITS-1 downto 0));
  cmd_literal                   <= cmd_word_2(15 downto 7);


  -- helper signals for big- and little-endianness variants
  gen_big_endian_adj: if BIG_ENDIAN generate
    size_counter_endianness_adj <= size_counter;
  end generate gen_big_endian_adj;

  gen_little_endian_adj: if not BIG_ENDIAN generate
    size_counter_endianness_adj <= cmd_reg_size - size_counter;
  end generate gen_little_endian_adj;


  -- some gymnastics for width matching
  process(clk_i) is
  begin
    if rising_edge(clk_i) then
      if rst_i = '1' then
        cmd_local_reg_addr_or_literal_adj_untrimmed <= (others => '0');
        cmd_local_reg_addr_adj_untrimmed <= (others => '0');
      else
        cmd_local_reg_addr_or_literal_adj_untrimmed <= std_logic_vector(unsigned(cmd_local_reg_addr_or_literal) + size_counter_endianness_adj);
        cmd_local_reg_addr_adj_untrimmed <= cmd_local_reg_addr_read + size_counter_endianness_adj;
      end if;
    end if;
  end process;
  cmd_local_reg_addr_or_literal_adj <= cmd_local_reg_addr_or_literal_adj_untrimmed(REGABITS-1 downto 0);
  cmd_local_reg_addr_adj <= cmd_local_reg_addr_adj_untrimmed(REGABITS-1 downto 0);


  -- register the FSM state
  state_reg_proc: process (clk_i) is
  begin  -- process state_reg_proc
    if rising_edge(clk_i) then        -- rising clock edge
      if rst_i = '1' then           -- synchronous reset (active high)
        state_r <= IDLE;
      else
        state_r <= state;
      end if;
    end if;
  end process state_reg_proc;


  -- export some states for the watchdog and startup jump
  state_is_same_o    <= '1' when (state = state_r)     else '0';
  state_is_fetch_o   <= '1' when (state = FETCH_av)    else '0';
  state_is_wait_o    <= '1' when (state = EXEC_WAIT_2) else '0';
  state_is_startup_o <= '1' when (state = STARTUP)     else '0';


  -- purpose: implement main FSM
  -- type   : sequential
  -- inputs : cmd_add_stop, cmd_address, cmd_dev_addr,
  --          cmd_dst_a, cmd_literal, cmd_local_reg_addr,
  --          cmd_local_reg_addr_adj, cmd_local_reg_addr_or_literal,
  --          cmd_local_reg_addr_or_literal_adj, cmd_opcode,
  --          cmd_reg_addr, cmd_reg_size, cmd_src_a, cmd_time,
  --          cmd_ms, cmd_use_literal, i2c_done_i, i2c_rxack_n_i,
  --          i2c_rxbyte_i, pmem_dout_i, pmem_dv_i, regs_i, rf_done_i,
  --          rf_dout_i, rst_i, timer_done_i, start_at_wd_rst_addr_i,
  --          cmd_dev_from_pointer, alu_result_i, alu_res_v_i,
  --          cmd_goto_onjb0, cmd_goto_onjb1, cmd_gotoiscall
  -- outputs: state, data_to_write, size_counter, timer_start_o,
  --          timer_count_ms_o, timer_how_long_o, i2c_en_o, i2c_start_o,
  --          i2c_stop_o, i2c_rd_o, i2c_wr_o, i2c_txack_o, i2c_txbyte_o,
  --          rf_start_o, rf_write_o, rf_addr_o, rf_din_o, syncio_counter,
  --          err_decode_o, err_noack_o, pc, retpnt, cmd_word, cmd_word_2,
  --          pmem_fetch_o, alu_in1_o, alu_in2_o, alu_cmd_o, alu_op_v_o,
  --          regs_o, cmd_reg_dev_addr, jump_bit
  main_proc_fsm: process (clk_i) is
  begin  -- process main_proc_fsm
    if rising_edge(clk_i) then          -- rising clock edge
      if rst_i = '1' then               -- synchronous reset (active high)
        state <= IDLE;

        data_to_write <= (others => '0');

        size_counter  <= (others => '0');

        timer_start_o    <= '0';
        timer_count_ms_o <= '0';
        timer_how_long_o <= (others => '0');

        i2c_en_o     <= '0';
        i2c_start_o  <= '0';
        i2c_stop_o   <= '0';
        i2c_rd_o     <= '0';
        i2c_wr_o     <= '0';
        i2c_txack_o  <= '0';
        i2c_txbyte_o <= (others=>'0');

        rf_start_o <= '0';
        rf_write_o <= '0';
        rf_addr_o  <= (others => '0');
        rf_din_o   <= (others => '0');

        syncio_counter <= (others => '0');

        cmd_reg_dev_addr <= (others => '0');

        err_decode_o <= '0';
        err_noack_o  <= '0';

        jump_bit <= '0';

        pc           <= (others => '0');
        retpnt       <= (others => '0');
        cmd_word     <= (others => '0');
        cmd_word_2   <= (others => '0');
        pmem_fetch_o <= '0';

        alu_in1_o  <= (others => '0');
        alu_in2_o  <= (others => '0');
        alu_cmd_o  <= (others => '0');
        alu_op_v_o <= '0';
        regs_o     <= (others => '0');
      else
        -- every cycle, these signals should revert to these states

        -- timer start signal should be pulsed, not held
        timer_start_o <= '0';

        -- same with rf_start
        rf_start_o <= '0';

        -- the decoding error
        err_decode_o <= '0';

        -- and with that one
        pmem_fetch_o <= '0';

        -- i2c should be generally enabled
        i2c_en_o   <= '1';

        -- syncio_o flag is pulsed high for one clock cycle, zero otherwise
        syncio_o <= '0';

        -- the ALU op_v flag is just pulsed high
        alu_op_v_o <= '0';

        case (state) is
          -- IDLE state, we're here only after the reset (and after errors?)
          when IDLE =>
            pc <= (others => '0');

            i2c_en_o     <= '0';
            i2c_start_o  <= '0';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '0';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= (others=>'0');

            timer_start_o    <= '1';
            timer_how_long_o <= (0 => '1', others => '0');

            jump_bit <= '0';

            state <= STARTUP;
          when STARTUP =>               -- wait for the startup timer to ring
            if (start_at_wd_rst_addr_i = '1') then
              pc <= to_unsigned(RESET_START_FROM, MEMABITS);
            end if;

            if (timer_done_i = '1') then
              pmem_fetch_o <= '1';
              state <= FETCH_av;        -- don't increment the PC, first
                                        -- command
            end if;

          
          when FETCH =>
            pmem_fetch_o <= '1';
            pc <= pc + 1;
            state <= FETCH_av;

          when FETCH_av =>              -- memory address is correct
            cmd_word <= pmem_dout_i;
            if (pmem_dv_i = '1') then
              state <= DECODE;
            end if;

          when DECODE =>                -- decode the opcode
            case (cmd_opcode) is
              when c_op_WRITE1 =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                state <= FETCH_WRITE_dv; -- grab second word first
              when c_op_WRITE2 =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                state <= FETCH_WRITE_dv; -- grab second word first
              when c_op_READ1 =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                state <= FETCH_READ_dv; -- grab second word first
              when c_op_READ2 =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                state <= FETCH_READ_dv; -- grab second word first
              when c_op_STOP =>
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH;
                else
                  state <= EXEC_STOP;
                end if;
              when c_op_GOTO =>
                state <= EXEC_GOTO;
              when c_op_WAIT =>
                state <= EXEC_WAIT;
              when c_op_WAIT_TRIG =>
                state <= EXEC_WAIT_TRIG;
              when c_op_MOV =>
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH;
                else
                  state <= EXEC_MOV;
                end if;
              when c_op_SYNCIO =>
                state <= EXEC_SYNCIO;
              when c_op_LOAD =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH_av;
                else
                  state <= FETCH_LOAD_av;
                end if;
              when c_op_ALU2 =>
                pmem_fetch_o <= '1';
                pc <= pc + 1;
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH_av;
                else
                  state <= FETCH_LOAD_av; -- this will then continue as ALU2
                end if;
              when c_op_ALU1 =>
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH;
                else
                  state <= EXEC_ALU1;
                end if;
              when c_op_NOP =>
                if ESSENTIALS_ONLY then -- command disabled
                  err_decode_o <= '1';
                  state <= FETCH;
                else
                  state <= FETCH;
                end if;
              when others =>
                err_decode_o <= '1';
                state <= FETCH;         -- shouldn't happen, throw error
            end case;

          when FETCH_WRITE_dv =>         -- memory results are correct
            cmd_word_2   <= pmem_dout_i; -- store second word, then exec
            size_counter <= cmd_reg_size;
            if (pmem_dv_i = '1') then
              state <= EXEC_WRITE;
            end if;

          when FETCH_READ_dv =>         -- memory results are correct
            cmd_word_2 <= pmem_dout_i;  -- store second word, then exec
            if (pmem_dv_i = '1') then
              if (cmd_dev_from_pointer = '1') then
                state <= EXEC_READ_PNT;
              elsif (cmd_skip_cmd = '1') then
                state <= EXEC_READ_NOTFIRST;
              else
                state <= EXEC_READ;
              end if;
            end if;
            

          when EXEC_WRITE =>          -- decide if writing device address
            if ((cmd_skip_cmd = '1') and (cmd_add_stop = '0')) then
              state <= EXEC_WRITE_FETCH_NEXT_DATA;
            else
              state <= EXEC_WRITE_ADDR;
            end if;


          when EXEC_WRITE_ADDR =>     -- first issue a start condition
                                      -- and write the device address
            i2c_start_o  <= '1';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            -- TODO: maybe we needed one more bit here?
            -- make sure a write address is selected
            i2c_txbyte_o <= cmd_dev_addr & '0';

            -- transaction over, go and write the register address
            if (i2c_done_i = '1') then
              i2c_start_o <= '0';
              i2c_wr_o    <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              if (cmd_skip_cmd = '1') then
                state     <= EXEC_WRITE_FETCH_NEXT_DATA;
              else
                state     <= EXEC_WRITE_REG;
              end if;
            end if;

          when EXEC_WRITE_REG =>        -- write the register address

            i2c_start_o  <= '0';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= cmd_reg_addr;

            if (i2c_done_i = '1') then
              i2c_wr_o  <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= EXEC_WRITE_FETCH_NEXT_DATA; -- fetch the data
            end if;

          when EXEC_WRITE_DATA =>       -- write the data
            i2c_start_o  <= '0';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= data_to_write;

            if (i2c_done_i = '1') then
              i2c_wr_o  <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= EXEC_WRITE_CHECK_SIZE;
            end if;

          when EXEC_WRITE_CHECK_SIZE => -- is this the last transaction?
            if ((cmd_use_literal = '1') or (size_counter = 0)) then  -- yes
              if (cmd_add_stop='1') then
                state <= EXEC_WRITE_SEND_STOP;
              else
                state <= FETCH;
              end if;
            else                        -- no, it's not
              size_counter <= size_counter - 1;
              state <= EXEC_WRITE_WAIT_ADJ_UPD;
            end if;

          -- endianness-adjusted address should be computed first
          when EXEC_WRITE_WAIT_ADJ_UPD =>
            state <= EXEC_WRITE_FETCH_NEXT_DATA;

          when EXEC_WRITE_FETCH_NEXT_DATA => -- select the data to use
            if (cmd_use_literal = '1') then  -- literal?
              data_to_write <= cmd_local_reg_addr_or_literal;

              state <= EXEC_WRITE_DATA;
            else                        -- or register?
              rf_start_o <= '1';
              rf_write_o <= '0';
              rf_addr_o  <= cmd_local_reg_addr_or_literal_adj;

              state <= EXEC_WRITE_WAIT_NEXT_DATA;
            end if;

          when EXEC_WRITE_WAIT_NEXT_DATA => -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              data_to_write <= rf_dout_i;

              state <= EXEC_WRITE_DATA;
            end if;

          when EXEC_WRITE_SEND_STOP =>  -- send a stop condition
            i2c_start_o  <= '0';
            i2c_stop_o   <= '1';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '0';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= data_to_write;

            if (i2c_done_i = '1') then
              i2c_wr_o   <= '0';
              i2c_stop_o <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= FETCH;
            end if;

          when EXEC_READ =>             -- first issue a start condition
                                        -- and write the device address
            i2c_start_o  <= '1';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            -- make sure a write address is selected
            i2c_txbyte_o <= cmd_dev_addr & '0';

            if (i2c_done_i = '1') then
              i2c_start_o <= '0';
              i2c_wr_o    <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= EXEC_READ_REG;
            end if;

          when EXEC_READ_PNT =>         -- register as pointer variant
            rf_start_o <= '1';
            rf_write_o <= '0';
            rf_addr_o  <= cmd_dev_addr(REGABITS-1 downto 0);
            state <= EXEC_READ_PNT_WAIT;

          when EXEC_READ_PNT_WAIT =>    -- wait for register output
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              cmd_reg_dev_addr <= rf_dout_i(6 downto 0);
              state <= EXEC_READ_PNT_GO;
            end if;

          when EXEC_READ_PNT_GO =>      -- like EXEC_READ
            i2c_start_o  <= '1';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            -- make sure a write address is selected
            i2c_txbyte_o <= cmd_reg_dev_addr & '0';

            if (i2c_done_i = '1') then
              i2c_start_o <= '0';
              i2c_wr_o    <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= EXEC_READ_REG;
            end if;

          when EXEC_READ_REG =>         -- write the register address
            i2c_start_o  <= '0';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= cmd_reg_addr;

            if (i2c_done_i = '1') then
              i2c_wr_o  <= '0';

              err_noack_o <= i2c_rxack_n_i; -- no ack? error and continue
              if ERR_SET_JUMP then
                jump_bit <= i2c_rxack_n_i;
              end if;

              state <= EXEC_READ_NOTFIRST;
            end if;

          when EXEC_READ_NOTFIRST =>    -- write the device address
                                        -- again
            size_counter <= cmd_reg_size;

            i2c_start_o  <= '1';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '1';
            i2c_txack_o  <= '0';
            -- make sure a read address is selected this time
            if (cmd_dev_from_pointer = '1') then
              i2c_txbyte_o <= cmd_reg_dev_addr & '1';
            else
              i2c_txbyte_o <= cmd_dev_addr & '1';
            end if;

            if (i2c_done_i = '1') then
              i2c_start_o <= '0';
              i2c_wr_o    <= '0';

              state <= EXEC_READ_DATA;
            end if;

          when EXEC_READ_DATA =>
            i2c_start_o  <= '0';
            i2c_stop_o   <= '0';
            i2c_rd_o     <= '1';
            i2c_wr_o     <= '0';

            if (size_counter = 0) then  -- issue an ack unless it's the
                                        -- last word
              i2c_txack_o  <= '1';
            else
              i2c_txack_o  <= '0';
            end if;
            i2c_txbyte_o <= (others => '0');

            if (i2c_done_i = '1') then
              rf_start_o <= '1';        -- store data in the reg file
              rf_write_o <= '1';
              rf_addr_o  <= std_logic_vector(cmd_local_reg_addr_adj);
              rf_din_o   <= i2c_rxbyte_i;

              i2c_rd_o    <= '0';
              state <= EXEC_READ_STORE_DATA;
            end if;

          when EXEC_READ_STORE_DATA =>  -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              state <= EXEC_READ_CHECK_SIZE;
            end if;

          when EXEC_READ_CHECK_SIZE =>  -- is this the last transaction?
            if (size_counter = 0) then  -- yes, it is
              if (cmd_add_stop='1') then
                i2c_txack_o  <= '0';
                state <= EXEC_READ_SEND_STOP;
              else
                state <= FETCH;
              end if;
            else                        -- no, it's not
              size_counter <= size_counter - 1;
              state <= EXEC_READ_WAIT_ADJ_UPD;
            end if;

          -- endianness-adjusted address should be computed first
          when EXEC_READ_WAIT_ADJ_UPD =>
            state <= EXEC_READ_DATA;

          when EXEC_READ_SEND_STOP =>   -- send the stop
            i2c_start_o  <= '0';
            i2c_stop_o   <= '1';
            i2c_rd_o     <= '0';
            i2c_wr_o     <= '0';
            i2c_txack_o  <= '0';
            i2c_txbyte_o <= (others => '0');

            if (i2c_done_i = '1') then
              i2c_stop_o <= '0';

              state <= FETCH;
            end if;

          when EXEC_STOP =>
            i2c_stop_o   <= '1';

            state <= EXEC_STOP_2;

          when EXEC_STOP_2 =>           -- wait for the transaction to
                                        -- finish
            i2c_stop_o   <= '0';

            if (i2c_done_i = '1') then
              i2c_wr_o   <= '0';
              i2c_stop_o <= '0';

              state <= FETCH;
            end if;

          when EXEC_GOTO =>             -- the jump is simple
            if (((cmd_goto_onjb0 = '0') and (cmd_goto_onjb1 = '0')) or
                ((cmd_goto_onjb0 = '1') and (jump_bit = '0')) or
                ((cmd_goto_onjb1 = '1') and (jump_bit = '1'))) then
              jump_bit <= '0';

              pmem_fetch_o <= '1';
              if (cmd_gotoiscall = '1') then
                pc <= retpnt;
              else
                pc <= cmd_address;
              end if;
            else
              pmem_fetch_o <= '1';
              pc <= pc + 1;
            end if;
            state <= FETCH_av;

          when EXEC_WAIT =>             -- wait, in microseconds
            i2c_en_o <= '0';

            timer_start_o    <= '1';
            timer_count_ms_o <= cmd_ms;
            timer_how_long_o <= cmd_time;

            state <= EXEC_WAIT_2;

          when EXEC_WAIT_2 =>           -- wait for the timer
            i2c_en_o <= '0';

            if (timer_done_i = '1') then
              state <= FETCH;
            end if;

          when EXEC_WAIT_TRIG =>        -- wait for an external trigger
            if (ext_trig_i = '1') then
              state <= FETCH;
            end if;

          when EXEC_MOV =>              -- fetch the data first
            rf_start_o <= '1';
            rf_write_o <= '0';
            rf_addr_o  <= std_logic_vector(cmd_src_a);
            state <= EXEC_MOV_WAIT_R;

          when EXEC_MOV_WAIT_R =>       -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then   -- write the data now
              rf_start_o <= '1';
              rf_write_o <= '1';
              rf_din_o   <= rf_dout_i;
              rf_addr_o  <= std_logic_vector(cmd_dst_a);
              state <= EXEC_MOV_WAIT_W;
            end if;

          when EXEC_MOV_WAIT_W =>       -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              state <= FETCH;
            end if;

          when FETCH_LOAD_av =>         -- memory address is correct
            if (pmem_dv_i = '1') then
              cmd_word_2 <= pmem_dout_i; -- store second word, then exec
              if (cmd_loadiscall = '1') then
                state <= EXEC_LOAD_RP;
              else
                if (cmd_opcode = c_op_ALU2) then
                  state <= EXEC_ALU2_LOAD;
                else
                  state <= EXEC_LOAD;
                end if;
              end if;
            end if;

          when EXEC_LOAD_RP =>          -- load to a register
            retpnt <= unsigned(cmd_literal(MEMABITS-1 downto 0));
            state <= FETCH;

          when EXEC_LOAD =>             -- load to a register
            rf_start_o <= '1';
            rf_write_o <= '1';
            rf_addr_o  <= std_logic_vector(cmd_local_reg_addr);
            rf_din_o   <= cmd_literal(WORDWIDTH-1 downto 0);
            state <= EXEC_LOAD_WAIT;

          when EXEC_LOAD_WAIT =>        -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              state <= FETCH;
            end if;

          when EXEC_ALU2_LOAD =>             -- load to a register
            rf_start_o <= '1';
            rf_write_o <= '0';
            rf_addr_o  <= std_logic_vector(cmd_local_reg_addr);
            state <= EXEC_ALU2_WAIT_R;

          when EXEC_ALU2_WAIT_R =>      -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              alu_in1_o             <= rf_dout_i;
              alu_in2_o             <= cmd_literal(WORDWIDTH-1 downto 0);
              alu_cmd_o(3)          <= '1';
              alu_cmd_o(2 downto 0) <= cmd_alu_cmd;
              alu_op_v_o            <= '1';

              state <= EXEC_ALU1_WAIT_RES;
            end if;

          when EXEC_ALU1 =>              -- request the initial value
            rf_start_o <= '1';
            rf_write_o <= '0';
            rf_addr_o  <= std_logic_vector(cmd_local_reg_addr);
            state <= EXEC_ALU1_WAIT_R;

          when EXEC_ALU1_WAIT_R =>       -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then   -- got it, increment
              alu_in1_o             <= rf_dout_i;
              alu_cmd_o(3)          <= '0';
              alu_cmd_o(2 downto 0) <= cmd_alu_cmd;
              alu_op_v_o            <= '1';

              state <= EXEC_ALU1_WAIT_RES;
            end if;

          when EXEC_ALU1_WAIT_RES =>      -- wait for the ALU result
            if (alu_res_v_i = '1') then
              rf_start_o <= '1';
              rf_write_o <= '1';
              rf_din_o   <= alu_result_i;
              if ZERO_SET_JUMP then
                jump_bit <= alu_iszero_i;
              end if;
              state <= EXEC_ALU1_WAIT_W;
            end if;

          when EXEC_ALU1_WAIT_W =>       -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';
            if (rf_done_i = '1') then
              state <= FETCH;
            end if;

          when EXEC_SYNCIO =>           -- first synchronize input regs
            if (syncio_counter < IWORDS) then
              rf_start_o <= '1';
              rf_write_o <= '1';
              rf_addr_o  <= std_logic_vector(syncio_counter);
              rf_din_o   <= regs_i((to_integer(syncio_counter)+1)*WORDWIDTH-1 downto
                                    to_integer(syncio_counter)*WORDWIDTH);

              state <= EXEC_SYNCIO_READIN_WAIT;
            else
              state <= EXEC_SYNCIO_WRITEOUT;
            end if;

          when EXEC_SYNCIO_READIN_WAIT => -- wait for reg file
            rf_start_o <= '0';
            rf_write_o <= '0';

            if (rf_done_i = '1') then   -- next register
              syncio_counter <= syncio_counter + 1;

              state <= EXEC_SYNCIO;
            end if;

          when EXEC_SYNCIO_WRITEOUT =>  -- then do the output regs
            if (syncio_counter < (IWORDS + OWORDS)) then
              rf_start_o <= '1';
              rf_write_o <= '0';
              rf_addr_o  <= std_logic_vector(syncio_counter);

              state <= EXEC_SYNCIO_WRITEOUT_WAIT;
            else
              syncio_counter <= (others => '0');
              syncio_o <= '1';          -- sync complete, pulse syncio_o
              state <= FETCH;
            end if;

          when EXEC_SYNCIO_WRITEOUT_WAIT => -- wait for the reg file
            rf_start_o <= '0';
            rf_write_o <= '0';

            if (rf_done_i = '1') then
              regs_o((to_integer(syncio_counter)-IWORDS+1)*WORDWIDTH-1 downto
                     (to_integer(syncio_counter)-IWORDS)*WORDWIDTH) <= rf_dout_i;

              syncio_counter <= syncio_counter + 1;

              state <= EXEC_SYNCIO_WRITEOUT;
            end if;

          when others =>                -- TODO: put something here for
                                        -- safe implementations?
            state <= IDLE;

        end case;
        
      end if;
    end if;
  end process main_proc_fsm;

end architecture behavioral;
