-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_timer.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
-- This is a utility module for the mouseq IP core, it implements a small ALU
-- with a latency of 3 clock cycles.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_alu is
  port (
    clk_i   : in std_logic;

    alu_in1_i    : in std_logic_vector(WORDWIDTH-1 downto 0);
    alu_in2_i    : in std_logic_vector(WORDWIDTH-1 downto 0);
    alu_cmd_i    : in std_logic_vector(3 downto 0);
    alu_op_v_i   : in std_logic;
    alu_result_o : out std_logic_vector(WORDWIDTH-1 downto 0);
    alu_iszero_o : out std_logic;
    alu_res_v_o  : out std_logic
  );
end entity mouseq_alu;

architecture behavioral of mouseq_alu is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  signal alu_in1_r : std_logic_vector(WORDWIDTH-1 downto 0);
  signal alu_in2_r : std_logic_vector(WORDWIDTH-1 downto 0);
  signal alu_cmd_r : std_logic_vector(3 downto 0);
  signal alu_op_v_r : std_logic;
  signal alu_op_v_r2 : std_logic;

  signal alu_result : std_logic_vector(WORDWIDTH-1 downto 0);

begin  -- architecture behavioral

  reg_proc: process (clk_i) is
  begin  -- process reg_proc
    if rising_edge(clk_i) then          -- rising clock edge
      alu_in1_r <= alu_in1_i;
      alu_in2_r <= alu_in2_i;
      alu_cmd_r <= alu_cmd_i;
      alu_op_v_r <= alu_op_v_i;
    end if;
  end process reg_proc;

  alu_proc: process (clk_i) is
  begin  -- process alu_proc
    if rising_edge(clk_i) then          -- rising clock edge
      alu_op_v_r2 <= alu_op_v_r;
      case alu_cmd_r is
        when "0000" =>
          alu_result <= std_logic_vector(unsigned(alu_in1_r) + 1);

        when "0001" =>
          alu_result <= std_logic_vector(unsigned(alu_in1_r) - 1);

        when "0010" =>
          alu_result <= alu_in1_r(WORDWIDTH-2 downto 0) & '0';

        when "0011" =>
          alu_result <= '0' & alu_in1_r(WORDWIDTH-1 downto 1);

        when "0100" =>
          alu_result <= alu_in1_r(WORDWIDTH-2 downto 0) & alu_in1_r(WORDWIDTH-1);

        when "0101" =>
          alu_result <= alu_in1_r(0) & alu_in1_r(WORDWIDTH-1 downto 1);

        when "0110" =>
          alu_result <= not alu_in1_r;

        when "0111" =>
          alu_result <= std_logic_vector(-signed(alu_in1_r));

        when "1000" =>
          alu_result <= std_logic_vector(unsigned(alu_in1_r) + unsigned(alu_in2_r));

        when "1001" =>
          alu_result <= std_logic_vector(unsigned(alu_in1_r) - unsigned(alu_in2_r));

        when "1010" =>
          alu_result <= alu_in1_r and alu_in2_r;

        when "1011" =>
          alu_result <= alu_in1_r or alu_in2_r;

        when others => null;
      end case;
    end if;
  end process alu_proc;

  out_proc: process (clk_i) is
  begin  -- process out_proc
    if rising_edge(clk_i) then          -- rising clock edge
      alu_result_o <= alu_result;
      alu_res_v_o <= alu_op_v_r2;
      if (alu_result = (alu_result'range => '0')) then
        alu_iszero_o <= '1';
      else
        alu_iszero_o <= '0';
      end if;
    end if;
  end process out_proc;

end architecture behavioral;
