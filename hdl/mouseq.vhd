-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is the main file for the mouseq IP core. It instantiates an program
-- memory, a simple processor, and an i2c controller and integrates them such
-- that monitoring tasks are made much simpler.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq is
  generic (
    CLK_PERIOD : natural := 10_000);      -- clock period in ps
  port (
    clk_i      : in std_logic;
    clk90_i    : in std_logic;
    rst_n_i    : in std_logic;

    -- external trigger
    ext_trig_i : in std_logic;

    -- I/O regs
    regs_in_i  : in std_logic_vector(IWORDS*WORDWIDTH-1 downto 0);
    regs_out_o : out std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
    syncio_o   : out std_logic;

    -- program memory ECC
    ecc_err_o  : out std_logic;
    ecc_cor_o  : out std_logic;

    -- i2c bus
    scl_o      : out std_logic;
    scl_oen_o  : out std_logic;
    scl_i      : in std_logic;
    sda_o      : out std_logic;
    sda_oen_o  : out std_logic;
    sda_i      : in std_logic
    );
end mouseq;

architecture behavioral of mouseq is

  -- initially put to better measure resources but it seems to be giving
  -- better results like that, leaving it on.
  attribute syn_hier : string;
  attribute syn_hier of behavioral : architecture is "hard";

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  -- program memory signals
  signal pmem_fetch : std_logic;
  signal pmem_dv : std_logic;
  signal pmem_dout : std_logic_vector(MEMWIDTH-1 downto 0);
  signal pc : std_logic_vector(MEMABITS-1 downto 0);

  -- I/O might be registered or no, declare signals to use
  signal regs_in  : std_logic_vector(IWORDS*WORDWIDTH-1 downto 0);
  signal regs_out : std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
  signal syncio : std_logic;

  -- signals for i2c master component
  signal rst : std_logic;
  signal i2c_en      : std_logic;
  signal i2c_start   : std_logic;
  signal i2c_stop    : std_logic;
  signal i2c_rd      : std_logic;
  signal i2c_wr      : std_logic;
  signal i2c_txack   : std_logic;
  signal i2c_busy    : std_logic;
  signal i2c_lost    : std_logic;
  signal i2c_txbyte  : std_logic_vector(7 downto 0);
  signal i2c_rxbyte  : std_logic_vector(7 downto 0);
  signal i2c_done    : std_logic;
  signal i2c_rxack_n : std_logic;

  -- (optional) watchdog signals
  type watchdog_state_t is (
    WD_STATE_CHANGE,
    WD_STATE_SAME,
    WD_RESET
    );
  signal watchdog_state : watchdog_state_t;
  signal watchdog_cnt : unsigned(WATCHDOG_BITS-1 downto 0);

  -- FSM signals for the watchdog
  signal state_is_same    : std_logic;
  signal state_is_fetch   : std_logic;
  signal state_is_wait    : std_logic;
  signal state_is_startup : std_logic;

  -- watchdog reset start address self-scrubbing register
  signal start_at_reset_addr1 : std_logic;
  signal start_at_reset_addr2 : std_logic;
  signal start_at_reset_addr3 : std_logic;
  signal start_at_reset_addr_vote_out : std_logic;

  -- preserve the three copies
  attribute syn_preserve : boolean;
  attribute syn_preserve of start_at_reset_addr1 : signal is true;
  attribute syn_preserve of start_at_reset_addr2 : signal is true;
  attribute syn_preserve of start_at_reset_addr3 : signal is true;

  -- wait timer control signals
  signal timer_start : std_logic;
  signal timer_count_ms : std_logic;
  signal timer_how_long : std_logic_vector(9 downto 0);
  signal timer_done : std_logic;

  -- register file control signals
  signal rf_start : std_logic;
  signal rf_write : std_logic;
  signal rf_addr  : std_logic_vector(REGABITS-1 downto 0);
  signal rf_din   : std_logic_vector(WORDWIDTH-1 downto 0);
  signal rf_dout  : std_logic_vector(WORDWIDTH-1 downto 0);
  signal rf_done  : std_logic;

  -- ALU signals
  signal alu_in1    : std_logic_vector(WORDWIDTH-1 downto 0);
  signal alu_in2    : std_logic_vector(WORDWIDTH-1 downto 0);
  signal alu_cmd    : std_logic_vector(3 downto 0);
  signal alu_result : std_logic_vector(WORDWIDTH-1 downto 0);
  signal alu_iszero : std_logic;
  signal alu_op_v   : std_logic;
  signal alu_res_v  : std_logic;

  -- error signals
  signal err_decode : std_logic;
  signal err_noack  : std_logic;
  signal err_rf     : std_logic;

  signal clk : std_logic;
  signal clk90 : std_logic;

begin

  clk <= clk_i;
  clk90 <= clk90_i;

  -- instantiate the program memory
  gen_std_progmem: if (SIMPLE_PROGMEM and not NX_PROGMEM) generate
    mouseq_progmem_1: entity work.mouseq_progmem
      port map (
        clk_i   => clk,
        rst_n_i => rst_n_i,
        start_i => pmem_fetch,
        addr_i  => pc,
        data_o  => pmem_dout,
        done_o  => pmem_dv
        );
  end generate gen_std_progmem;


  gen_alt_progmem: if (not SIMPLE_PROGMEM and not NX_PROGMEM) generate
    mouseq_sreg_progmem_inst: entity work.mouseq_sreg_progmem
      port map (
        clk_i   => clk,
        rst_n_i => rst_n_i,
        start_i => pmem_fetch,
        addr_i  => pc,
        data_o  => pmem_dout,
        done_o  => pmem_dv
        );
  end generate gen_alt_progmem;


  gen_nx_progmem: if NX_PROGMEM generate
    mouseq_nx_progmem_inst: entity work.mouseq_nx_progmem
      port map (
        clk_i   => clk,
        clk90_i => clk90,
        rst_n_i => rst_n_i,
        start_i => pmem_fetch,
        addr_i  => pc,
        data_o  => pmem_dout,
        done_o  => pmem_dv,
        ecc_err_o => ecc_err_o,
        ecc_cor_o => ecc_cor_o
        );
  end generate gen_nx_progmem;


  -- optionally add a register layer for the I/O
  io_regs : if BUFFERIO generate
    io_regs_proc : process (clk) is
    begin
      if rising_edge(clk) then          -- rising clock edge
        if (rst_n_i = '0') then         -- synchronous reset (active low)
          regs_in <= (others => '0');
          regs_out_o <= (others => '0');
          syncio_o <= '0';
        else
          regs_in <= regs_in_i;
          regs_out_o <= regs_out;
          syncio_o <= syncio;
        end if;
      end if;
    end process io_regs_proc;
  end generate io_regs;


  -- or don't
  no_io_regs : if not BUFFERIO generate
    regs_in <= regs_in_i;
    regs_out_o <= regs_out;
    syncio_o <= syncio;
  end generate no_io_regs;


  -- optionally add a watchdog
  watchdog: if WATCHDOG_ON generate
    -- purpose : reset the main fsm and the i2c master if the watchdog times out
    -- type    : sequential
    -- inputs  : clk, rst_n_i, state_is_same, state_is_wait, state_is_startup
    -- outputs : rst
    watchdog_proc: process (clk) is
    begin  -- process watchdog_proc
      if rising_edge(clk) then          -- rising clock edge
        if rst_n_i = '0' then           -- synchronous reset (active low)
          rst     <= '1';

          watchdog_state <= WD_STATE_CHANGE;
          watchdog_cnt <= (others => '0');
        else
          rst     <= '0';

          case watchdog_state is
            when WD_STATE_CHANGE =>
              watchdog_cnt <= (others => '0');
              if ((state_is_same = '1') and
                  (state_is_wait = '0') and
                  (state_is_startup = '0')) then
                watchdog_state <= WD_STATE_SAME;
              end if;

            when WD_STATE_SAME =>
              watchdog_cnt <= watchdog_cnt + 1;
              if (watchdog_cnt = WATCHDOG_VAL) then
                watchdog_cnt <= (others => '0');
                watchdog_state <= WD_RESET;
              elsif ((state_is_same = '1') and
                     (state_is_wait = '0') and
                     (state_is_startup = '0')) then
                watchdog_state <= WD_STATE_SAME;
              else
                watchdog_state <= WD_STATE_CHANGE;
              end if;

            when WD_RESET =>
              rst <= '1';
              watchdog_cnt <= watchdog_cnt + 1;
              if (watchdog_cnt = WATCHDOG_RESET_CYCLES) then
                watchdog_cnt <= (others => '0');
                watchdog_state <= WD_STATE_CHANGE;
              end if;

            when others =>
              watchdog_state <= WD_STATE_CHANGE;

          end case;

        end if;
      end if;
    end process watchdog_proc;
  end generate watchdog;

  -- purpose: generate the start_at_reset_addr flag for watchdog resets
  --          (rst_n_i will always make it start at pc=0)
  -- type : sequential
  -- inputs : clk, state
  -- outputs: start_at_reset_addr
  start_at_reset_addr_flag_proc: process (clk) is
  begin  -- process start_at_reset_addr_flag_proc
    if rising_edge(clk) then            -- rising clock edge
      if (rst_n_i = '0') then
        start_at_reset_addr1 <= '0';
        start_at_reset_addr2 <= '0';
        start_at_reset_addr3 <= '0';
      else
        start_at_reset_addr1 <= start_at_reset_addr_vote_out;
        start_at_reset_addr2 <= start_at_reset_addr_vote_out;
        start_at_reset_addr3 <= start_at_reset_addr_vote_out;
        if (state_is_fetch = '1') then
          start_at_reset_addr1 <= '1';
          start_at_reset_addr2 <= '1';
          start_at_reset_addr3 <= '1';
        end if;
      end if;
    end if;
  end process start_at_reset_addr_flag_proc;

  start_at_reset_addr_vote_out <= vote(start_at_reset_addr1,
                                       start_at_reset_addr2,
                                       start_at_reset_addr3);


  rst_inv: if not WATCHDOG_ON generate
    -- negate the reset to use in the i2c master and main FSM
    rst_inv_proc: process (clk) is
    begin  -- process rst_inv_proc
      if rising_edge(clk) then          -- rising clock edge
        rst <= not rst_n_i;
      end if;
    end process rst_inv_proc;
  end generate rst_inv;


  -- instantiate the i2c master
  cmp_i2c_master : entity work.i2c_master_byte_ctrl
    port map (
      clk      => clk,
      rst      => rst,
      nReset   => '1',                  -- disable async reset
      ena      => i2c_en,
      clk_cnt  => I2C_CLKS,
      start    => i2c_start,
      stop     => i2c_stop,
      read     => i2c_rd,
      write    => i2c_wr,
      ack_in   => i2c_txack,

      i2c_busy => i2c_busy,
      i2c_al   => i2c_lost,
      din      => i2c_txbyte,
      cmd_ack  => i2c_done,
      ack_out  => i2c_rxack_n,
      dout     => i2c_rxbyte,

      scl_i    => scl_i,
      scl_o    => scl_o,
      scl_oen  => scl_oen_o,
      sda_i    => sda_i,
      sda_o    => sda_o,
      sda_oen  => sda_oen_o
      );


  -- instantiate the timer module
  mouseq_timer_inst: entity work.mouseq_timer
    generic map (
      CLK_PERIOD       => CLK_PERIOD,
      DISABLE_MS       => ESSENTIALS_ONLY,
      TOTAL_COUNT_BITS => 10)
    port map (
      clk_i      => clk,
      rst_n_i    => rst_n_i,
      start_i    => timer_start,
      count_ms_i => timer_count_ms,
      how_long_i => timer_how_long,
      done_o     => timer_done
      );


  -- instantiate the register file
  gen_sreg_regfile: if not SIMPLE_REGFILE generate
    mouseq_sreg_reg_file_inst: entity work.mouseq_sreg_reg_file
      port map (
        clk_i   => clk,
        rst_n_i => rst_n_i,
        start_i => rf_start,
        write_i => rf_write,
        addr_i  => rf_addr,
        data_i  => rf_din,
        data_o  => rf_dout,
        err_o   => err_rf,
        done_o  => rf_done
        );
  end generate gen_sreg_regfile;


  gen_simple_regfile: if SIMPLE_REGFILE generate
    mouseq_simple_reg_file_inst: entity work.mouseq_simple_reg_file
      port map (
        clk_i   => clk,
        rst_n_i => rst_n_i,
        start_i => rf_start,
        write_i => rf_write,
        addr_i  => rf_addr,
        data_i  => rf_din,
        data_o  => rf_dout,
        err_o   => err_rf,
        done_o  => rf_done
        );
  end generate gen_simple_regfile;

  mouseq_alu_1: entity work.mouseq_alu
    port map (
      clk_i        => clk,
      alu_in1_i    => alu_in1,
      alu_in2_i    => alu_in2,
      alu_cmd_i    => alu_cmd,
      alu_op_v_i   => alu_op_v,
      alu_result_o => alu_result,
      alu_iszero_o => alu_iszero,
      alu_res_v_o  => alu_res_v
      );

  main_fsm: entity work.mouseq_fsm
    port map (
      clk_i                  => clk,
      rst_i                  => rst,
      start_at_wd_rst_addr_i => start_at_reset_addr_vote_out,
      ext_trig_i             => ext_trig_i,

      i2c_done_i          => i2c_done,
      i2c_rxack_n_i       => i2c_rxack_n,
      i2c_rxbyte_i        => i2c_rxbyte,
      i2c_en_o            => i2c_en,
      i2c_start_o         => i2c_start,
      i2c_stop_o          => i2c_stop,
      i2c_rd_o            => i2c_rd,
      i2c_wr_o            => i2c_wr,
      i2c_txack_o         => i2c_txack,
      i2c_txbyte_o        => i2c_txbyte,

      pmem_dout_i         => pmem_dout,
      pmem_dv_i           => pmem_dv,
      pmem_fetch_o        => pmem_fetch,
      pmem_addr_o         => pc,

      rf_dout_i           => rf_dout,
      rf_done_i           => rf_done,
      rf_start_o          => rf_start,
      rf_write_o          => rf_write,
      rf_addr_o           => rf_addr,
      rf_din_o            => rf_din,

      timer_done_i        => timer_done,
      timer_start_o       => timer_start,
      timer_count_ms_o    => timer_count_ms,
      timer_how_long_o    => timer_how_long,

      alu_result_i        => alu_result,
      alu_iszero_i        => alu_iszero,
      alu_res_v_i         => alu_res_v,
      alu_in1_o           => alu_in1,
      alu_in2_o           => alu_in2,
      alu_cmd_o           => alu_cmd,
      alu_op_v_o          => alu_op_v,

      state_is_same_o     => state_is_same,
      state_is_fetch_o    => state_is_fetch,
      state_is_wait_o     => state_is_wait,
      state_is_startup_o  => state_is_startup,

      err_decode_o        => err_decode,
      err_noack_o         => err_noack,

      regs_i              => regs_in,
      regs_o              => regs_out,
      syncio_o            => syncio
      );

end behavioral;
