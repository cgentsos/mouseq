-------------------------------------------------------------------------------
-- Title      : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
-- Project    : Distributed I/O Tier
-------------------------------------------------------------------------------
-- File       : mouseq_progmem.vhd
-- Author     : Christos Gentsos <christos.gentsos@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-- This is a utility module for the mouseq IP core, it implements a simple
-- register file.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019-2020 CERN
--
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 0.51 (the "License") (which enables you, at your option,
-- to treat this file as licensed under the Apache License 2.0); you may not
-- use this file except in compliance with the License. You may obtain a copy
-- of the License at http://solderpad.org/licenses/SHL-0.51.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_simple_reg_file is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    start_i : in  std_logic;
    write_i : in  std_logic;
    addr_i  : in std_logic_vector(REGABITS-1 downto 0);
    data_i  : in std_logic_vector(WORDWIDTH-1 downto 0);

    data_o : out std_logic_vector(WORDWIDTH-1 downto 0);
    err_o  : out std_logic;
    done_o : out std_logic);
end entity mouseq_simple_reg_file;

architecture behavioral of mouseq_simple_reg_file is

  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  -- main program memory
  signal reg_file   : RegType;

  attribute syn_ramstyle : string;
  attribute syn_ramstyle of reg_file : signal is "registers";  

  attribute syn_radhardlevel of reg_file: signal is "none";

begin  -- architecture behavioral

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (write_i = '1') then
        reg_file(to_integer(unsigned(addr_i))) <= data_i;
      end if;
      data_o <= reg_file(to_integer(unsigned(addr_i)));
      done_o <= start_i;
      if not (unsigned(addr_i) < REGSIZE) then
        err_o <= start_i;
      end if;

      if (rst_n_i = '0') then
        data_o <= (others => '0');
        done_o <= '0';
      end if;
    end if;
  end process;
  

end architecture behavioral;
