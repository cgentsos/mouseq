#!/bin/bash

TMR_ON=0
grep -sqi 'constant TMR : boolean := true' mouseq_pkg_gen.vhd && TMR_ON=1

if [[ TMR_ON -eq 1 ]]; then
    echo "Enabling TMR"
    sed -i -E 's/(.*)attribute syn_radhardlevel of behavioral: architecture is "none";/\1attribute syn_radhardlevel of behavioral: architecture is "tmr";/' mouseq*.vhd
else
    echo "Disabling TMR"
    sed -i -E 's/(.*)attribute syn_radhardlevel of behavioral: architecture is "tmr";/\1attribute syn_radhardlevel of behavioral: architecture is "none";/' mouseq*.vhd
fi
