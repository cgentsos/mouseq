import os
import sys
from nanoxmap import *

dir = str(os.path.dirname(os.path.realpath(__file__)))

project = createProject(dir) 



###############
# configuration
###############

project.setVariantName('NG-MEDIUM')
project.setTopCellName('mouseq')

project.setOptions({'MappingEffort': 'Medium',
                    'TimingDriven':'Yes',
                    'MaxRegisterCount':'3200',
                    'ManageUnconnectedSignals':'Error',
                    'VariantAwareSynthesis': 'No',
                    'MultiplierToDSPMapThreshold':'1',
                    'UseNxLibrary': 'Yes'})

#project.addMappingDirective('getModels(memory)','RAM','RAM_ECC')



###############
# source files
###############

hdl_dir = '../hdl/'

project.addFile(hdl_dir + 'i2c/i2c_master_bit_ctrl.vhd')
project.addFile(hdl_dir + 'i2c/i2c_master_byte_ctrl.vhd')
project.addFile(hdl_dir + 'mouseq_pkg_gen.vhd')
project.addFile(hdl_dir + 'mouseq_simple_reg_file.vhd')
project.addFile(hdl_dir + 'mouseq_sreg_reg_file.vhd')
project.addFile(hdl_dir + 'mouseq_progmem.vhd')
project.addFile(hdl_dir + 'mouseq_sreg_progmem.vhd')
project.addFile(hdl_dir + 'mouseq_timer.vhd')
project.addFile(hdl_dir + 'mouseq.vhd')



###############
# constraints
###############

# sys.path.append('../../top/diot_wic_demo/')
# from diot_wic_demo_nx import setConstraints
# setConstraints(project)

project.addPad('clk_i',    {'location': 'IO_B12D09P', 'type': 'LVCMOS_2.5V_4mA', 'differential': False})
project.createClock('getPort(clk_i)','clk_i', 40000)

project.save('mouseq_native.nxm')



###############
# synthesis
###############

if not project.synthesize():
	sys.exit(1)
project.save('mouseq_synthesized.v')



###############
# routing
###############

if not project.place():
	sys.exit(1)
project.reportPorts()
project.save('mouseq_placed.nxm')
if not project.route():
	sys.exit(1)
project.save('mouseq_routed.nxm')
#reports
#setLogfile('report.log')
project.reportInstances()



###############
# STA
###############

analyzer = project.createAnalyzer()
analyzer.launch()
#analyzer.reportDomains()



###############
# bitstream
###############

project.generateBitstream('mouseq_bitstream.nxb')



###############
# final reports
###############

print 'Errors: ', getErrorCount()
print 'Warnings: ', getWarningCount()
