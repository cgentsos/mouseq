library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library work;
use work.i2c_bfm_pkg.all;

entity simple_psu is
  generic (
    I2C_ADDR : std_logic_vector(9 downto 0) := "0001011000";
    I2C_FREQ : natural := 72_150);
  port (
    clk_i           : in std_logic;
    rst_n_i         : in std_logic;
    scl_b           : inout std_logic := 'Z';
    sda_b           : inout std_logic := 'Z';
    gen_random_i    : in std_logic;
    concat_bytes_o  : out std_logic_vector(21*8-1 downto 0)
    );
end entity simple_psu;

architecture behavioral of simple_psu is

  signal page : std_logic_vector(7 downto 0) := x"00";
  signal page_int : integer;

  type two_8b_words_t is array (0 to 1) of std_logic_vector(7 downto 0);
  signal mout : two_8b_words_t := (x"01", x"02");
  type two_16b_words_t is array (0 to 3) of std_logic_vector(7 downto 0);
  signal vout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal iout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal pout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal tout : two_16b_words_t := (x"01", x"02", x"03", x"04");

  signal i2c_config : t_i2c_bfm_config := C_I2C_BFM_CONFIG_DEFAULT;

begin  -- architecture behavioral

  page_int <= to_integer(unsigned(page));
  concat_bytes_o(2*8-1 downto 0*8) <= vout(2*page_int+1) & vout(2*page_int);
  concat_bytes_o(4*8-1 downto 2*8) <= iout(2*page_int+1) & iout(2*page_int);
  concat_bytes_o(6*8-1 downto 4*8) <= pout(2*page_int+1) & pout(2*page_int);
  concat_bytes_o(8*8-1 downto 6*8) <= tout(2*page_int+1) & tout(2*page_int);
  -- concat_bytes_o(9*8-1 downto 8*8) <= mout(page_int);


  p_rand : process
    procedure randomize_vars is
    begin
      for i in 0 to 3 loop
        vout(i) <= random(8);
        iout(i) <= random(8);
        pout(i) <= random(8);
        tout(i) <= random(8);
      end loop;  -- i in 0 to 3

      for i in 0 to 1 loop
        mout(i) <= random(8);
      end loop;  -- i in 0 to 1
    end procedure;
  begin

    randomize_vars;

    wait until rising_edge(gen_random_i);
    wait until rising_edge(clk_i);

  end process;


  p_trans : process
    variable cmd : t_byte_array(0 to 0);
    variable valid : boolean;
    variable data1b : t_byte_array(0 to 0);
    variable data2b : t_byte_array(0 to 1);
  begin

    i2c_config.slave_mode_address <= unsigned(I2C_ADDR);
    i2c_config.i2c_bit_time <= 1_000_000 us / I2C_FREQ;
    i2c_config.master_sda_to_scl <= 7 us;
    i2c_config.master_scl_to_sda <= 7 us;
    i2c_config.acknowledge_severity  <= warning;
    i2c_config.slave_rw_bit_severity <= warning;
    i2c_config.slave_mode_address_alert <= false;

    wait for 1 us;

    while true loop
      i2c_slave_receive(data => cmd, valid => valid, msg => "PSU receive cmd",
                        scl => scl_b, sda => sda_b,
                        config => i2c_config);
      if valid then
        case cmd(0) is
          when x"00" =>
            i2c_slave_receive_single_byte(byte => data1b(0), valid => valid, msg => "PSU receive data",
                              scl => scl_b, sda => sda_b,
                              config => i2c_config);
            if valid then
              i2c_slave_set_ack('0', msg => "PSU receive data",
                                scl => scl_b, sda => sda_b,
                                config => i2c_config);
              page <= data1b(0);
            end if;

          when x"8B" =>
            data2b(0) := vout(2*page_int+1);
            data2b(1) := vout(2*page_int);
            i2c_slave_transmit(data2b, "FCM2 transmit",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"8C" =>
            data2b(0) := iout(2*page_int+1);
            data2b(1) := iout(2*page_int);
            i2c_slave_transmit(data2b, "FCM2 transmit",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"8D" =>
            data2b(0) := tout(2*page_int+1);
            data2b(1) := tout(2*page_int);
            i2c_slave_transmit(data2b, "FCM2 transmit",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"96" =>
            data2b(0) := pout(2*page_int+1);
            data2b(1) := pout(2*page_int);
            i2c_slave_transmit(data2b, "FCM2 transmit",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"20" =>
            data1b(0) := mout(2*page_int+1);
            i2c_slave_transmit(data1b, "FCM2 transmit",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when others => null;
        end case;
      end if;
    end loop;
  end process;

end architecture behavioral;
