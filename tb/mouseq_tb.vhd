library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library work;
use work.mouseq_pkg.all;
use work.i2c_bfm_pkg.all;
use work.uart_bfm_pkg.all;

entity mouseq_tb is
end entity mouseq_tb;

architecture uvvm of mouseq_tb is

  constant C_CLK_PERIOD : time := 20 ns;
  constant C_I2C_FREQ : natural := 72_150;

  constant C_FCM2_I2C_ADDR : std_logic_vector(9 downto 0) := "0000011100";
  constant C_PSU1_I2C_ADDR : std_logic_vector(9 downto 0) := "0001011000";
  constant C_PSU2_I2C_ADDR : std_logic_vector(9 downto 0) := "0000110000";

  signal clk : std_logic := '0';
  signal clock_ena : boolean := false;

  signal rst_n : std_logic := '1';

  signal ext_trig_i : std_logic := '0';
  signal regs_in_i : std_logic_vector(IWORDS*WORDWIDTH-1 downto 0);
  signal regs_out_o : std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
  signal syncio : std_logic;
  signal ecc_err_o : std_logic;
  signal ecc_cor_o : std_logic;
  signal mouseq_scl_o      : std_logic;
  signal mouseq_scl_oen_o  : std_logic;
  signal mouseq_scl_i      : std_logic := '0';
  signal mouseq_sda_o      : std_logic;
  signal mouseq_sda_oen_o  : std_logic;
  signal mouseq_sda_i      : std_logic := '0';

  signal scl : std_logic := 'Z';
  signal sda : std_logic := 'Z';

  signal gen_random : std_logic := '0';
  signal fcm2_concat_bytes : std_logic_vector(21*8-1 downto 0);
  signal uart_txd : std_logic;

begin  -- architecture uvvm

  clock_generator(clk, clock_ena, C_CLK_PERIOD, "Main clock");

  scl <= 'H';
  sda <= 'H';

  scl <= mouseq_scl_o when mouseq_scl_oen_o='0' else 'Z';
  sda <= mouseq_sda_o when mouseq_sda_oen_o='0' else 'Z';

  mouseq_scl_i <= scl;
  mouseq_sda_i <= sda;

  gen_random <= syncio;

  dut: entity work.mouseq
    generic map (
      CLK_PERIOD => C_CLK_PERIOD / 1 ps)
    port map (
      clk_i      => clk,
      clk90_i    => '0',
      rst_n_i    => rst_n,
      ext_trig_i => ext_trig_i,
      regs_in_i  => regs_in_i,
      regs_out_o => regs_out_o,
      syncio_o   => syncio,
      ecc_err_o  => ecc_err_o,
      ecc_cor_o  => ecc_cor_o,
      scl_o      => mouseq_scl_o,
      scl_oen_o  => mouseq_scl_oen_o,
      scl_i      => mouseq_scl_i,
      sda_o      => mouseq_sda_o,
      sda_oen_o  => mouseq_sda_oen_o,
      sda_i      => mouseq_sda_i
      );

  uart_mon_intf_1: entity work.uart_mon_intf
    generic map (
      WORD_CNT => OWORDS)
    port map (
      clk_i      => clk,
      rst_n_i    => rst_n,
      regs_i     => regs_out_o,
      uart_txd_o => uart_txd,
      uart_rxd_i => '0'
      );

  simple_fcm2_1: entity work.simple_fcm2
    generic map (
      I2C_ADDR => C_FCM2_I2C_ADDR,
      I2C_FREQ => C_I2C_FREQ)
    port map (
      clk_i          => clk,
      rst_n_i        => rst_n,
      scl_b          => scl,
      sda_b          => sda,
      gen_random_i   => gen_random,
      concat_bytes_o => fcm2_concat_bytes);

  simple_psu_1: entity work.simple_psu
    generic map (
      I2C_ADDR => C_PSU1_I2C_ADDR,
      I2C_FREQ => C_I2C_FREQ)
    port map (
      clk_i          => clk,
      rst_n_i        => rst_n,
      scl_b          => scl,
      sda_b          => sda,
      gen_random_i   => gen_random,
      concat_bytes_o => open);

  simple_psu_2: entity work.simple_psu
    generic map (
      I2C_ADDR => C_PSU2_I2C_ADDR,
      I2C_FREQ => C_I2C_FREQ)
    port map (
      clk_i          => clk,
      rst_n_i        => rst_n,
      scl_b          => scl,
      sda_b          => sda,
      gen_random_i   => gen_random,
      concat_bytes_o => open);

  p_main: process

    procedure enable_clocks(dummy : t_void) is
    begin
      clock_ena <= true;
    end procedure;

    procedure disable_clocks(
      dummy: t_void) is
    begin
      clock_ena <= false;
    end procedure;

    procedure reset (
      signal rst_sig : inout std_logic;
      constant active_pol : std_logic;
      constant N_CYCLES : integer := 1) is
    begin
      rst_sig <= not active_pol;
      wait until rising_edge(clk);
      rst_sig <= active_pol;
      for I in 1 to N_CYCLES loop
        wait until rising_edge(clk);
      end loop;  -- I
      rst_sig <= not active_pol;
    end procedure;

    variable data : t_byte_array(0 to 1);

  begin
    enable_clocks(VOID);
    reset(rst_n, '0', 5);
    

    wait for 100 ms;

    disable_clocks(VOID);

    std.env.stop;
    wait;  -- to stop completely

  end process;

  comp_proc: process (clk) is
  begin  -- process comp_proc
    if rising_edge(clk) then          -- rising clock edge
      if syncio = '1' then
        log("mouseq synced its IOs");
        check_value(fcm2_concat_bytes, regs_out_o(21*8-1 downto 0),
                    warning, "comparing FCM2 and mouseq values.");
      end if;
    end if;
  end process comp_proc;

end architecture uvvm;
