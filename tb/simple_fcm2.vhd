library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library work;
use work.i2c_bfm_pkg.all;

entity simple_fcm2 is
  generic (
    I2C_ADDR : std_logic_vector(9 downto 0) := "0000011100";
    I2C_FREQ : natural := 72_150);
  port (
    clk_i           : in std_logic;
    rst_n_i         : in std_logic;
    scl_b           : inout std_logic := 'Z';
    sda_b           : inout std_logic := 'Z';
    gen_random_i    : in std_logic;
    concat_bytes_o  : out std_logic_vector(21*8-1 downto 0)
    );
end entity simple_fcm2;

architecture behavioral of simple_fcm2 is

  signal fan_status : std_logic_vector(7 downto 0) := x"CA";
  signal temp_status : std_logic_vector(7 downto 0) := x"FE";
  signal volt_status : std_logic_vector(7 downto 0) := x"BE";
  type fan_array_t is array (0 to 11) of std_logic_vector(7 downto 0);
  signal fan_speeds : fan_array_t := (x"01", x"02", x"03", x"04", x"05", x"06", x"07", x"08", x"09", x"0A", x"0B", x"0C");
  type temp_array_t is array (0 to 5) of std_logic_vector(7 downto 0);
  signal temps : temp_array_t := (x"11", x"12", x"13", x"14", x"15", x"16");

  signal i2c_config : t_i2c_bfm_config := C_I2C_BFM_CONFIG_DEFAULT;

begin  -- architecture behavioral

  concat_bytes_o(1*8-1 downto 0*8) <= fan_status;
  concat_bytes_o(2*8-1 downto 1*8) <= temp_status;
  concat_bytes_o(3*8-1 downto 2*8) <= volt_status;
  gen_fan_out: for I in 0 to 11 generate
    concat_bytes_o((4+I)*8-1 downto (3+I)*8) <= fan_speeds(I);
  end generate gen_fan_out;
  gen_temp_out: for I in 0 to 5 generate
    concat_bytes_o((4+12+I)*8-1 downto (3+12+I)*8) <= temps(I);
  end generate gen_temp_out;


  p_rand : process
    procedure randomize_vars is
    begin
      log("randomizing...");
      fan_status <= random(8);
      temp_status <= random(8);
      volt_status <= random(8);

      for i in 0 to 11 loop
        fan_speeds(i) <= random(8);
      end loop;  -- i in 0 to 5

      for i in 0 to 5 loop
        temps(i) <= random(8);
      end loop;  -- i in 0 to 5
    end procedure;
  begin

    randomize_vars;

    wait until rising_edge(gen_random_i);
    wait until rising_edge(clk_i);

  end process;


  p_trans : process
    variable cmd : t_byte_array(0 to 0);
    variable valid : boolean;
    variable data1b : t_byte_array(0 to 0);
    variable data6b : t_byte_array(0 to 5);
    variable data12b : t_byte_array(0 to 11);
  begin

    i2c_config.slave_mode_address <= unsigned(I2C_ADDR);
    i2c_config.i2c_bit_time <= 1_000_000 us / I2C_FREQ;
    i2c_config.master_sda_to_scl <= 7 us;
    i2c_config.master_scl_to_sda <= 7 us;
    i2c_config.acknowledge_severity  <= warning;
    i2c_config.slave_rw_bit_severity <= warning;
    i2c_config.slave_mode_address_alert <= false;

    wait for 1 us;

    while true loop
      i2c_slave_receive(data => cmd, valid => valid, msg => "FCM2 receive cmd",
                        scl => scl_b, sda => sda_b,
                        config => i2c_config);
      if valid then
        case cmd(0) is
          when x"00" =>
            data1b(0) := fan_status;
            i2c_slave_transmit(data1b, "FCM2 transmit data",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"01" =>
            data1b(0) := temp_status;
            i2c_slave_transmit(data1b, "FCM2 transmit data",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"02" =>
            data1b(0) := volt_status;
            i2c_slave_transmit(data1b, "FCM2 transmit data",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"03" =>
            for I in 0 to 11 loop
              data12b(I) := fan_speeds(I);
            end loop;  -- I
            i2c_slave_transmit(data12b, "FCM2 transmit data",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when x"04" =>
            for I in 0 to 5 loop
              data6b(I) := temps(I);
            end loop;  -- I
            i2c_slave_transmit(data6b, "FCM2 transmit data",
                               scl => scl_b, sda => sda_b,
                               config => i2c_config);

          when others => null;
        end case;
      end if;
    end loop;

  end process;

end architecture behavioral;
