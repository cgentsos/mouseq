files = ['mouseq_tb.vhd',
         'simple_fcm2.vhd',
         'simple_psu.vhd',
         '../tb_old/uart_driver.vhd',
         '../tb_old/uart_intf.vhd']

modules = {'git': 'git:https://github.com/chgentso/UVVM_Light.git',
           'local': '../hdl'}

fetchto = '../ip_cores'
