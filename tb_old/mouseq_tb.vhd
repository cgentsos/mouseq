library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mouseq_pkg.all;

entity mouseq_tb is
end entity mouseq_tb;

architecture mouseq_tb of mouseq_tb is

  constant clk_period : time := 10 ns;
  constant rst_time   : time := 100 ns;

  signal clk_i      : std_logic := '0';
  signal clk90_i    : std_logic := '0';
  signal rst_n_i    : std_logic := '1';
  signal regs_in_i  : std_logic_vector(IWORDS*WORDWIDTH-1 downto 0) := (others => '0');
  signal regs_out_o : std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
  signal syncio : std_logic;
  signal mouseq_scl_o      : std_logic;
  signal mouseq_scl_oen_o  : std_logic;
  signal mouseq_scl_i      : std_logic := '0';
  signal mouseq_sda_o      : std_logic;
  signal mouseq_sda_oen_o  : std_logic;
  signal mouseq_sda_i      : std_logic := '0';

  signal scl : std_logic;
  signal sda : std_logic;

  signal ext_trig : std_logic := '0';
  signal gen_random : std_logic := '0';

  signal fcm2_concat_bytes : std_logic_vector(21*8-1 downto 0);

  signal uart_rxd_i : std_logic;
  signal uart_txd_o : std_logic;

begin

  -- purpose: drive the clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_proc: process is
  begin  -- process clk_proc
    clk_i <= '0';
    wait for clk_period/2;
    clk_i <= '1';
    wait for clk_period/2;
  end process clk_proc;

  -- purpose: drive the 90-degree phase-shifted clock
  -- type   : combinational
  -- inputs :
  -- outputs: clk90_i
  clk90_proc: process is
  begin  -- process clk_proc
    wait for clk_period/4;
    clk90_i <= '0';
    wait for clk_period/2;
    clk90_i <= '1';
    wait for clk_period/4;
  end process clk90_proc;


  -- purpose: drive reset
  -- type   : combinational
  -- inputs : 
  -- outputs: rst_n_i
  rst_proc: process is
  begin  -- process rst_proc
    rst_n_i <= '0';
    wait for rst_time;
    rst_n_i <= '1';
    wait;
  end process rst_proc;


  -- purpose: put some values into the input registers
  -- type   : combinational
  -- inputs : 
  -- outputs: regs_in_i
  drive_input_regs: process is
  begin  -- process drive_input_regs
    for i in 0 to IWORDS-1 loop
      regs_in_i((i+1)*WORDWIDTH-1 downto i*WORDWIDTH) <= std_logic_vector(to_unsigned(i, WORDWIDTH));
    end loop;
    wait for 40 us;
    for i in 0 to IWORDS-1 loop
      regs_in_i((i+1)*WORDWIDTH-1 downto i*WORDWIDTH) <= std_logic_vector(to_unsigned(i+10, WORDWIDTH));
    end loop;
    wait;
  end process drive_input_regs;

  trig_proc: process is
  begin  -- process trig_proc
    wait for 100 us;
    ext_trig <= '1';
    wait until rising_edge(clk_i);
    ext_trig <= '0';
  end process trig_proc;

  dut: entity work.mouseq
    generic map (
      CLK_PERIOD => (clk_period / (1 ps)))
    port map (
      clk_i      => clk_i,
      clk90_i    => clk90_i,
      rst_n_i    => rst_n_i,
      ext_trig_i => ext_trig,
      regs_in_i  => regs_in_i,
      regs_out_o => regs_out_o,
      syncio_o   => syncio,
      scl_o      => mouseq_scl_o,
      scl_oen_o  => mouseq_scl_oen_o,
      scl_i      => mouseq_scl_i,
      sda_o      => mouseq_sda_o,
      sda_oen_o  => mouseq_sda_oen_o,
      sda_i      => mouseq_sda_i
      );

  scl <= 'H';
  sda <= 'H';

  mouseq_scl_i <= scl;
  mouseq_sda_i <= sda;
  scl <= mouseq_scl_o when mouseq_scl_oen_o='0' else 'Z';
  sda <= mouseq_sda_o when mouseq_sda_oen_o='0' else 'Z';

  gen_random <= syncio;

  comp_proc: process (clk_i) is
  begin  -- process comp_proc
    if rising_edge(clk_i) then          -- rising clock edge
      if syncio = '1' then
        report "mouseq synced its IOs.";
        if fcm2_concat_bytes = regs_out_o(21*8-1 downto 0) then
          report "its output matches the FCM2 values.";
        else
          report "its output and the FCM2 values don't match."
            severity failure;
        end if;
      end if;
    end if;
  end process comp_proc;

  fcm2_simple_inst: entity work.fcm2_simple
    port map (
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      scl_b          => scl,
      sda_b          => sda,
      i2c_addr_i     => "0011100",
      gen_random_i   => gen_random,
      concat_bytes_o => fcm2_concat_bytes
      );

  psu_simple_inst1: entity work.psu_simple
    port map (
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      scl_b          => scl,
      sda_b          => sda,
      i2c_addr_i     => "1011000",
      gen_random_i   => gen_random,
      concat_bytes_o => open
      );

  psu_simple_inst2: entity work.psu_simple
    port map (
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      scl_b          => scl,
      sda_b          => sda,
      i2c_addr_i     => "0110000",
      gen_random_i   => gen_random,
      concat_bytes_o => open
      );

  uart_mon_intf_1: entity work.uart_mon_intf
    generic map (
      WORD_CNT => OWORDS)
    port map (
      clk_i      => clk_i,
      rst_n_i    => rst_n_i,
      regs_i     => regs_out_o,
      uart_txd_o => uart_txd_o,
      uart_rxd_i => uart_rxd_i
      );

end architecture mouseq_tb;
