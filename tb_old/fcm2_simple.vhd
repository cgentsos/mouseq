library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use ieee.math_real.uniform;
use ieee.math_real.floor;

entity fcm2_simple is
  port (
    clk_i           : in std_logic;
    rst_n_i         : in std_logic;
    scl_b           : inout std_logic;
    sda_b           : inout std_logic;
    i2c_addr_i      : in std_logic_vector(6 downto 0) := "0011100";
    gen_random_i    : in std_logic;
    concat_bytes_o  : out std_logic_vector(21*8-1 downto 0)
);
end entity fcm2_simple;

architecture behavioral of fcm2_simple is

  signal fan_status : std_logic_vector(7 downto 0) := x"CA";
  signal temp_status : std_logic_vector(7 downto 0) := x"FE";
  signal volt_status : std_logic_vector(7 downto 0) := x"BE";
  type fan_array_t is array (0 to 11) of std_logic_vector(7 downto 0);
  signal fan_speeds : fan_array_t := (x"01", x"02", x"03", x"04", x"05", x"06", x"07", x"08", x"09", x"0A", x"0B", x"0C");
  type temp_array_t is array (0 to 5) of std_logic_vector(7 downto 0);
  signal temps : temp_array_t := (x"11", x"12", x"13", x"14", x"15", x"16");

  signal s_scl_i       : std_logic;
  signal s_scl_o       : std_logic;
  signal s_scl_en_o    : std_logic;
  signal s_sda_i       : std_logic;
  signal s_sda_o       : std_logic;
  signal s_sda_en_o    : std_logic;

  signal s_ack         : std_logic := '1';
  signal s_tx_byte     : std_logic_vector(7 downto 0) := (others => '0');
  signal s_rx_byte     : std_logic_vector(7 downto 0);
  signal s_i2c_sta_p   : std_logic;
  signal s_i2c_sto_p   : std_logic;
  signal s_addr_good_p : std_logic;
  signal s_r_done_p    : std_logic;
  signal s_w_done_p    : std_logic;
  signal s_op          : std_logic;

begin  -- architecture behavioral

  concat_bytes_o(1*8-1 downto 0*8) <= fan_status;
  concat_bytes_o(2*8-1 downto 1*8) <= temp_status;
  concat_bytes_o(3*8-1 downto 2*8) <= volt_status;
  gen_fan_out: for I in 0 to 11 generate
    concat_bytes_o((4+I)*8-1 downto (3+I)*8) <= fan_speeds(I);
  end generate gen_fan_out;
  gen_temp_out: for I in 0 to 5 generate
    concat_bytes_o((4+12+I)*8-1 downto (3+12+I)*8) <= temps(I);
  end generate gen_temp_out;

  process
    variable seed1 : positive := 1;
    variable seed2 : positive := 1;
    variable x : real;
    variable y : integer;
  begin

    uniform(seed1, seed2, x);
    y := integer(x * 255.0);
    fan_status <= std_logic_vector(to_unsigned(y, 8));

    uniform(seed1, seed2, x);
    y := integer(x * 255.0);
    temp_status <= std_logic_vector(to_unsigned(y, 8));

    uniform(seed1, seed2, x);
    y := integer(x * 255.0);
    volt_status <= std_logic_vector(to_unsigned(y, 8));

    for i in 0 to 11 loop
      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      fan_speeds(i) <= std_logic_vector(to_unsigned(y, 8));
    end loop;  -- i in 0 to 5

    for i in 0 to 5 loop
      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      temps(i) <= std_logic_vector(to_unsigned(y, 8));
    end loop;  -- i in 0 to 5

    wait until rising_edge(gen_random_i);
    wait until rising_edge(clk_i);

  end process;


  process
    variable cmd : std_logic_vector(7 downto 0);
  begin

    wait until (s_addr_good_p='1');
    wait until rising_edge(clk_i);
    wait until (s_r_done_p='1');
    wait until rising_edge(clk_i);
    cmd := s_rx_byte;

    case cmd is
      when x"00" =>
        s_tx_byte <= fan_status;
        report "read the FCM2 fan status.";

      when x"01" =>
        s_tx_byte <= temp_status;
        report "read the FCM2 temperature status.";

      when x"02" =>
        s_tx_byte <= volt_status;
        report "read the FCM2 volt status.";

      when x"03" =>
        for i in 0 to 11 loop
          s_tx_byte <= fan_speeds(i);
          wait until rising_edge(s_w_done_p);
        end loop;  -- i
        report "read the FCM2 fan speeds.";

      when x"04" =>
        for i in 0 to 5 loop
          s_tx_byte <= temps(i);
          wait until rising_edge(s_w_done_p);
        end loop;  -- i
        report "read the FCM2 temperatures.";

      when others => null;
    end case;

  end process;


  s_scl_i <= scl_b;
  s_sda_i <= sda_b;
  scl_b <= s_scl_o when s_scl_en_o='1' else 'Z';
  sda_b <= s_sda_o when s_sda_en_o='1' else 'Z';


  gc_i2c_slave_inst: entity work.gc_i2c_slave
    generic map (
      g_auto_addr_ack => true)
    port map (
      clk_i         => clk_i,
      rst_n_i       => rst_n_i,
      scl_i         => s_scl_i,
      scl_o         => s_scl_o,
      scl_en_o      => s_scl_en_o,
      sda_i         => s_sda_i,
      sda_o         => s_sda_o,
      sda_en_o      => s_sda_en_o,
      i2c_addr_i    => i2c_addr_i,
      ack_i         => s_ack,
      tx_byte_i     => s_tx_byte,
      rx_byte_o     => s_rx_byte,
      i2c_sta_p_o   => s_i2c_sta_p,
      i2c_sto_p_o   => s_i2c_sto_p,
      addr_good_p_o => s_addr_good_p,
      r_done_p_o    => s_r_done_p,
      w_done_p_o    => s_w_done_p,
      op_o          => s_op
      );


end architecture behavioral;
