library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vhpiregflip;
use vhpiregflip.vhpiregflip_funcs.all;

use work.mouseq_pkg.all;

entity mouseq_tb is
end entity mouseq_tb;

architecture mouseq_tb of mouseq_tb is

  constant clk_period : time := 40 ns;
  constant rst_time   : time := 4000 ns;

  signal clk_i      : std_logic := '0';
  signal rst_n_i    : std_logic := '1';
  signal ext_trig_i : std_logic := '0';
  signal regs_in_i  : std_logic_vector(IWORDS*WORDWIDTH-1 downto 0) := (others => '0');
  signal regs_out_o : std_logic_vector(OWORDS*WORDWIDTH-1 downto 0);
  signal mouseq_scl_o      : std_logic;
  signal mouseq_scl_oen_o  : std_logic;
  signal mouseq_scl_i      : std_logic := '0';
  signal mouseq_sda_o      : std_logic;
  signal mouseq_sda_oen_o  : std_logic;
  signal mouseq_sda_i      : std_logic := '0';

  signal scl : std_logic;
  signal sda : std_logic;

  signal s_scl_i       : std_logic;
  signal s_scl_o       : std_logic;
  signal s_scl_en_o    : std_logic;
  signal s_sda_i       : std_logic;
  signal s_sda_o       : std_logic;
  signal s_sda_en_o    : std_logic;

  signal s_i2c_addr    : std_logic_vector(6 downto 0);
  signal s_ack         : std_logic;
  signal s_tx_byte     : std_logic_vector(7 downto 0);
  signal s_rx_byte     : std_logic_vector(7 downto 0);
  signal s_i2c_sta_p   : std_logic;
  signal s_i2c_sto_p   : std_logic;
  signal s_addr_good_p : std_logic;
  signal s_r_done_p    : std_logic;
  signal s_w_done_p    : std_logic;
  signal s_op          : std_logic;

begin

  -- purpose: drive the clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clk_proc: process is
  begin  -- process clk_proc
    clk_i <= '0';
    wait for clk_period/2;
    clk_i <= '1';
    wait for clk_period/2;
    inc_flip;
  end process clk_proc;


  -- purpose: drive reset
  -- type   : combinational
  -- inputs : 
  -- outputs: rst_n_i
  rst_proc: process is
  begin  -- process rst_proc
    rst_n_i <= '0';
    wait for rst_time;
    rst_n_i <= '1';
    wait;
  end process rst_proc;


  -- purpose: put some values into the input registers
  -- type   : combinational
  -- inputs : 
  -- outputs: regs_in_i
  drive_input_regs: process is
  begin  -- process drive_input_regs
    for i in 0 to IWORDS-1 loop
      regs_in_i((i+1)*WORDWIDTH-1 downto i*WORDWIDTH) <= std_logic_vector(to_unsigned(i, WORDWIDTH));
    end loop;
    wait for 40 us;
    for i in 0 to IWORDS-1 loop
      regs_in_i((i+1)*WORDWIDTH-1 downto i*WORDWIDTH) <= std_logic_vector(to_unsigned(i+10, WORDWIDTH));
    end loop;
    wait;
  end process drive_input_regs;


  dut: entity work.mouseq
    -- generic map (
    --   CLK_PERIOD => (clk_period / (1 ps)))
    port map (
      clk_i      => clk_i,
      rst_n_i    => rst_n_i,
      -- regs_in_i  => regs_in_i,
      regs_out_o => regs_out_o,
      ext_trig_i => ext_trig_i,
      scl_o      => mouseq_scl_o,
      scl_oen    => mouseq_scl_oen_o,
      scl_i      => mouseq_scl_i,
      sda_o      => mouseq_sda_o,
      sda_oen    => mouseq_sda_oen_o,
      sda_i      => mouseq_sda_i
      );

  scl <= 'H';
  sda <= 'H';

  mouseq_scl_i <= scl;
  mouseq_sda_i <= sda;
  scl <= mouseq_scl_o when mouseq_scl_oen_o='0' else 'Z';
  sda <= mouseq_sda_o when mouseq_sda_oen_o='0' else 'Z';

  s_scl_i <= scl;
  s_sda_i <= sda;
  scl <= s_scl_o when s_scl_en_o='1' else 'Z';
  sda <= s_sda_o when s_sda_en_o='1' else 'Z';

  gc_i2c_slave_inst: entity work.gc_i2c_slave
    generic map (
      g_auto_addr_ack => true)
    port map (
      clk_i         => clk_i,
      rst_n_i       => rst_n_i,
      scl_i         => s_scl_i,
      scl_o         => s_scl_o,
      scl_en_o      => s_scl_en_o,
      sda_i         => s_sda_i,
      sda_o         => s_sda_o,
      sda_en_o      => s_sda_en_o,
      i2c_addr_i    => s_i2c_addr,
      ack_i         => s_ack,
      tx_byte_i     => s_tx_byte,
      rx_byte_o     => s_rx_byte,
      i2c_sta_p_o   => s_i2c_sta_p,
      i2c_sto_p_o   => s_i2c_sto_p,
      addr_good_p_o => s_addr_good_p,
      r_done_p_o    => s_r_done_p,
      w_done_p_o    => s_w_done_p,
      op_o          => s_op
      );

  fcm2_simple_inst: entity work.fcm2_simple
    port map (
      clk_i         => clk_i,
      rst_n_i       => rst_n_i,
      i2c_addr_o    => s_i2c_addr,
      ack_o         => s_ack,
      tx_byte_o     => s_tx_byte,
      rx_byte_i     => s_rx_byte,
      i2c_sta_p_i   => s_i2c_sta_p,
      i2c_sto_p_i   => s_i2c_sto_p,
      addr_good_p_i => s_addr_good_p,
      r_done_p_i    => s_r_done_p,
      w_done_p_i    => s_w_done_p
    );


end architecture mouseq_tb;
