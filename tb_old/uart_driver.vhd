library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity uart_mon_wb_drv is
  generic (
    WAIT_TRIG : boolean := false;       -- wait for trigger before sending next
                                        -- data frame

    WAIT_TIME : boolean := false;       -- wait for WAIT_CNT clock cycles
                                        -- between data frames
    WAIT_CNT : natural := 10000;        -- the wait cycle count

    WORD_CNT : natural := 33            -- maximum word counter value
    );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    wb_adr_o : out std_logic_vector(4 downto 0);
    wb_dat_o : out std_logic_vector(31 downto 0);
    wb_dat_i : in std_logic_vector(31 downto 0);
    wb_cyc_o : out std_logic;
    wb_stb_o : out std_logic;
    wb_we_o : out std_logic;
    wb_ack_i : in std_logic;
    wb_stall_i : in std_logic;

    trig_i : in std_logic;

    regs_i : in std_logic_vector(WORD_CNT*8-1 downto 0)

    );
end entity uart_mon_wb_drv;

architecture behav of uart_mon_wb_drv is

  constant WAIT_CNT_WIDTH : natural := integer(ceil(log2(real(WAIT_CNT))));
  constant WORD_CNT_WIDTH : natural := integer(ceil(log2(real(WORD_CNT))));

  signal wait_counter : unsigned(WAIT_CNT_WIDTH-1 downto 0) := (others => '0');
  signal word_counter : unsigned(WORD_CNT_WIDTH-1 downto 0) := (others => '0');

  type state_t is (IDLE,
                   WR_BAUD_REQ,
                   WR_BAUD_GETACK,
                   RD_TX_RDY_REQ,
                   RD_TX_RDY_GETACK,
                   WR_TX_REQ,
                   WR_TX_GETACK,
                   WAIT_BETWEEN_FRAMES);
  signal state : state_t := IDLE;

begin  -- architecture behav

  assert (not (WAIT_TIME and WAIT_TRIG)) report "Can't wait on both time and trigger (yet)." severity error;

  state_proc : process(clk_i) is
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        wb_stb_o <= '0';
        wb_adr_o <= (others => '0');
        wb_dat_o <= (others => '0');
        wb_cyc_o <= '0';
        wb_stb_o <= '0';
        wb_we_o <= '0';

        word_counter <= (others => '0');
        wait_counter <= (others => '0');

        state <= IDLE;
      else
        wb_stb_o <= '0';
        case state is
          when IDLE =>
            wb_adr_o <= (others => '0');
            wb_dat_o <= (others => '0');
            wb_cyc_o <= '0';
            wb_stb_o <= '0';
            wb_we_o <= '0';

            if (wb_stall_i = '0') then
              state <= WR_BAUD_REQ;
            end if;

          when WR_BAUD_REQ =>
            wb_adr_o <= (0 => '1', others => '0');
            wb_dat_o <= x"000004b8";
            wb_cyc_o <= '1';
            wb_stb_o <= '1';
            wb_we_o <= '1';

            state <= WR_BAUD_GETACK;

          when WR_BAUD_GETACK =>
            if (wb_ack_i = '1') then
              wb_cyc_o <= '0';
              wb_we_o <= '0';
              state <= WAIT_BETWEEN_FRAMES;
            end if;

          when RD_TX_RDY_REQ =>
            wb_adr_o <= (others => '0');
            wb_dat_o <= (others => '0');

            if (wb_stall_i = '0') then
              wb_cyc_o <= '1';
              wb_stb_o <= '1';
              wb_we_o <= '0';
              state <= RD_TX_RDY_GETACK;
            end if;

          when RD_TX_RDY_GETACK =>
            if (wb_ack_i = '1') then
              wb_cyc_o <= '0';
              if (wb_dat_i(0) = '1') then
                state <= RD_TX_RDY_REQ; -- still transmitting
              else
                state <= WR_TX_REQ;        -- TX is idle, go!
              end if;
            end if;

          when WR_TX_REQ =>
            wb_adr_o <= (1 => '1', others => '0');
            -- report "word_counter = " & integer'image(to_integer(word_counter)) severity note;
            wb_dat_o <= x"000000" & regs_i((to_integer(word_counter)+1)*8-1 downto
                                           to_integer(word_counter)*8);

            if (wb_stall_i = '0') then
              wb_cyc_o <= '1';
              wb_stb_o <= '1';
              wb_we_o <= '1';
              state <= WR_TX_GETACK;
            end if;

          when WR_TX_GETACK =>
            if (wb_ack_i = '1') then
              wb_cyc_o <= '0';
              wb_we_o <= '0';

              if (word_counter = WORD_CNT - 1) then
                word_counter <= (others => '0');
                wait_counter <= (others => '0');
                state <= WAIT_BETWEEN_FRAMES;
              else
                word_counter <= word_counter + 1;
                state <= RD_TX_RDY_REQ;
              end if;
            end if;

          when WAIT_BETWEEN_FRAMES =>
            if (WAIT_TIME) then
              wait_counter <= wait_counter + 1;
            end if;

            if (((WAIT_TRIG) and (trig_i = '1')) or
                ((WAIT_TIME) and (wait_counter = WAIT_CNT - 1)) or
                ((not WAIT_TRIG) and (not WAIT_TIME))) then
              state <= RD_TX_RDY_REQ;
            end if;

          when others => null;
        end case;
      end if;
    end if;
  end process state_proc;

end architecture behav;
