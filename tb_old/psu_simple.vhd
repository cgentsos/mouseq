library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use ieee.math_real.uniform;
use ieee.math_real.floor;

entity psu_simple is
  port (
    clk_i           : in std_logic;
    rst_n_i         : in std_logic;
    scl_b           : inout std_logic;
    sda_b           : inout std_logic;
    i2c_addr_i      : in std_logic_vector(6 downto 0) := "0011100";
    gen_random_i    : in std_logic;
    concat_bytes_o  : out std_logic_vector(9*8-1 downto 0)
);
end entity psu_simple;

architecture behavioral of psu_simple is

  signal page : std_logic_vector(7 downto 0) := x"00";
  signal page_int : integer;

  type two_8b_words_t is array (0 to 1) of std_logic_vector(7 downto 0);
  signal mout : two_8b_words_t := (x"01", x"02");
  type two_16b_words_t is array (0 to 3) of std_logic_vector(7 downto 0);
  signal vout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal iout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal pout : two_16b_words_t := (x"01", x"02", x"03", x"04");
  signal tout : two_16b_words_t := (x"01", x"02", x"03", x"04");

  signal s_scl_i       : std_logic;
  signal s_scl_o       : std_logic;
  signal s_scl_en_o    : std_logic;
  signal s_sda_i       : std_logic;
  signal s_sda_o       : std_logic;
  signal s_sda_en_o    : std_logic;

  signal s_ack         : std_logic := '1';
  signal s_tx_byte     : std_logic_vector(7 downto 0) := (others => '0');
  signal s_rx_byte     : std_logic_vector(7 downto 0);
  signal s_i2c_sta_p   : std_logic;
  signal s_i2c_sto_p   : std_logic;
  signal s_addr_good_p : std_logic;
  signal s_r_done_p    : std_logic;
  signal s_w_done_p    : std_logic;
  signal s_op          : std_logic;

begin  -- architecture behavioral

  page_int <= to_integer(unsigned(page));
  concat_bytes_o(2*8-1 downto 0*8) <= vout(2*page_int+1) & vout(2*page_int);
  concat_bytes_o(4*8-1 downto 2*8) <= iout(2*page_int+1) & iout(2*page_int);
  concat_bytes_o(6*8-1 downto 4*8) <= pout(2*page_int+1) & pout(2*page_int);
  concat_bytes_o(8*8-1 downto 6*8) <= tout(2*page_int+1) & tout(2*page_int);
  -- concat_bytes_o(9*8-1 downto 8*8) <= mout(page_int);

  process
    variable seed1 : positive := 1;
    variable seed2 : positive := 1;
    variable x : real;
    variable y : integer;
  begin

    for i in 0 to 3 loop
      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      vout(i) <= std_logic_vector(to_unsigned(y, 8));

      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      iout(i) <= std_logic_vector(to_unsigned(y, 8));

      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      pout(i) <= std_logic_vector(to_unsigned(y, 8));

      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      tout(i) <= std_logic_vector(to_unsigned(y, 8));
    end loop;  -- i in 0 to 3

    for i in 0 to 1 loop
      uniform(seed1, seed2, x);
      y := integer(x * 255.0);
      mout(i) <= std_logic_vector(to_unsigned(y, 8));
    end loop;  -- i in 0 to 1

    wait until rising_edge(gen_random_i);
    wait until rising_edge(clk_i);

  end process;


  process
    variable cmd : std_logic_vector(7 downto 0);
  begin

    wait until (s_addr_good_p='1');
    wait until rising_edge(clk_i);
    wait until (s_r_done_p='1');
    wait until rising_edge(clk_i);
    cmd := s_rx_byte;

    case cmd is
      when x"00" =>
        -- set page command
        wait until (s_r_done_p='1');
        wait until rising_edge(clk_i);
        page <= s_rx_byte;
        report "set the PSU page to " & integer'image(to_integer(unsigned(s_rx_byte))) & ".";

      when x"8B" =>
        s_tx_byte <= vout(2*page_int+1);
        wait until rising_edge(s_w_done_p);
        s_tx_byte <= vout(2*page_int);
        wait until rising_edge(s_w_done_p);
        report "read the PSU voltage at page " & integer'image(page_int) & ".";

      when x"8C" =>
        s_tx_byte <= iout(2*page_int+1);
        wait until rising_edge(s_w_done_p);
        s_tx_byte <= iout(2*page_int);
        wait until rising_edge(s_w_done_p);
        report "read the PSU current at page " & integer'image(page_int) & ".";

      when x"8D" =>
        s_tx_byte <= tout(2*page_int+1);
        wait until rising_edge(s_w_done_p);
        s_tx_byte <= tout(2*page_int);
        wait until rising_edge(s_w_done_p);
        report "read the PSU temperature at page " & integer'image(page_int) & ".";

      when x"96" =>
        s_tx_byte <= pout(2*page_int+1);
        wait until rising_edge(s_w_done_p);
        s_tx_byte <= pout(2*page_int);
        wait until rising_edge(s_w_done_p);
        report "read the PSU power at page " & integer'image(page_int) & ".";

      when x"20" =>
        s_tx_byte <= pout(2*page_int+1);
        report "read the PSU vmode at page " & integer'image(page_int) & ".";

      when others => null;
    end case;

  end process;


  s_scl_i <= scl_b;
  s_sda_i <= sda_b;
  scl_b <= s_scl_o when s_scl_en_o='1' else 'Z';
  sda_b <= s_sda_o when s_sda_en_o='1' else 'Z';


  gc_i2c_slave_inst: entity work.gc_i2c_slave
    generic map (
      g_auto_addr_ack => true)
    port map (
      clk_i         => clk_i,
      rst_n_i       => rst_n_i,
      scl_i         => s_scl_i,
      scl_o         => s_scl_o,
      scl_en_o      => s_scl_en_o,
      sda_i         => s_sda_i,
      sda_o         => s_sda_o,
      sda_en_o      => s_sda_en_o,
      i2c_addr_i    => i2c_addr_i,
      ack_i         => s_ack,
      tx_byte_i     => s_tx_byte,
      rx_byte_o     => s_rx_byte,
      i2c_sta_p_o   => s_i2c_sta_p,
      i2c_sto_p_o   => s_i2c_sto_p,
      addr_good_p_o => s_addr_good_p,
      r_done_p_o    => s_r_done_p,
      w_done_p_o    => s_w_done_p,
      op_o          => s_op
      );


end architecture behavioral;
