library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_mon_intf is
  generic (
    WORD_CNT : natural := 32            -- input word count
    );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    regs_i : in std_logic_vector(WORD_CNT*8-1 downto 0);

    uart_txd_o : out std_logic;
    uart_rxd_i : in std_logic
    );
  
end entity uart_mon_intf;

architecture behav of uart_mon_intf is

  constant TRAILER_LEN : natural := 3;  -- unused for now

  signal regs_to_uart : std_logic_vector((WORD_CNT+2)*8-1 downto 0);

  signal wb_adr : std_logic_vector(4 downto 0);
  signal wb_dat_tu : std_logic_vector(31 downto 0);
  signal wb_dat_fu : std_logic_vector(31 downto 0);
  signal wb_cyc : std_logic;
  signal wb_sel : std_logic_vector(3 downto 0) := "1111";
  signal wb_stb : std_logic;
  signal wb_we : std_logic;
  signal wb_ack : std_logic;
  signal wb_stall : std_logic;

  signal trig_i : std_logic;

  signal trig_from_mouseq : std_logic;
  signal trig_from_mouseq_r : std_logic;

begin  -- architecture behav

  regs_to_uart((WORD_CNT-1)*8-1 downto 0) <= regs_i((WORD_CNT-1)*8-1 downto 0);
  regs_to_uart((WORD_CNT+2)*8-1 downto (WORD_CNT-1)*8) <= x"c0be00";

  trig_from_mouseq <= regs_i(WORD_CNT*8-1);
  m_trig_proc: process (clk_i) is
  begin  -- process trig_proc
    if rising_edge(clk_i) then          -- rising clock edge
      if (rst_n_i = '0') then             -- synchronous reset (active high)
        trig_from_mouseq_r <= '0';
        trig_i <= '0';
      else
        trig_from_mouseq_r <= trig_from_mouseq;
        if ((trig_from_mouseq_r = '0') and (trig_from_mouseq = '1')) then
          trig_i <= '1';
        else
          trig_i <= '0';
        end if;
      end if;
    end if;
  end process m_trig_proc;

  uart_mon_wb_drv_1: entity work.uart_mon_wb_drv
    generic map (
      WAIT_TIME => false,
      WAIT_TRIG => true,
      WAIT_CNT => 200,
      WORD_CNT => WORD_CNT+2)
    port map (
      clk_i => clk_i,
      rst_n_i => rst_n_i,
      wb_adr_o => wb_adr,
      wb_dat_o => wb_dat_tu,
      wb_dat_i => wb_dat_fu,
      wb_cyc_o => wb_cyc,
      wb_stb_o => wb_stb,
      wb_we_o => wb_we,
      wb_ack_i => wb_ack,
      wb_stall_i => wb_stall,
      trig_i => trig_i,
      regs_i => regs_to_uart
      );

  wb_simple_uart_1: entity work.wb_simple_uart
    generic map (
      g_with_virtual_uart => false,
      g_with_physical_uart => true)
    port map (
      clk_sys_i => clk_i,
      rst_n_i => rst_n_i,
      wb_adr_i => wb_adr,
      wb_dat_i => wb_dat_tu,
      wb_dat_o => wb_dat_fu,
      wb_cyc_i => wb_cyc,
      wb_sel_i => wb_sel,
      wb_stb_i => wb_stb,
      wb_we_i => wb_we,
      wb_ack_o => wb_ack,
      wb_stall_o => wb_stall,
      int_o => open,
      uart_rxd_i => uart_rxd_i,
      uart_txd_o => uart_txd_o
      );

end architecture behav;
