library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.nxPackage.all;

entity nx_ram is

  generic (
    std_mode : string := "";
    mcka_edge : bit := '0';
    mckb_edge : bit := '0';
    pcka_edge : bit := '0';
    pckb_edge : bit := '0';
    mem_ctxt : string := "";
    raw_config0 : bit_vector(3 downto 0) := B"0000";
    raw_config1 : bit_vector(15 downto 0) := B"0000000000000000");

  port (
    ACK : in std_logic := '0';
    ACKC : in std_logic := '0';
    ACKD : in std_logic := '0';
    ACKR : in std_logic := '0';
    BCK : in std_logic := '0';
    BCKC : in std_logic := '0';
    BCKD : in std_logic := '0';
    BCKR : in std_logic := '0';
    AI1 : in std_logic := '0';
    AI2 : in std_logic := '0';
    AI3 : in std_logic := '0';
    AI4 : in std_logic := '0';
    AI5 : in std_logic := '0';
    AI6 : in std_logic := '0';
    AI7 : in std_logic := '0';
    AI8 : in std_logic := '0';
    AI9 : in std_logic := '0';
    AI10 : in std_logic := '0';
    AI11 : in std_logic := '0';
    AI12 : in std_logic := '0';
    AI13 : in std_logic := '0';
    AI14 : in std_logic := '0';
    AI15 : in std_logic := '0';
    AI16 : in std_logic := '0';
    AI17 : in std_logic := '0';
    AI18 : in std_logic := '0';
    AI19 : in std_logic := '0';
    AI20 : in std_logic := '0';
    AI21 : in std_logic := '0';
    AI22 : in std_logic := '0';
    AI23 : in std_logic := '0';
    AI24 : in std_logic := '0';
    BI1 : in std_logic := '0';
    BI2 : in std_logic := '0';
    BI3 : in std_logic := '0';
    BI4 : in std_logic := '0';
    BI5 : in std_logic := '0';
    BI6 : in std_logic := '0';
    BI7 : in std_logic := '0';
    BI8 : in std_logic := '0';
    BI9 : in std_logic := '0';
    BI10 : in std_logic := '0';
    BI11 : in std_logic := '0';
    BI12 : in std_logic := '0';
    BI13 : in std_logic := '0';
    BI14 : in std_logic := '0';
    BI15 : in std_logic := '0';
    BI16 : in std_logic := '0';
    BI17 : in std_logic := '0';
    BI18 : in std_logic := '0';
    BI19 : in std_logic := '0';
    BI20 : in std_logic := '0';
    BI21 : in std_logic := '0';
    BI22 : in std_logic := '0';
    BI23 : in std_logic := '0';
    BI24 : in std_logic := '0';
    ACOR : out std_logic := '0';
    AERR : out std_logic := '0';
    BCOR : out std_logic := '0';
    BERR : out std_logic := '0';
    AO1 : out std_logic := '0';
    AO2 : out std_logic := '0';
    AO3 : out std_logic := '0';
    AO4 : out std_logic := '0';
    AO5 : out std_logic := '0';
    AO6 : out std_logic := '0';
    AO7 : out std_logic := '0';
    AO8 : out std_logic := '0';
    AO9 : out std_logic := '0';
    AO10 : out std_logic := '0';
    AO11 : out std_logic := '0';
    AO12 : out std_logic := '0';
    AO13 : out std_logic := '0';
    AO14 : out std_logic := '0';
    AO15 : out std_logic := '0';
    AO16 : out std_logic := '0';
    AO17 : out std_logic := '0';
    AO18 : out std_logic := '0';
    AO19 : out std_logic := '0';
    AO20 : out std_logic := '0';
    AO21 : out std_logic := '0';
    AO22 : out std_logic := '0';
    AO23 : out std_logic := '0';
    AO24 : out std_logic := '0';
    BO1 : out std_logic := '0';
    BO2 : out std_logic := '0';
    BO3 : out std_logic := '0';
    BO4 : out std_logic := '0';
    BO5 : out std_logic := '0';
    BO6 : out std_logic := '0';
    BO7 : out std_logic := '0';
    BO8 : out std_logic := '0';
    BO9 : out std_logic := '0';
    BO10 : out std_logic := '0';
    BO11 : out std_logic := '0';
    BO12 : out std_logic := '0';
    BO13 : out std_logic := '0';
    BO14 : out std_logic := '0';
    BO15 : out std_logic := '0';
    BO16 : out std_logic := '0';
    BO17 : out std_logic := '0';
    BO18 : out std_logic := '0';
    BO19 : out std_logic := '0';
    BO20 : out std_logic := '0';
    BO21 : out std_logic := '0';
    BO22 : out std_logic := '0';
    BO23 : out std_logic := '0';
    BO24 : out std_logic := '0';
    AA1 : in std_logic := '0';
    AA2 : in std_logic := '0';
    AA3 : in std_logic := '0';
    AA4 : in std_logic := '0';
    AA5 : in std_logic := '0';
    AA6 : in std_logic := '0';
    AA7 : in std_logic := '0';
    AA8 : in std_logic := '0';
    AA9 : in std_logic := '0';
    AA10 : in std_logic := '0';
    AA11 : in std_logic := '0';
    AA12 : in std_logic := '0';
    AA13 : in std_logic := '0';
    AA14 : in std_logic := '0';
    AA15 : in std_logic := '0';
    AA16 : in std_logic := '0';
    ACS : in std_logic := '0';
    AWE : in std_logic := '0';
    AR : in std_logic := '0';
    BA1 : in std_logic := '0';
    BA2 : in std_logic := '0';
    BA3 : in std_logic := '0';
    BA4 : in std_logic := '0';
    BA5 : in std_logic := '0';
    BA6 : in std_logic := '0';
    BA7 : in std_logic := '0';
    BA8 : in std_logic := '0';
    BA9 : in std_logic := '0';
    BA10 : in std_logic := '0';
    BA11 : in std_logic := '0';
    BA12 : in std_logic := '0';
    BA13 : in std_logic := '0';
    BA14 : in std_logic := '0';
    BA15 : in std_logic := '0';
    BA16 : in std_logic := '0';
    BCS : in std_logic := '0';
    BWE : in std_logic := '0';
    BR : in std_logic := '0');

end entity nx_ram;

architecture behav of nx_ram is

  signal AI : std_logic_vector(23 downto 0);
  signal AO : std_logic_vector(23 downto 0);
  signal AA : std_logic_vector(15 downto 0);

  signal BI : std_logic_vector(23 downto 0);
  signal BO : std_logic_vector(23 downto 0);
  signal BA : std_logic_vector(15 downto 0);

  type nx_mem_t is array (0 to 2047) of std_logic_vector(23 downto 0);

  function to_std_logic_vector(a : string) return std_logic_vector is
    variable ret : std_logic_vector(a'length-1 downto 0);
    variable i_mod : natural;
  begin
    for i in a'range loop
      i_mod := i mod 19;
      if (a(i) = '1') then
        ret(a'length-i_mod) := '1';
      else
        ret(a'length-i_mod) := '0';
      end if;
    end loop;
    return ret;
  end function to_std_logic_vector;

  function read_init (
    constant ctxt : string)
    return nx_mem_t is
    variable idx : natural := 0;
    variable retval : nx_mem_t := (others => (others => '0'));
  begin  -- function read_init
    for i in 0 to 2047 loop -- it starts with the MSB of the word at addr 0
      idx := 1 + (i * 19);
      retval(i) := "000000" & to_std_logic_vector(ctxt(idx to idx + 17));
    end loop;  -- i
    return retval;
  end function read_init;

  shared variable mem : nx_mem_t := read_init(mem_ctxt);

begin  -- architecture behav

  process(ACK)
  begin
    if rising_edge(ACK) then
      if (AWE = '1') then
        mem(to_integer(unsigned(AA))) := AI;
      end if;
      AO <= mem(to_integer(unsigned(AA)));
    end if;
  end process;

  process(BCK)
  begin
    if rising_edge(BCK) then
      if (BWE = '1') then
        mem(to_integer(unsigned(BA))) := BI;
      end if;
      BO <= mem(to_integer(unsigned(BA)));
    end if;
  end process;

  AI(0) <= AI1;
  AI(1) <= AI2;
  AI(2) <= AI3;
  AI(3) <= AI4;
  AI(4) <= AI5;
  AI(5) <= AI6;
  AI(6) <= AI7;
  AI(7) <= AI8;
  AI(8) <= AI9;
  AI(9) <= AI10;
  AI(10) <= AI11;
  AI(11) <= AI12;
  AI(12) <= AI13;
  AI(13) <= AI14;
  AI(14) <= AI15;
  AI(15) <= AI16;
  AI(16) <= AI17;
  AI(17) <= AI18;
  AI(18) <= AI19;
  AI(19) <= AI20;
  AI(20) <= AI21;
  AI(21) <= AI22;
  AI(22) <= AI23;
  AI(23) <= AI24;

  AO1 <= AO(0);
  AO2 <= AO(1);
  AO3 <= AO(2);
  AO4 <= AO(3);
  AO5 <= AO(4);
  AO6 <= AO(5);
  AO7 <= AO(6);
  AO8 <= AO(7);
  AO9 <= AO(8);
  AO10 <= AO(9);
  AO11 <= AO(10);
  AO12 <= AO(11);
  AO13 <= AO(12);
  AO14 <= AO(13);
  AO15 <= AO(14);
  AO16 <= AO(15);
  AO17 <= AO(16);
  AO18 <= AO(17);
  AO19 <= AO(18);
  AO20 <= AO(19);
  AO21 <= AO(20);
  AO22 <= AO(21);
  AO23 <= AO(22);
  AO24 <= AO(23);

  AA(0) <= AA1;
  AA(1) <= AA2;
  AA(2) <= AA3;
  AA(3) <= AA4;
  AA(4) <= AA5;
  AA(5) <= AA6;
  AA(6) <= AA7;
  AA(7) <= AA8;
  AA(8) <= AA9;
  AA(9) <= AA10;
  AA(10) <= AA11;
  AA(11) <= AA12;
  AA(12) <= AA13;
  AA(13) <= AA14;
  AA(14) <= AA15;
  AA(15) <= AA16;

  BI(0) <= BI1;
  BI(1) <= BI2;
  BI(2) <= BI3;
  BI(3) <= BI4;
  BI(4) <= BI5;
  BI(5) <= BI6;
  BI(6) <= BI7;
  BI(7) <= BI8;
  BI(8) <= BI9;
  BI(9) <= BI10;
  BI(10) <= BI11;
  BI(11) <= BI12;
  BI(12) <= BI13;
  BI(13) <= BI14;
  BI(14) <= BI15;
  BI(15) <= BI16;
  BI(16) <= BI17;
  BI(17) <= BI18;
  BI(18) <= BI19;
  BI(19) <= BI20;
  BI(20) <= BI21;
  BI(21) <= BI22;
  BI(22) <= BI23;
  BI(23) <= BI24;

  BO1 <= BO(0);
  BO2 <= BO(1);
  BO3 <= BO(2);
  BO4 <= BO(3);
  BO5 <= BO(4);
  BO6 <= BO(5);
  BO7 <= BO(6);
  BO8 <= BO(7);
  BO9 <= BO(8);
  BO10 <= BO(9);
  BO11 <= BO(10);
  BO12 <= BO(11);
  BO13 <= BO(12);
  BO14 <= BO(13);
  BO15 <= BO(14);
  BO16 <= BO(15);
  BO17 <= BO(16);
  BO18 <= BO(17);
  BO19 <= BO(18);
  BO20 <= BO(19);
  BO21 <= BO(20);
  BO22 <= BO(21);
  BO23 <= BO(22);
  BO24 <= BO(23);

  BA(0) <= BA1;
  BA(1) <= BA2;
  BA(2) <= BA3;
  BA(3) <= BA4;
  BA(4) <= BA5;
  BA(5) <= BA6;
  BA(6) <= BA7;
  BA(7) <= BA8;
  BA(8) <= BA9;
  BA(9) <= BA10;
  BA(10) <= BA11;
  BA(11) <= BA12;
  BA(12) <= BA13;
  BA(13) <= BA14;
  BA(14) <= BA15;
  BA(15) <= BA16;

end architecture behav;
