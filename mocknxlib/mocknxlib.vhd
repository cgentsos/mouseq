library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package nxPackage is

  component nx_ram is
    generic (
      std_mode : string := "";  -- standard mode [FAST_2kx18, SLOW_2kx18, NOECNOECC24, ...] empty for raw
      mcka_edge : bit := '0';  -- 0: rising edge for port A clock - 1: falling edge
      mckb_edge : bit := '0';  -- 0: rising edge for port B clock - 1: falling edge
      pcka_edge : bit := '0';  -- 0: rising edge for pipe A clock - 1: falling edge
      pckb_edge : bit := '0';  -- 0: rising edge for pipe B clock - 1: falling edge
      mem_ctxt : string := "";          -- context initialization
      raw_config0 : bit_vector(3 downto 0) := B"0000";              -- PRC
      raw_config1 : bit_vector(15 downto 0) := B"0000000000000000"  -- MOD
      );
    port (
      ACK : in std_logic := '0';
      ACKC : in std_logic := '0';
      ACKD : in std_logic := '0';
      ACKR : in std_logic := '0';
      BCK : in std_logic := '0';
      BCKC : in std_logic := '0';
      BCKD : in std_logic := '0';
      BCKR : in std_logic := '0';

      AI1 : in std_logic := '0';
      AI2 : in std_logic := '0';
      AI3 : in std_logic := '0';
      AI4 : in std_logic := '0';
      AI5 : in std_logic := '0';
      AI6 : in std_logic := '0';
      AI7 : in std_logic := '0';
      AI8 : in std_logic := '0';
      AI9 : in std_logic := '0';
      AI10 : in std_logic := '0';
      AI11 : in std_logic := '0';
      AI12 : in std_logic := '0';
      AI13 : in std_logic := '0';
      AI14 : in std_logic := '0';
      AI15 : in std_logic := '0';
      AI16 : in std_logic := '0';

      AI17 : in std_logic := '0';
      AI18 : in std_logic := '0';
      AI19 : in std_logic := '0';
      AI20 : in std_logic := '0';
      AI21 : in std_logic := '0';
      AI22 : in std_logic := '0';
      AI23 : in std_logic := '0';
      AI24 : in std_logic := '0';

      BI1 : in std_logic := '0';
      BI2 : in std_logic := '0';
      BI3 : in std_logic := '0';
      BI4 : in std_logic := '0';
      BI5 : in std_logic := '0';
      BI6 : in std_logic := '0';
      BI7 : in std_logic := '0';
      BI8 : in std_logic := '0';
      BI9 : in std_logic := '0';
      BI10 : in std_logic := '0';
      BI11 : in std_logic := '0';
      BI12 : in std_logic := '0';
      BI13 : in std_logic := '0';
      BI14 : in std_logic := '0';
      BI15 : in std_logic := '0';
      BI16 : in std_logic := '0';

      BI17 : in std_logic := '0';
      BI18 : in std_logic := '0';
      BI19 : in std_logic := '0';
      BI20 : in std_logic := '0';
      BI21 : in std_logic := '0';
      BI22 : in std_logic := '0';
      BI23 : in std_logic := '0';
      BI24 : in std_logic := '0';

      ACOR : out std_logic := '0';
      AERR : out std_logic := '0';
      BCOR : out std_logic := '0';
      BERR : out std_logic := '0';

      AO1 : out std_logic := '0';
      AO2 : out std_logic := '0';
      AO3 : out std_logic := '0';
      AO4 : out std_logic := '0';
      AO5 : out std_logic := '0';
      AO6 : out std_logic := '0';
      AO7 : out std_logic := '0';
      AO8 : out std_logic := '0';
      AO9 : out std_logic := '0';
      AO10 : out std_logic := '0';
      AO11 : out std_logic := '0';
      AO12 : out std_logic := '0';
      AO13 : out std_logic := '0';
      AO14 : out std_logic := '0';
      AO15 : out std_logic := '0';
      AO16 : out std_logic := '0';

      AO17 : out std_logic := '0';
      AO18 : out std_logic := '0';
      AO19 : out std_logic := '0';
      AO20 : out std_logic := '0';
      AO21 : out std_logic := '0';
      AO22 : out std_logic := '0';
      AO23 : out std_logic := '0';
      AO24 : out std_logic := '0';

      BO1 : out std_logic := '0';
      BO2 : out std_logic := '0';
      BO3 : out std_logic := '0';
      BO4 : out std_logic := '0';
      BO5 : out std_logic := '0';
      BO6 : out std_logic := '0';
      BO7 : out std_logic := '0';
      BO8 : out std_logic := '0';
      BO9 : out std_logic := '0';
      BO10 : out std_logic := '0';
      BO11 : out std_logic := '0';
      BO12 : out std_logic := '0';
      BO13 : out std_logic := '0';
      BO14 : out std_logic := '0';
      BO15 : out std_logic := '0';
      BO16 : out std_logic := '0';

      BO17 : out std_logic := '0';
      BO18 : out std_logic := '0';
      BO19 : out std_logic := '0';
      BO20 : out std_logic := '0';
      BO21 : out std_logic := '0';
      BO22 : out std_logic := '0';
      BO23 : out std_logic := '0';
      BO24 : out std_logic := '0';

      AA1 : in std_logic := '0';
      AA2 : in std_logic := '0';
      AA3 : in std_logic := '0';
      AA4 : in std_logic := '0';
      AA5 : in std_logic := '0';
      AA6 : in std_logic := '0';

      AA7 : in std_logic := '0';
      AA8 : in std_logic := '0';
      AA9 : in std_logic := '0';
      AA10 : in std_logic := '0';
      AA11 : in std_logic := '0';
      AA12 : in std_logic := '0';
      AA13 : in std_logic := '0';
      AA14 : in std_logic := '0';
      AA15 : in std_logic := '0';
      AA16 : in std_logic := '0';

      ACS : in std_logic := '0';
      AWE : in std_logic := '0';
      AR : in std_logic := '0';

      BA1 : in std_logic := '0';
      BA2 : in std_logic := '0';
      BA3 : in std_logic := '0';
      BA4 : in std_logic := '0';
      BA5 : in std_logic := '0';
      BA6 : in std_logic := '0';

      BA7 : in std_logic := '0';
      BA8 : in std_logic := '0';
      BA9 : in std_logic := '0';
      BA10 : in std_logic := '0';
      BA11 : in std_logic := '0';
      BA12 : in std_logic := '0';
      BA13 : in std_logic := '0';
      BA14 : in std_logic := '0';
      BA15 : in std_logic := '0';
      BA16 : in std_logic := '0';

      BCS : in std_logic := '0';
      BWE : in std_logic := '0';
      BR : in std_logic := '0'
      );
  end component nx_ram;

  component NX_PLL is
    generic (
      vco_range : integer range 0 to 2 := 0;  -- 0 to 3
      ref_div_on : bit := '0';                -- bypass :: %2
      fbk_div_on : bit := '0';                -- bypass :: %2
      ext_fbk_on : bit := '0';                -- 0: disabled - 1: enabled

      fbk_intdiv : integer range 1 to 31 := 2;  -- 0 to 31  (%1 to %32)

      fbk_delay_on : bit := '0';               -- 0: no delay - 1: delay
      fbk_delay : integer range 0 to 63 := 0;  -- 0 to 63

      clk_outdiv1 : integer range 0 to 7 := 0;  -- 0 to 7   (%1 to %2^7)
      clk_outdiv2 : integer range 0 to 7 := 0;  -- 0 to 7   (%1 to %2^7)
      clk_outdiv3 : integer range 0 to 7 := 0   -- 0 to 7   (%1 to %2^7)
      );
    port (
      REF : in std_logic := '0';
      FBK : in std_logic := '0';

      VCO : out std_logic := '0';

      D1 : out std_logic := '0';
      D2 : out std_logic := '0';
      D3 : out std_logic := '0';
      OSC : out std_logic := '0';

      RDY : out std_logic := '0'
      );
  end component NX_PLL;

  component NX_WFG is
    generic (
      wfg_edge : bit := '0';            -- 0: no invert / Rising
                                    -- 1:    invert / Falling

      mode : bit := '0';                -- 0: no pattern - 1: pattern
      pattern_end : integer range 0 to 15 := 1;  -- 0: to 15 (1 step to 16 steps)
      pattern : bit_vector(0 to 15) := (others => '0');  -- pattern p0 ... p15

      delay_on : bit := '0';              -- 0: no delay - 1: delay
      delay : integer range 0 to 63 := 0  -- 0 to 63 (1 unit to 64 unit)
      );
    port (
      SI : in std_logic := '0';
      ZI : in std_logic := '0';
      RDY : in std_logic := '1';
      SO : out std_logic := '0';
      ZO : out std_logic := '0'
      );
  end component NX_WFG;


end package nxPackage;
