#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import filecmp
from makemseq import main

class TestApp(unittest.TestCase):
    def setUp(self):
        return True

    def test_nanoxplore_script(self):
        self.statusOK = main('nanoxplore.mseq', 'tests/nanoxplore_pkg.vhd', 'tests/nanoxplore_pgm.mif', True)
        self.assertEqual(self.statusOK, 0)

        self.pkgOK = filecmp.cmp('tests/nanoxplore_pkg_golden.vhd', 'tests/nanoxplore_pkg.vhd')
        self.assertEqual(self.pkgOK, True)

        self.pgmOK = filecmp.cmp('tests/nanoxplore_pgm_golden.mif', 'tests/nanoxplore_pgm.mif')
        self.assertEqual(self.pgmOK, True)

    def test_broken_nanoxplore_scripts(self):
        self.statusOK = main('tests/nanoxplore_broken1.mseq', 'tests/broken_nanoxplore_pkg.vhd', 'tests/broken_nanoxplore_pgm.mif', True)
        self.assertEqual(self.statusOK, 1)

        self.statusOK = main('tests/nanoxplore_broken2.mseq', 'tests/broken_nanoxplore_pkg.vhd', 'tests/broken_nanoxplore_pgm.mif', True)
        self.assertEqual(self.statusOK, 1)

        self.statusOK = main('tests/nanoxplore_broken3.mseq', 'tests/broken_nanoxplore_pkg.vhd', 'tests/broken_nanoxplore_pgm.mif', True)
        self.assertEqual(self.statusOK, 1)

        self.statusOK = main('tests/nanoxplore_broken4.mseq', 'tests/broken_nanoxplore_pkg.vhd', 'tests/broken_nanoxplore_pgm.mif', True)
        self.assertEqual(self.statusOK, 1)


def run_suite():
    suite = unittest.TestSuite()
    suite.addTests(
        unittest.TestLoader().loadTestsFromTestCase(TestApp)
    )

    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(run_suite())
