#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
## Title    : MOUSEQ (Uncomplicated MOnitoring SEQuencer)
## Project  : Distributed I/O Tier
###############################################################################
## File       : makemouseq.py
## Author     : Christos Gentsos <christos.gentsos@cern.ch>
## Company    : CERN (BE-CO-HT)
## Standard   : Python / VHDL (generated)
###############################################################################
## Description:
## This is the assembler for the mouseq IP core. It generates a memory
## initialization file (in .mif format) and a VHDL package to configure
## the main component.
###############################################################################
##
## Copyright (c) 2019-2020 CERN
##
## Copyright and related rights are licensed under the Solderpad Hardware
## License, Version 0.51 (the “License”) (which enables you, at your option,
## to treat this file as licensed under the Apache License 2.0); you may not
## use this file except in compliance with the License. You may obtain a copy
## of the License at http://solderpad.org/licenses/SHL-0.51.
## Unless required by applicable law or agreed to in writing, software,
## hardware and materials distributed under this License is distributed on an
## “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
## or implied. See the License for the specific language governing permissions
## and limitations under the License.
##
###############################################################################

import argparse
import re
import os
import math
from enum import Enum, auto


##############################################
## constant and global variable definitions ##
##############################################

class MsgType(Enum):
    """Enumeration for message types"""
    ERROR = auto()
    WARNING = auto()
    INFO = auto()


class InstructionSet(Enum):
    """Enumeration for all possible instruction names"""
    WRITE = auto()        # write instruction
    WRITE_NS = auto()     # write instruction (without STOP)
    WRITE_NF = auto()     # write w/o command word
    WRITE_NA = auto()     # write data, w/o device address or STOP
    READ = auto()         # read instruction
    READ_NS = auto()      # read instruction (without STOP)
    READ_NF = auto()      # read w/o command word (blk w, blk r, proc call)
    READ_PNT = auto()     # read instruction, reg points to dev address
    STOP = auto()         # stop instruction
    GOTO = auto()         # go to label
    GOTO_JB0 = auto()     # conditional go to: on jump bit not set
    GOTO_JB1 = auto()     # conditional go to: on jump bit set
    WAIT_US = auto()      # wait instruction (us)
    WAIT_MS = auto()      # wait instruction (ms)
    WAIT_TRIG = auto()    # wait on external trigger
    MOV = auto()          # copy between whole registers
    MOV_B = auto()        # copy register bytes
    LOAD = auto()         # load to register byte
    ADD = auto()          # add to register byte
    SUB = auto()          # subtract from register byte
    AND = auto()          # bitwise AND with register byte
    OR = auto()           # bitwise OR with register byte
    INC = auto()          # increment register byte
    DEC = auto()          # decrement register byte
    SHL = auto()          # shift register byte to the left
    SHR = auto()          # shift register byte to the right
    ROL = auto()          # rotate register byte to the left
    ROR = auto()          # rotate register byte to the right
    NOT = auto()          # bitwise not
    MINUS = auto()        # signed negation
    SYNCIO = auto()       # sync register file with I/O
    CALL = auto()         # save pc, call function
    CALL_JB0 = auto()     # save pc, call function (conditional)
    CALL_JB1 = auto()     # save pc, call function (conditional)
    RET = auto()          # return from function to saved pc
    NOP = auto()          # NOP instruction


class ReservedWords(Enum):
    """Enumeration of reserved words"""
    LABEL = auto()        # no particular reason, mostly a placeholder


class ArgumentType(Enum):
    """Enumeration of allowed argument types"""
    LITERAL = auto()      # it's a literal
    REGISTER = auto()     # it's a register
    LABEL = auto()        # it points to a label


class RegisterType(Enum):
    """Enumeration of the three register types"""
    INPUT = auto()
    OUTPUT = auto()
    INTERNAL = auto()


class Register:
    """Class representing a register object"""
    # register name
    name = ''

    # register type, can be input, output or internal.
    reg_type = RegisterType.INPUT

    # A position, in bits, is assigned to each non-internal register.
    position = -1

    # A size, in bytes, is assigned to each register.
    size = 1


class Instruction:
    """An instruction object"""
    words_dict = {
        InstructionSet.WRITE.name: 2,
        InstructionSet.WRITE_NS.name: 2,
        InstructionSet.WRITE_NF.name: 2,
        InstructionSet.WRITE_NA.name: 2,
        InstructionSet.READ.name: 2,
        InstructionSet.READ_NS.name: 2,
        InstructionSet.READ_NF.name: 2,
        InstructionSet.READ_PNT.name: 2,
        InstructionSet.LOAD.name: 2,
        InstructionSet.ADD.name: 2,
        InstructionSet.SUB.name: 2,
        InstructionSet.AND.name: 2,
        InstructionSet.OR.name: 2,
        InstructionSet.CALL.name: 3,
        InstructionSet.CALL_JB0.name: 3,
        InstructionSet.CALL_JB1.name: 3,
    }
    def set_name(self, name):
        """Set the instruction name, return False if invalid."""
        self.name = name.upper()
        if self.name in [a.name for a in InstructionSet]:
            return True
        return False


    def update_words(self):
        """Update the instruction length."""
        self.words = self.words_dict.get(self.name, 1)
        if self.name == InstructionSet.MOV.name:
            self.words = self.arguments[0].size


    def tostr(self):
        """Convert to string format for reporting."""
        return self.name + ' ' + ', '.join(map(str, self.arguments))


    # instruction name
    name = ''

    # argument list
    arguments = []

    # number of words for multi-word instructions
    words = 1

    # program counter
    pc_orig = -1

    # actual program counter, corrected for multi-word instructions
    pc = -1


class Argument:
    """Argument of an instruction"""
    def __str__(self):
        if self.arg_type == ArgumentType.LITERAL:
            return str(self.value)
        if self.arg_type == ArgumentType.REGISTER:
            if self.reg_type == RegisterType.INPUT:
                prefix = 'I'
            elif self.reg_type == RegisterType.OUTPUT:
                prefix = 'O'
            else:
                prefix = 'i'
            return prefix + '(' + str(self.value) + ',' + str(self.size) + ')'
        return 'L' + str(self.value)


    # the argument type. if it's not a register, it's a literal...
    arg_type = ArgumentType.LITERAL

    # if it's a literal or a label, its value
    value = -1

    # if it's a register, its type
    reg_type = RegisterType.INPUT

    # and its size
    size = -1


params = {}               # list of general parameters
jump_fix_dict = {}        # dictionary to repair jumps
registers = []            # list of registers
instructions = []         # list of instructions
consts = {}               # dictionary for user consts
labels = {}               # dictionary for labels
program_counter = 0       # the program counter used to set the labels
int_reg_pos = 0           # counter to arrange internal registers
is_verbose = 0            # verbosity flag
exit_status = 0           # exit status


def init_global_vars():
    """Initialize the global variables. To be run from main()."""

    global params
    params = {
        'OWORDS': 0,                 # output port width, default=0
        'IWORDS': 0,                 # input port width, default=0
        'I2C_CLKS': 0x80,            # I2C clock prescale, SCL*4
        'BUFFERIO': 0,               # buffer I/O ports, default=0
        'BIG_ENDIAN': 0,             # endianness for multi-byte transfers
        'ERR_SET_JUMP': 1,           # i2c ack errors will set / clear the jump bit
        'ZERO_SET_JUMP': 1,          # ALU result = 0 will set the jump bit
        'FAMILY': 'PROASIC3',        # device family, for mem implementation
        'SIMPLE_PROGMEM': 0,         # simple program memory (more resources @TMR)
        'SIMPLE_REGFILE': 0,         # simple register file (more resources)
        'TMR': 0,                    # set the syn_radhardlevel TMR attribute
        'ESSENTIALS_ONLY': 0,        # no NOP, MOV, ALU, LOAD, STOP or WAIT_MS
        'WATCHDOG': 0,               # enable watchdog and set timeout
        'WATCHDOG_RST_CYCLES': 10,   # how many rst cycles the watchdog should hold
        'RESET_START_FROM': '',      # at reset jump to an address to skip init
    }

    global jump_fix_dict
    jump_fix_dict = {}

    global registers
    registers = []

    global instructions
    instructions = []

    global consts
    consts = {}

    global labels
    labels = {}

    global program_counter
    program_counter = 0

    global int_reg_pos
    int_reg_pos = 0

    global exit_status
    exit_status = 0

    global is_verbose
    is_verbose = 0


WORD_WIDTH = 8            # register width
NX_MEM_SIZE = 2048        # NX_MEM size
ERRLINE_OFFSET = 0        # offset to apply to code in error lines

# ALU1 instructions and opcodes
ALU1_INSNS = set([
    InstructionSet.ADD.name,
    InstructionSet.SUB.name,
    InstructionSet.AND.name,
    InstructionSet.OR.name,
])

ALU1_INS_BITS = {
    'INC': '000',
    'DEC': '001',
    'SHL': '010',
    'SHR': '011',
    'ROL': '100',
    'ROR': '101',
    'NOT': '110',
    'MINUS': '111',
}

# ALU2 instructions and opcodes
ALU2_INSNS = set([
    InstructionSet.INC.name,
    InstructionSet.DEC.name,
    InstructionSet.SHL.name,
    InstructionSet.SHR.name,
    InstructionSet.ROL.name,
    InstructionSet.ROR.name,
    InstructionSet.NOT.name,
    InstructionSet.MINUS.name,
])

ALU2_INS_BITS = {
    'ADD': '000',
    'SUB': '001',
    'AND': '010',
    'OR': '011',
}


###############################
## error and warning strings ##
###############################

ERR_EVALUATE = 'Error while evaluating expression.'
ERR_INS_NAME = 'Invalid instruction.'
ERR_INS_DISABLED = 'Instruction disabled by param ESSENTIALS_ONLY.'
ERR_INS_ARGUMENTS_WRONG_NUMBER = 'Wrong number of arguments.'
ERR_INS_ARGUMENT_WRONG_TYPE = 'Wrong argument type.'
ERR_INS_ARGUMENTS_BIG_LITERAL = 'Literal value outside 8-bit range.'
ERR_INS_ARGUMENTS_BIG_10b_LITERAL = 'Literal value outside 10-bit range.'
ERR_INS_ARGUMENT_WRONG_SIZE = 'Wrong register size.'
ERR_INS_TOO_BIG = 'Trying to emit an instruction bigger than 16 bits, ' \
'something\'s wrong.'
ERR_MOV_SIZE_DISAGREEMENT = 'Size disagreement in MOV instruction.'
ERR_ARG_DECODING = 'Error while decoding instruction argument.'
ERR_INVALID_CONST = 'Invalid const assignment.'
ERR_RESERVED_WORD_CONST = 'using a reserved word as a const name.'
ERR_REG_REDEFINITION = 'Register already defined.'
ERR_OUTPUT_DOUBLE_ASSIGNMENT = 'output bit assigned twice.'
ERR_OUTPUT_GAP = 'unassigned output bit'
ERR_OUTPUT_OUT_OF_RANGE = 'output bit out of range'
ERR_INPUT_DOUBLE_ASSIGNMENT = 'input bit assigned twice.'
ERR_INPUT_GAP = 'unassigned input bit'
ERR_INPUT_OUT_OF_RANGE = 'input bit out of range'
ERR_WATCHDOG_RESET_OUT_OF_RANGE = 'the number of reset cycles cannot be ' \
'larger than the watchdog threshold'
ERR_LABEL_UNDEFINED = 'label undefined'
ERR_IMPROPER_REG_DEF = 'improper register definition'

WARN_ROUNDING = 'There was some rounding here.'

MSG_IGNORE_CONTINUE = 'I\'ll try to continue though...'


########################################
## constants handling and some simple ##
## code pre-processing                ##
########################################

def print_input(line_list):
    """Helper function that prints the program"""
    print('Input code:')
    print('-----------')
    for code_line in line_list:
        print(code_line)
    print('-----------')
    print('')


def print_instructions():
    """Helper function to print the program, instruction by instruction."""
    print('Input code:')
    print('-----------')
    for ins in instructions:
        print(f'instruction: {ins.name} at {ins.pc_orig} (actually {ins.pc})')
        if ins.words > 1:
            print(f'{ins.words}-words instruction')
        print(f'arguments:')
        for arg in ins.arguments:
            if arg.arg_type == ArgumentType.REGISTER:
                dir_str = arg.reg_type.name.lower()
                print(f'  {dir_str} reg {str(arg)} at {arg.value}, size={arg.size}')
            elif arg.arg_type == ArgumentType.LABEL:
                print(f'  label, value={arg.value}')
            else:
                print(f'  literal, value={arg.value}')
    print('-----------')
    print('')


def preprocess_basic(code_lines):
    """Basic pre-processing"""

    # remove comments
    code_lines = list(map(lambda a: re.sub(r'#.*', '', a), code_lines))

    # strip leading and trailing whitespace from each line
    code_lines = list(map(str.strip, code_lines))

    # remove empty lines
    code_lines = list(filter(None, code_lines))

    # strip excess whitespace
    code_lines = list(map(lambda a: re.sub(r' +', r' ', a), code_lines))

    # make lowercase
    # code_lines = list(map(str.lower, code_lines))

    return code_lines


def print_error(err_string, line='', will_continue=False, severity=MsgType.ERROR):
    """Utility function to print errors and set exit status"""
    global exit_status
    if severity == MsgType.ERROR:
        exit_status = 1

    print()
    print('~'*60)
    if line:
        print(' '*ERRLINE_OFFSET+str(line))
        print(f'    ^^^ {severity.name.upper()}: {err_string} ^^^')
    else:
        print(f'    {severity.name.upper()}: {err_string}')
    if will_continue:
        print('           ' + MSG_IGNORE_CONTINUE)
    print('~'*60)


def print_verbose(what, **kwargs):
    """Print something only if verbosity flag is on."""
    if is_verbose:
        print(what, **kwargs)


def print_dict(indict):
    """Utility function to print a dictionary line-by-line"""
    for key, val in indict.items():
        print(f'{key} is set to {val}.')


def check_inputs_outputs():
    """this checks the input and output bit assignments for any double
    assignments or gaps.
    """
    retval = True

    # start without any assignments
    outbit_assigned = [False]*params['OWORDS']*WORD_WIDTH
    inbit_assigned = [False]*params['IWORDS']*WORD_WIDTH

    # loop over our different bit widths, providing their indices
    for reg in registers:
        # check if it's an TMP (internal) register
        if reg.reg_type != RegisterType.INTERNAL:
            # loop over its individual bits
            for r_iter in range(reg.position*WORD_WIDTH,
                                reg.position*WORD_WIDTH + reg.size*WORD_WIDTH):

                isout = (reg.reg_type == RegisterType.OUTPUT)

                # alias the right array depending on input / output
                if isout:
                    inoutbit_assigned = outbit_assigned
                else:
                    inoutbit_assigned = inbit_assigned

                # out of bounds check
                if r_iter >= len(inoutbit_assigned):
                    if isout:
                        print_error(ERR_OUTPUT_OUT_OF_RANGE,
                                    r_iter, 1)
                    else:
                        print_error(ERR_INPUT_OUT_OF_RANGE,
                                    r_iter, 1)
                    retval = False

                # already assigned elsewhere check
                elif inoutbit_assigned[r_iter]:
                    if isout:
                        print_error(ERR_OUTPUT_DOUBLE_ASSIGNMENT,
                                    r_iter, 1)
                    else:
                        print_error(ERR_INPUT_DOUBLE_ASSIGNMENT,
                                    r_iter, 1)
                    retval = False

                # in bounds, not assigned before, ok let's allocate the bit
                else:
                    inoutbit_assigned[r_iter] = True

    # check for unconnected output bits
    for (i, bit) in enumerate(outbit_assigned):
        if not bit:
            print_error(ERR_OUTPUT_GAP, i, 0)
            retval = False

    # check for unconnected input bits
    for (i, bit) in enumerate(inbit_assigned):
        if not bit:
            print_error(ERR_INPUT_GAP, i, 0)
            retval = False

    print('Input and output bits checked for double assignments or gaps.')
    return retval


def update_instruction_register_positions():
    """this looks up for registers used in the program's instructions and
       replaces their addresses with the proper (HW) ones (input first,
       then output, then internal).
    """
    for ins in instructions:
        for insarg in ins.arguments:
            if insarg.arg_type == ArgumentType.REGISTER:
                if insarg.reg_type == RegisterType.OUTPUT:
                    print_verbose('Moving output reg from {:d}'.format(insarg.value), end=' ')
                    insarg.value += params['IWORDS']
                    print_verbose('to {:d}'.format(insarg.value))
                elif insarg.reg_type == RegisterType.INTERNAL:
                    print_verbose('moved internal reg from {:d}'.format(insarg.value), end=' ')
                    insarg.value += params['IWORDS'] + params['OWORDS']
                    print_verbose('to {:d}'.format(insarg.value))


def print_regs():
    """Utility function to print the register definitions"""
    for reg in registers:
        # is the register internal or I/O?
        if reg.reg_type == RegisterType.INTERNAL:
            print(f'Register {reg.name} declared as internal, width = {reg.size}')
        else:
            pos = reg.position*WORD_WIDTH
            width = reg.size*WORD_WIDTH
            inputoutput = reg.reg_type.name.lower()
            print(f'Register {reg.name} is assigned to {inputoutput} bits '
                  f'{pos+width-1} downto {pos}.')


def is_const_line(line):
    """If the line matches the const declaration prototype, return True"""
    return re.match('.*=.*', line)


def is_label_line(line):
    """If the line matches the label syntax, return True"""
    return re.match('.*:$', line)


def is_regdef_line(line):
    """If the line matches the register definition syntax, return True"""
    return re.match('.*:.+$', line)


def update_consts_in_expression(line):
    """Find if an code line expression contains a constant and replace it
       with its value. If the code line is a constant definition
       itself, be careful to only replace the right-hand side.
    """
    if consts:       # only substitute when there are constants to use
        if is_const_line(line): # constant definition, only touch the right side
            start_char = re.search('=', line).start(0)
        else:
            start_char = 0

        # construct a regexp that would match any of the defined consts
        consts_regexp_string = r'(\s*)('
        for const in consts:
            consts_regexp_string += '('
            consts_regexp_string += str(const).lower()
            consts_regexp_string += ')|'
        consts_regexp_string = consts_regexp_string[:-1] + r')($|\W+)'

        # only touch the right side of the assignment
        line = line[:start_char] + \
            re.sub(consts_regexp_string,
                   lambda a: a.group(1) +
                   '(' + str(consts[a.group(2).lower()]) + ')' +
                   a.group(a.lastindex),
                   line[start_char:], flags=re.I)

    return line


def kindasafe_eval(what):
    """This is not air-tight, security-wise, but it should nevertheless
       prevent any shenanigans.
    """
    try:
        # if it's a family name don't try to process that
        if ((what.upper() == 'NX') or
                (what.upper() == 'PROASIC3') or
                (what.upper() == 'XILINX') or
                (what.upper() == 'ALTERA')):
            return what

        retval = eval(what, {"__builtins__": None}, {})
        if int(retval) != retval:
            print_error(WARN_ROUNDING, what, severity=MsgType.WARNING)
        retval = int(retval)
        return retval
    except:
        print_error(ERR_EVALUATE, what)
        return what


def parse_const(line):
    """This function will check if a code line is a const assignment and,
       if it is, parse it.

       The function returns the processed line itself if the line has
       been processed correctly, and False if it hasn't.
    """
    # that's a label line or a register definition line, in any case
    # it's not a const assignment, return
    if is_label_line(line) or is_regdef_line(line) or not is_const_line(line):
        return line

    # in case the value uses existing constants, substitute them recursively,
    # adding parentheses around any substitution
    line = update_consts_in_expression(line)

    # regexp to match the left-hand and right-hand sides of the assignment
    const_assign_match_str = r'([\w]+)\s*=\s*(.+)'
    mymatch = re.match(const_assign_match_str, line)
    if mymatch:
        const_name = mymatch.group(1)
        # the right hand side may have some expressions and/or it may depend on
        # other constants (if so, these have been substituted with parentheses
        # added, need to evaluate them away). In any case, evaluate the
        # expression here. If it is the RESET_START_FROM param, defer eval.
        if const_name != 'RESET_START_FROM':
            const_val = kindasafe_eval(mymatch.group(2))
        else:
            const_val = mymatch.group(2)
        line = re.sub(const_assign_match_str,
                      lambda a: a.group(1) + ' = ' + str(const_val), line)
    else:
        print_error(ERR_INVALID_CONST, line, 1)  # syntax error, fail
        return False

    # special match for setting parameter values
    mymatch = re.match('(' + '|'.join(params.keys()) + ')$', const_name.upper(), flags=re.I)
    if mymatch:
        print_verbose(f'setting param {mymatch.group(1)} to {const_val}')
        params[mymatch.group(1)] = const_val

        # simple program memory is the default for all families except PROASIC3
        if ((mymatch.group(1) == 'FAMILY') and (const_val != 'PROASIC3')):
            params['SIMPLE_PROGMEM'] = 1

        return line             # got it, return

    # check for the user trying to set any reserved words
    for reserved in list(InstructionSet) + list(ReservedWords):
        mymatch = re.match('^' + reserved.name + '$', const_name.upper(), flags=re.I)
        if mymatch:
            print_error(ERR_RESERVED_WORD_CONST, line, 1)
            return False        # syntax error, fail

    # none of the above special cases, just a user constant
    print_verbose(f'setting user constant "{const_name}" to {const_val}')
    consts[const_name.lower()] = const_val

    return line                 # user const set, return


def parse_regdef(line):
    """This function will check if a code line defines a register."""
    # that's a label line, in any case it's not a register definition
    # line, return
    if is_label_line(line) or not is_regdef_line(line):
        return line

    # I/O register declaration
    mymatch = re.match(r'(\w+):\s*(\w+)\s+(\d+)\s*.*$', line)
    mymatch2 = re.match(r'(\w+):\s*(\w+)\s+(\d+)\s*,\s*(\d+)$', line)
    if mymatch and (mymatch.group(2).upper() != "INT"):
        global registers
        newregname = mymatch.group(1)

        for reg in registers:
            if newregname == reg.name:
                print_error(ERR_REG_REDEFINITION, line)
                return False

        registers += [Register()]
        registers[-1].name = newregname

        # match register direction
        if mymatch.group(2).upper() == "IN":
            newregdir = "input"
            registers[-1].reg_type = RegisterType.INPUT
        elif mymatch.group(2).upper() == "OUT":
            newregdir = "output"
            registers[-1].reg_type = RegisterType.OUTPUT
        else:
            print_error(ERR_IMPROPER_REG_DEF, line, 1)
            return False

        # match I/O bit position
        newregpos = int(mymatch.group(3))
        registers[-1].position = newregpos

        newregsize = 1
        # match register size, if provided
        if mymatch2:
            newregsize = int(mymatch2.group(4))
            registers[-1].size = newregsize

        print_verbose(f'defined register "{newregname}" as {newregdir} '
                      f'at bits {newregpos+newregsize*WORD_WIDTH-1} '
                      f'downto {newregpos}')
        return line

    # internal register declaration
    mymatch = re.match(r'(\w+):\s*INT.*$', line.upper(), re.I)
    mymatch2 = re.match(r'(\w+):\s*INT\s+(\d+)\s*$', line.upper(), re.I)
    if mymatch:
        newregname = mymatch.group(1)

        for reg in registers:
            if newregname == reg.name:
                print_error(ERR_REG_REDEFINITION, line)
                return False

        registers += [Register()]
        registers[-1].name = newregname
        registers[-1].reg_type = RegisterType.INTERNAL

        newregsize = 1
        if mymatch2:
            newregsize = int(mymatch2.group(2))
            registers[-1].size = newregsize

        global int_reg_pos      # use and increment internal register counter
        registers[-1].position = int_reg_pos
        int_reg_pos += newregsize

        print_verbose(f'defined internal register "{newregname}" of size '
                      f'{newregsize}')
        return line

    # if no proper syntax matched
    print_error(ERR_IMPROPER_REG_DEF, line, 1)
    return False


def parse_label(line):
    """This function will check if a code line defines a label and, if it
       is, parse it.
    """
    # empty line, return (matters for being able to use this with filter)
    if not line:
        return line

    # that's a register definition line or a const assignment, in any
    # case it's not a label line, return
    if is_regdef_line(line) or is_const_line(line) or not is_label_line(line):
        return line

    mymatch = re.match(r'(\w+):$', line)
    if mymatch:
        newlabelname = mymatch.group(1)
        print_verbose(f'setting label "{newlabelname}" to point to '
                      f'{program_counter}')
        labels[newlabelname] = program_counter

    return line


def update_and_eval_if_number(expr):
    """Utility function to be used in argument list comprehension inside
       parse_instruction
    """
    # look for any constants to substitute
    expr = update_consts_in_expression(expr)

    # eval if there are no words (only numbers, including 0x and 0b notation)
    if not re.match(r'(^|\W+)[a-zA-Z_]\w*', expr):
        expr = kindasafe_eval(expr)

    return expr


def arg_from_str(arg_str):
    """Utility function to be used in argument list comprehension inside
       parse_instruction. Assumes update_and_eval_if_number has already been ran.
    """
    retval = Argument()

    # check if it's a literal
    mymatch = re.match(r'(\d+)', str(arg_str))
    if mymatch:
        # it is, set its value and return
        retval.arg_type = ArgumentType.LITERAL
        retval.value = int(mymatch.group(1))
        return retval

    # it's not, look in the register definitions
    for reg in registers:
        if reg.name.upper() == arg_str.upper():
            # found it, set the parameters right
            retval.arg_type = ArgumentType.REGISTER
            retval.reg_type = reg.reg_type
            retval.value = reg.position
            retval.size = reg.size
            return retval

    # it's not a reg either, look in the label definitions
    if arg_str in labels.keys():
        # found it, set the parameters right
        retval.arg_type = ArgumentType.LABEL
        retval.value = int(labels[arg_str])
        return retval

    # it's not there either, that's not right.
    print_error(ERR_ARG_DECODING, arg_str)
    return False


def parse_instruction(line):
    """Parse an instruction line and return it, after substituting any
       constants, in the form of a list of tokens."""

    # empty line, return (matters for being able to use this with filter)
    if not line:
        return line

    # that's a label line or a register definition or a const
    # assignment, return
    if is_label_line(line) or is_regdef_line(line) or is_const_line(line):
        return line

    # separate the first word, which is the instruction
    char_after_instruction = re.search(r'(\W|$)', line).end(0) # eat whitespace
    instruction = line[:char_after_instruction].strip() # strip whitespace

    # split the (comma-separated) arguments
    argument_list = filter(None, re.split(
        r'\s*,\s*', line[char_after_instruction:]))

    # process them (substitute consts, evaluate expressions)
    argument_list = list(map(update_and_eval_if_number, argument_list))

    # reconstruct the line after everything has been evaluated
    line = instruction + ' ' + ', '.join(map(str, argument_list))

    # add to the global list of instructions
    global instructions
    instructions += [Instruction()]
    if not instructions[-1].set_name(instruction):
        print_error(ERR_INS_NAME, line)
        return False
    instructions[-1].arguments = list(map(arg_from_str, argument_list))
    instructions[-1].update_words()

    global program_counter
    instructions[-1].pc_orig = program_counter
    program_counter += 1        # don't forget to increment the PC

    return line


def fix_pcs():
    """Utility function that fixes of the program counter to repair jumps,
       as there are some instructions that are many words long.
    """
    offset = 0
    for ins in instructions:
        ins.pc = ins.pc_orig + offset

        # populate jump fix dictionary
        jump_fix_dict[ins.pc_orig] = ins.pc

        # adjust offset
        if ins.words > 1:
            offset += ins.words - 1


def check_lits_valid(ins):
    """Check if literal arguments are valid."""
    if (ins.name == InstructionSet.WAIT_US.name or
        ins.name == InstructionSet.WAIT_MS.name):
        maxlit = 10
    else:
        maxlit = 8

    for arg in ins.arguments:
        if arg.arg_type == ArgumentType.LITERAL:
            if (arg.value < 0) or (maxlit == 8 and arg.value > 255):
                # error for literals that are 8-bit unsigned
                print_error(ERR_INS_ARGUMENTS_BIG_LITERAL, ins.tostr())
                return False
            elif (arg.value < 0) or (maxlit == 10 and arg.value > 1023):
                # error for literals that are 10-bit unsigned
                print_error(ERR_INS_ARGUMENTS_BIG_10b_LITERAL, ins.tostr())
                return False

    # all good
    return True


def check_arg_num(ins, num):
    """Check the number of arguments."""
    if len(ins.arguments) != num:
        print_error(ERR_INS_ARGUMENTS_WRONG_NUMBER, ins.tostr())
        return False

    # all good
    return True


def check_arg_num_between(ins, low, high):
    """Check if the number of arguments is between low and high
    (inclusive)."""
    if (len(ins.arguments) < low) or (len(ins.arguments) > high):
        print_error(ERR_INS_ARGUMENTS_WRONG_NUMBER, ins.tostr())
        return False

    # all good
    return True


def check_write_ins_args(ins):
    """Check the arguments for the WRITE ins."""
    if not check_arg_num(ins, 3):
        return False

    # first two are literals
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # third one can be either a register or a literal
    if ins.arguments[2].arg_type == ArgumentType.LABEL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_write_nf_ins_args(ins):
    """Check the arguments for the WRITE_NF ins."""
    if not check_arg_num(ins, 2):
        return False

    # first one is a literal
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # second one can be either a register or a literal
    if ins.arguments[1].arg_type == ArgumentType.LABEL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_write_na_ins_args(ins):
    """Check the arguments for the WRITE_NA ins."""
    if not check_arg_num(ins, 1):
        return False

    # the one and only argument can be either a register or a literal
    if ins.arguments[0].arg_type == ArgumentType.LABEL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_read_ins_args(ins):
    """Check the arguments for the READ ins."""
    if not check_arg_num(ins, 3):
        return False

    # first two are literals
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # third one is a register
    if ins.arguments[2].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_read_nf_ins_args(ins):
    """Check the arguments for the READ_NF ins."""
    if not check_arg_num(ins, 2):
        return False

    # first one is a literal
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # second one is a register
    if ins.arguments[1].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_read_pnt_ins_args(ins):
    """Check the arguments for the READ_PNT ins."""
    if not check_arg_num(ins, 3):
        return False

    # first two are literals
    if ins.arguments[0].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[0].size != 1:
        print_error(ERR_INS_ARGUMENT_WRONG_SIZE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # third one is a register
    if ins.arguments[2].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[2].size != 1:
        print_error(ERR_INS_ARGUMENT_WRONG_SIZE, ins.tostr())
        return False

    # all good
    return True


def check_on_not_essential(ins):
    """Check if non-essential instructions are disabled."""
    if params['ESSENTIALS_ONLY']:
        print_error(ERR_INS_DISABLED, ins.tostr())
        return False

    # all good
    return True


def check_goto_ins_args(ins):
    """Check the arguments for the GOTO ins."""
    # one argument
    if len(ins.arguments) != 1:
        print_error(ERR_INS_ARGUMENTS_WRONG_NUMBER, ins.tostr())
        return False

    # it's a label
    if ins.arguments[0].arg_type != ArgumentType.LABEL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_wait_ins_args(ins):
    """Check the arguments for the WAIT ins."""
    # one argument
    if len(ins.arguments) != 1:
        print_error(ERR_INS_ARGUMENTS_WRONG_NUMBER, ins.tostr())
        return False

    # time is a literal
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_mov_ins_args(ins):
    """Check the arguments for the MOV ins."""
    # check argument number is right
    if not check_arg_num(ins, 2):
        return False

    # both are registers
    if ins.arguments[0].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # they should have the same size
    if ins.arguments[0].size != ins.arguments[1].size:
        print_error(ERR_MOV_SIZE_DISAGREEMENT, ins.tostr())
        return False

    # all good
    return True


def check_mov_b_ins_args(ins):
    """Check the arguments for the MOV_B ins."""
    # check argument number is right
    if not check_arg_num(ins, 3):
        return False

    # first two are registers
    if ins.arguments[0].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # third one is a literal (the offset)
    if ins.arguments[2].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_lit_reg_optlit_args(ins):
    """Check the arguments for LITERAL - REG - [LITERAL] insns."""
    # check argument number is right
    if not check_arg_num_between(ins, 2, 3):
        return False

    # register / literal (offset)
    if ins.arguments[0].arg_type != ArgumentType.LITERAL:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ins.arguments[1].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ((len(ins.arguments) == 3) and
            (ins.arguments[2].arg_type != ArgumentType.LITERAL)):
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_reg_optlit_args(ins):
    """Check the arguments for REG - [LITERAL] insns."""
    # check argument number is right
    if not check_arg_num_between(ins, 1, 2):
        return False

    # register / literal (offset)
    if ins.arguments[0].arg_type != ArgumentType.REGISTER:
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False
    if ((len(ins.arguments) == 2) and
            (ins.arguments[1].arg_type != ArgumentType.LITERAL)):
        print_error(ERR_INS_ARGUMENT_WRONG_TYPE, ins.tostr())
        return False

    # all good
    return True


def check_no_args(ins):
    """Verify that an instruction was called with no arguments."""
    # this takes no arguments
    return check_arg_num(ins, 0)


def check_instruction_args(ins):
    """Check instruction arguments."""
    select_checker = {
        InstructionSet.WRITE.name:     check_write_ins_args,
        InstructionSet.WRITE_NS.name:  check_write_ins_args,
        InstructionSet.WRITE_NF.name:  check_write_nf_ins_args,
        InstructionSet.WRITE_NA.name:  check_write_na_ins_args,
        InstructionSet.READ.name:      check_read_ins_args,
        InstructionSet.READ_NS.name:   check_read_ins_args,
        InstructionSet.READ_NF.name:   check_read_nf_ins_args,
        InstructionSet.READ_PNT.name:  check_read_pnt_ins_args,
        InstructionSet.WAIT_US.name:   check_wait_ins_args,
        InstructionSet.WAIT_MS.name:   check_wait_ins_args,
        InstructionSet.MOV.name:       check_mov_ins_args,
        InstructionSet.MOV_B.name:     check_mov_b_ins_args,
        InstructionSet.STOP.name:      check_no_args,
        InstructionSet.WAIT_TRIG.name: check_no_args,
        InstructionSet.RET.name:       check_no_args,
        InstructionSet.NOP.name:       check_no_args,
        InstructionSet.SYNCIO.name:    check_no_args,
        InstructionSet.GOTO.name:      check_goto_ins_args,
        InstructionSet.GOTO_JB0.name:  check_goto_ins_args,
        InstructionSet.GOTO_JB1.name:  check_goto_ins_args,
        InstructionSet.CALL.name:      check_goto_ins_args,
        InstructionSet.CALL_JB0.name:  check_goto_ins_args,
        InstructionSet.CALL_JB1.name:  check_goto_ins_args,
        InstructionSet.LOAD.name:      check_lit_reg_optlit_args,
        InstructionSet.ADD.name:       check_lit_reg_optlit_args,
        InstructionSet.SUB.name:       check_lit_reg_optlit_args,
        InstructionSet.AND.name:       check_lit_reg_optlit_args,
        InstructionSet.OR.name:        check_lit_reg_optlit_args,
        InstructionSet.INC.name:       check_reg_optlit_args,
        InstructionSet.DEC.name:       check_reg_optlit_args,
        InstructionSet.SHL.name:       check_reg_optlit_args,
        InstructionSet.SHR.name:       check_reg_optlit_args,
        InstructionSet.ROL.name:       check_reg_optlit_args,
        InstructionSet.ROR.name:       check_reg_optlit_args,
        InstructionSet.NOT.name:       check_reg_optlit_args,
        InstructionSet.MINUS.name:     check_reg_optlit_args,
    }

    non_essentials = set([
        InstructionSet.STOP.name,
        InstructionSet.WAIT_MS.name,
        InstructionSet.MOV.name,
        InstructionSet.MOV_B.name,
        InstructionSet.LOAD.name,
        InstructionSet.NOP.name,
    ]).union(ALU1_INSNS).union(ALU2_INSNS)

    check_func = select_checker.get(ins.name, lambda: False)
    if not check_func(ins):
        return False

    if ((ins.name in non_essentials) and
            (not check_on_not_essential(ins))):
        return False

    if not check_lits_valid(ins):
        return False

    return True


def get_mem_size():
    """Returns the minimum power-of-two memory size that can fit the program"""
    return 2**math.ceil(math.log2(instructions[-1].pc+1))


def emit_code(mem_f, pkg_if_nx=None):
    """Emit the program memory initialization code"""
    bin_bits = []
    for ins in instructions:
        if not check_instruction_args(ins):
            continue
        if ((ins.name == InstructionSet.WRITE.name) or
                (ins.name == InstructionSet.WRITE_NS.name)):
            # emit opcode
            bin_bits += ['001']

            # if using a literal:
            if ins.arguments[2].arg_type == ArgumentType.LITERAL:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[2].value)
                bin_bits[-1] += '0'
                bin_bits[-1] += '1111'
            # if using a register
            else:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[2].value)
                bin_bits[-1] += '0'
                bin_bits[-1] += '{:04b}'.format(ins.arguments[2].size - 1)

            bin_bits += ['']    # second word
            # the FW will fill in the write / read bit
            bin_bits[-1] += '{:07b}'.format(ins.arguments[0].value // 2)
            bin_bits[-1] += '{:08b}'.format(ins.arguments[1].value)
            if ins.name == InstructionSet.WRITE_NS.name:
                bin_bits[-1] += '0'
            else:
                bin_bits[-1] += '1'

        if ins.name == InstructionSet.WRITE_NF.name:
            # emit opcode
            bin_bits += ['001']

            # if using a literal:
            if ins.arguments[1].arg_type == ArgumentType.LITERAL:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[1].value)
                bin_bits[-1] += '1'
                bin_bits[-1] += '1111'
            # if using a register
            else:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[1].value)
                bin_bits[-1] += '1'
                bin_bits[-1] += '{:04b}'.format(ins.arguments[1].size - 1)

            bin_bits += ['']    # second word
            # the FW will fill in the write / read bit
            bin_bits[-1] += '{:07b}'.format(ins.arguments[0].value // 2)
            bin_bits[-1] += '0'*8 # no register address, fill with 0s
            bin_bits[-1] += '1' # no variant w/o stop for this


        if ins.name == InstructionSet.WRITE_NA.name:
            # emit opcode
            bin_bits += ['001']

            # if using a literal:
            if ins.arguments[0].arg_type == ArgumentType.LITERAL:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[0].value)
                bin_bits[-1] += '1'
                bin_bits[-1] += '1111'
            # if using a register
            else:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[0].value)
                bin_bits[-1] += '1'
                bin_bits[-1] += '{:04b}'.format(ins.arguments[0].size - 1)

            bin_bits += ['']    # second word
            # the FW will fill in the write / read bit
            bin_bits[-1] += '0'*7 # fill with zeroes
            bin_bits[-1] += '0'*8 # no register address, fill with 0s
            bin_bits[-1] += '0' # no variant with stop, use STOP cmd separately


        if ((ins.name == InstructionSet.READ.name) or
                (ins.name == InstructionSet.READ_NS.name) or
                (ins.name == InstructionSet.READ_NF.name) or
                (ins.name == InstructionSet.READ_PNT.name)):
            # emit opcode
            bin_bits += ['010']

            if not ins.name == InstructionSet.READ_NF.name:
                bin_bits[-1] += '{:06b}'.format(ins.arguments[2].value)
            else:
                # we're missing the second argument in that case
                bin_bits[-1] += '{:06b}'.format(ins.arguments[1].value)

            bin_bits[-1] += '0'

            # set pointer bit
            if ins.name == InstructionSet.READ_PNT.name:
                bin_bits[-1] += '1'
            else:
                bin_bits[-1] += '0'

            if ins.name == InstructionSet.READ_NF.name:
                bin_bits[-1] += '1'
            else:
                bin_bits[-1] += '0'

            if not ins.name == InstructionSet.READ_NF.name:
                bin_bits[-1] += '{:04b}'.format(ins.arguments[2].size - 1)
            else:
                # we're missing the second argument in that case
                bin_bits[-1] += '{:04b}'.format(ins.arguments[1].size - 1)

            bin_bits += ['']    # second word
            if ins.name == InstructionSet.READ_PNT.name:
                bin_bits[-1] += '{:07b}'.format(ins.arguments[0].value)
            else:
                # the FW will fill in the write / read bit
                bin_bits[-1] += '{:07b}'.format(ins.arguments[0].value // 2)

            if not ins.name == InstructionSet.READ_NF.name:
                bin_bits[-1] += '{:08b}'.format(ins.arguments[1].value)
            else:
                bin_bits[-1] += '0'*8

            if ins.name == InstructionSet.READ_NS.name:
                bin_bits[-1] += '0'
            else:
                bin_bits[-1] += '1'

        if ins.name == InstructionSet.STOP.name:
            # emit opcode
            bin_bits += ['0110']
            bin_bits[-1] += '0'*12 # 12 LSBs are unused

        if ((ins.name == InstructionSet.GOTO.name) or
                (ins.name == InstructionSet.GOTO_JB0.name) or
                (ins.name == InstructionSet.GOTO_JB1.name) or
                (ins.name == InstructionSet.RET.name)):
            # emit opcode
            bin_bits += ['1001']

            if not ins.name == InstructionSet.RET.name:
                bin_bits[-1] += '{:09b}'.format(jump_fix_dict[ins.arguments[0].value])
            else:
                bin_bits[-1] += '0'*9

            if ins.name == InstructionSet.GOTO_JB0.name:
                bin_bits[-1] += '100'
            elif ins.name == InstructionSet.GOTO_JB1.name:
                bin_bits[-1] += '010'
            elif ins.name == InstructionSet.RET.name:
                bin_bits[-1] += '001'
            else:
                bin_bits[-1] += '000'

        if ((ins.name == InstructionSet.WAIT_US.name) or
                (ins.name == InstructionSet.WAIT_MS.name)):
            # emit opcode
            bin_bits += ['1010']

            bin_bits[-1] += '{:010b}'.format(ins.arguments[0].value)
            if ins.name == InstructionSet.WAIT_MS.name:
                bin_bits[-1] += '1' # ms bit
            else:
                bin_bits[-1] += '0' # us bit

            bin_bits[-1] += '0' # LSB is unused

        if ins.name == InstructionSet.WAIT_TRIG.name:
            # emit opcode
            bin_bits += ['1011']
            bin_bits[-1] += '0'*12 # 12 LSBs are unused

        if ins.name == InstructionSet.MOV.name:
            for i in range(ins.arguments[0].size):
                # emit opcode
                bin_bits += ['1100']

                bin_bits[-1] += '{:06b}'.format(ins.arguments[0].value + i)
                bin_bits[-1] += '{:06b}'.format(ins.arguments[1].value + i)

        if ins.name == InstructionSet.MOV_B.name:
            # emit opcode
            bin_bits += ['1100']

            bin_bits[-1] += '{:06b}'.format(ins.arguments[0].value +
                                            ins.arguments[2].value)
            bin_bits[-1] += '{:06b}'.format(ins.arguments[1].value +
                                            ins.arguments[2].value)

        if ins.name == InstructionSet.LOAD.name:
            # emit opcode
            bin_bits += ['1110']

            if len(ins.arguments) > 2:
                offset = ins.arguments[2].value
            else:
                offset = 0

            bin_bits[-1] += '{:06b}'.format(ins.arguments[1].value + offset)
            bin_bits[-1] += '0'*6 # 6 LSBs are unused in the 1st word

            bin_bits += ['']
            bin_bits[-1] += '{:09b}'.format(ins.arguments[0].value)
            bin_bits[-1] += '0'*7 # 7 LSBs are unused in the 2nd word

        if ((ins.name == InstructionSet.CALL.name) or
                (ins.name == InstructionSet.CALL_JB0.name) or
                (ins.name == InstructionSet.CALL_JB1.name)):
            # emit opcode to load the pc to the return pointer
            bin_bits += ['1110']

            bin_bits[-1] += '0'*9
            bin_bits[-1] += '100'

            bin_bits += ['']
            bin_bits[-1] += '{:09b}'.format(ins.pc+3)
            bin_bits[-1] += '0'*7 # 7 LSBs are unused in the 2nd word

            # jump to the function
            bin_bits += ['1001']

            bin_bits[-1] += '{:09b}'.format(jump_fix_dict[ins.arguments[0].value])
            if ins.name == InstructionSet.CALL_JB0.name:
                bin_bits[-1] += '10'
            elif ins.name == InstructionSet.CALL_JB1.name:
                bin_bits[-1] += '01'
            else:
                bin_bits[-1] += '00'

            bin_bits[-1] += '0' # RET bit off

        if ins.name in ALU1_INSNS:
            # emit opcode
            bin_bits += ['1000']

            if len(ins.arguments) > 2:
                offset = ins.arguments[2].value
            else:
                offset = 0

            bin_bits[-1] += '{:06b}'.format(ins.arguments[1].value + offset)
            bin_bits[-1] += ALU2_INS_BITS[ins.name]
            bin_bits[-1] += '000' # 3 LSBs are unused in the 1st word

            bin_bits += ['']
            bin_bits[-1] += '{:09b}'.format(ins.arguments[0].value)
            bin_bits[-1] += '0'*7 # 7 LSBs are unused in the 2nd word

        if ins.name in ALU2_INSNS:
            # emit opcode
            bin_bits += ['1111']

            if len(ins.arguments) > 1:
                offset = ins.arguments[1].value
            else:
                offset = 0

            bin_bits[-1] += '{:06b}'.format(ins.arguments[0].value + offset)
            bin_bits[-1] += ALU1_INS_BITS[ins.name]
            bin_bits[-1] += '0'*3 # 3 LSBs are unused

        if ins.name == InstructionSet.SYNCIO.name:
            # emit opcode
            bin_bits += ['1101']
            bin_bits[-1] += '0'*12 # 12 LSBs are unused

        if ins.name == InstructionSet.NOP.name:
            # emit opcode
            bin_bits += ['0000']
            bin_bits[-1] += '0'*12 # 12 LSBs are unused

    if pkg_if_nx:
        pkg_if_nx.write('"')
        change_line = False
    for i, bin_ins in enumerate(bin_bits):
        if len(bin_ins) != 16:
            print_error(ERR_INS_TOO_BIG, bin_ins + ' at addr ' + str(i))
        else:
            if pkg_if_nx:
                if ((i + 1 == len(bin_bits)) and (len(bin_bits) == NX_MEM_SIZE)):
                    delimiter = '";\n'
                else:
                    delimiter = '," &'
                    if change_line:
                        delimiter += '\n' + ' '*4 + '"'
                    else:
                        delimiter += ' "'
                    change_line = not change_line
                pkg_if_nx.write('0'*8 + bin_ins + delimiter)
            else:
                mem_f.write(bin_ins+'\n')

    if pkg_if_nx:
        for i in range(NX_MEM_SIZE-len(bin_bits)):
            if i + 1 == NX_MEM_SIZE-len(bin_bits):
                delimiter = '";\n\n'
            else:
                delimiter = '," &'
                if change_line:
                    delimiter += '\n' + ' '*4 + '"'
                else:
                    delimiter += ' "'
                change_line = not change_line
            pkg_if_nx.write('0'*24 + delimiter)
    else:
        for _ in range(get_mem_size()-len(bin_bits)):
            mem_f.write('0'*16 + '\n')


def write_package(f_name, mem_f_name):
    """Write the package file."""
    # some basic parameter checks first
    if (params['WATCHDOG'] and
            (params['WATCHDOG_RST_CYCLES'] > params['WATCHDOG'])):
        print_error(ERR_WATCHDOG_RESET_OUT_OF_RANGE)
        return False

    if (params['RESET_START_FROM'] and
            (params['RESET_START_FROM'] not in labels.keys())):
        print_error(ERR_LABEL_UNDEFINED, params['RESET_START_FROM'])
        return False

    # compute RESET_START_FROM address, if it is defined
    if params['RESET_START_FROM']:
        reset_start_from = jump_fix_dict[labels[params['RESET_START_FROM']]]
    else:
        reset_start_from = 0

    with open(f_name, 'w') as pkg_f:
        pkg_f.write(
            f"""library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;

package mouseq_pkg is

  constant IWORDS : natural := {params['IWORDS']}; -- input registers
  constant OWORDS : natural := {params['OWORDS']}; -- output registers
  constant INTWORDS : natural := {int_reg_pos};    -- internal registers

  constant I2C_CLKS : unsigned(15 downto 0) := {'x"{:04x}"'.format(params['I2C_CLKS'])}; -- i2c clock prescale

  constant MEMWIDTH : natural := 16;               -- program mem word width, fixed
  constant WORDWIDTH : natural := 8;               -- reg width, fixed

  -- program memory size in words, address bus size
  constant MEMSIZE : natural := {get_mem_size()};
  constant MEMABITS : natural := integer(ceil(log2(real(MEMSIZE))));

  -- define a type for the program memory
  subtype RamCellType is std_logic_vector(MEMWIDTH-1 downto 0);
  type RamType is array(0 to MEMSIZE-1) of RamCellType;

  -- program memory initialization file
  constant PROG_FILENAME : string := "{os.path.basename(mem_f_name)}";

  -- register file size in words, address bus size
  constant REGSIZE : natural := IWORDS+OWORDS+INTWORDS;
  constant REGABITS : natural := integer(ceil(log2(real(REGSIZE))));

  -- define a type for the unified register space
  type RegType is array(0 to IWORDS+OWORDS+INTWORDS-1) of std_logic_vector(WORDWIDTH-1 downto 0);

  constant BUFFERIO : boolean := {'true' if params['BUFFERIO'] else 'false'}; -- add register layer I/O or not
  constant BIG_ENDIAN : boolean := {'true' if params['BIG_ENDIAN'] else 'false'}; -- select endianness for multi-byte
  constant FAMILY : string := "{params['FAMILY']}"; -- select device family for mem implementation
  constant NX_PROGMEM : boolean := {'true' if (params['FAMILY'] == 'NX') else 'false'}; -- enable ECC prog memory implementation for nanoXplore
  constant SIMPLE_PROGMEM : boolean := {'true' if params['SIMPLE_PROGMEM'] else 'false'}; -- select simple prog memory implementation (more resources with TMR)
  constant SIMPLE_REGFILE : boolean := {'true' if params['SIMPLE_REGFILE'] else 'false'}; -- select simple register file implementation (more resources)
  constant TMR : boolean := {'true' if params['TMR'] else 'false'}; -- TMR constant used to set attributes
  constant ESSENTIALS_ONLY : boolean := {'true' if params['ESSENTIALS_ONLY'] else 'false'}; -- disable NOP, MOV, ALU, LOAD, STOP and WAIT_MS

  constant ERR_SET_JUMP : boolean := {'true' if params['ERR_SET_JUMP'] else 'false'}; -- enable watchdog
  constant ZERO_SET_JUMP : boolean := {'true' if params['ZERO_SET_JUMP'] else 'false'}; -- enable watchdog

  constant WATCHDOG_ON : boolean := {'true' if params['WATCHDOG'] else 'false'}; -- enable watchdog
  constant WATCHDOG_VAL : natural := {params['WATCHDOG']*params['I2C_CLKS']+1}; -- watchdog value
  constant WATCHDOG_BITS : natural := integer(ceil(log2(real(WATCHDOG_VAL))));
  constant WATCHDOG_RESET_CYCLES : natural := {params['WATCHDOG_RST_CYCLES']}; -- how many cycles to keep the watchdog reset high

  constant RESET_START_FROM : natural := {reset_start_from}; -- reset at a certain address to skip init

  constant NX_MEM_INIT : string; -- for memory instantiation in nanoXplore, defined below

  -- define a helper function to initialize the program memory
  impure function InitRamFromFile(RamFileName : in string) return RamType;
  impure function InitRamFirstCellFromFile(RamFileName : in string) return RamCellType;

  function vote(I1 : std_logic; I2 : std_logic; I3 : std_logic) return std_logic;
  function get_attr_radhard return string;

end package mouseq_pkg;

package body mouseq_pkg is

  impure function InitRamFromFile(RamFileName : in string) return RamType is
    file RamFile : text is RamFileName;
    variable RamFileLine : line;
    variable temp_bv : bit_vector(MEMWIDTH-1 downto 0);
    variable RAM : RamType;
  begin
    for I in RamType'range loop
      readline(RamFile, RamFileLine);
      read(RamFileLine, temp_bv);
      RAM(I) := to_stdlogicvector(temp_bv);
    end loop;
    return RAM;
  end function;

  impure function InitRamFirstCellFromFile(RamFileName : in string) return RamCellType is
    file RamFile : text is RamFileName;
    variable RamFileLine : line;
    variable temp_bv : bit_vector(MEMWIDTH-1 downto 0);
  begin
    readline(RamFile, RamFileLine);
    read(RamFileLine, temp_bv);
    return(to_stdlogicvector(temp_bv));
  end function;

  -- purpose: manually implement TMR to scrub memories
  function vote(I1 : std_logic; I2 : std_logic; I3 : std_logic)
    return std_logic is
  begin  -- function vote
    return ((I1 and I2) or (I2 and I3) or (I3 and I1));
  end function vote;

  -- purpose: set the syn_radhardlevel attribute if TMR is set
  function get_attr_radhard return string is
  begin
    if TMR then
      return "tmr";
    else
      return "none";
    end if;
  end function get_attr_radhard;
""")

        if params['FAMILY'] == 'NX':
            pkg_f.write('\n  constant NX_MEM_INIT : string := ')
            emit_code(None, pkg_f)
        else:
            pkg_f.write('\n  constant NX_MEM_INIT : string := "";\n\n')

        pkg_f.write('end package body mouseq_pkg;\n')

    return True


###############
## main code ##
###############

def main(infile_n, outfile_pkg_n, outfile_mem_n, verbose):
    """Main function. First, it will read the code into a variable and
       do some basic preprocessing (strip instructions, whitespace, etc).
       Then, it will filter it through some functions that will process:
       1. constant and parameter definitions,
       2. register definitions,
       3. label definitions,
       4. instructions,
       in that order. Next, it will run a correction of the program counter
       to fix jumps, as there are some instructions that take two words. Finally,
       it will generate the HDL package for the I/O and register configuration
       and the memory initialization file for the program.
    """

    init_global_vars()

    global is_verbose
    is_verbose = verbose

    # read the whole input file into a variable
    with open(infile_n, 'r') as infile:
        code_lines = infile.readlines()

    code_lines = preprocess_basic(code_lines)

    # parse the code for labels just to get their names
    list(map(parse_label, code_lines))

    # then pass the code through the code parsing functions: first
    # process the constants, then the instructions, then the labels
    # again. It's important that it's ran like that as parse_label
    # gets output from the pc that is updated by parse_comand at every
    # line.
    code_lines = list(filter(None,
                             map(parse_instruction,
                                 map(parse_label,
                                     map(parse_regdef,
                                         map(parse_const,
                                             code_lines))))))

    # now that the labels have the right values, run the code through
    # parse_instruction again to update these values in the
    # instructions table.  First, reset the instructions database and
    # the program counter.
    global program_counter
    program_counter = 0
    global instructions
    instructions = []
    code_lines = list(filter(None, map(parse_instruction, code_lines)))

    # Finally, adjust for multi-word instructions to repair jump addresses
    fix_pcs()

    print()

    print('Configuration overview:')
    print('='*23)
    print()

    print('Parameters:')
    print_dict(params)

    print()

    # only print regs if we have any defined
    if registers:
        print('Registers:')
        print_regs()
    else:
        print('No registers defined')

    print()

    # only print consts if we have any
    if consts:
        print('User constants:')
        print_dict(consts)
    else:
        print('No constants defined')

    print()

    # only print labels if we have any
    if labels:
        print('Labels:')
        print_dict(labels)
    else:
        print('No labels defined')

    print()

    # I/O report
    print('Checking I/O assignments...')
    check_inputs_outputs()

    print()

    # update register positions to match the HW structure
    print('Updating register positions to match the HW structure...')
    update_instruction_register_positions()

    print()

    # print the pre-processed code if verbose flag is on
    if is_verbose:
        print_input(code_lines)
        print_instructions()

    # populate the memory initialization file
    print('Producing memory contents...')
    with open(outfile_mem_n, 'w') as outfile_mem:
        emit_code(outfile_mem)

    # write the package file
    print('Writing package...')
    write_package(outfile_pkg_n, outfile_mem_n)

    return exit_status


if __name__ == "__main__":
    # setup argument parser
    parser = argparse.ArgumentParser(description='Compile .mseq files'
                                     'to configure a mouseq IP core.')

    # we need an input file
    parser.add_argument('infile', type=argparse.FileType('r'),
                        help='the input file')

    # the output filenames are optional
    parser.add_argument('outfile_pkg', type=argparse.FileType('w'), nargs='?',
                        help='the output file for the package'
                        '(mouseq_pkg_gen.vhd by default)',
                        default=open('mouseq_pkg_gen.vhd', 'w'))

    parser.add_argument('outfile_mem', type=argparse.FileType('w'), nargs='?',
                        help='the output file for the program'
                        '(mouseq_mem.mif by default)',
                        default=open('mouseq_mem.mif', 'w'))

    # verbosity flag
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='be more verbose')

    # parse the arguments
    args = parser.parse_args()

    main(str(args.infile.name), str(args.outfile_pkg.name), str(args.outfile_mem.name), args.verbose)
