# Cycle analog mux and poll ADC to measure all voltages and currents
# of the development board


# parameter definitions
OWORDS = 32
IWORDS = 0

FAMILY = NX                     # for program memory implementation
BIG_ENDIAN = 1                  # the peripherals are SMBus-compatible
I2C_CLKS = 0x100

# register definitions
V12:   out 0,  2                    # 0
V18:   out 2,  2                    # 1
V25:   out 4,  2                    # 2
V33:   out 6,  2                    # 3
VFMC:  out 8,  2                    # 4
VHMC:  out 10, 2                    # 5
VDDS:  out 12, 2                    # 6
I12:   out 14, 2
I18:   out 16, 2
I25:   out 18, 2
I33:   out 20, 2
IFMC:  out 22, 2
IHMC:  out 24, 2
IDDS:  out 26, 2
TMP:   out 28, 2
ERR:   out 30, 1
TRIG:  out 31, 1

R_ADC_CONFIGV: int 2
R_ADC_CONFIGI: int 2
R_ADC_READCONFIG: int 2

# constant definitions
A_MUX1 = 0x98
A_MUX2 = 0x9a
A_ADC  = 0x90
A_TMP  = 0x92

RA_ADC_CONFIG = 1
RA_ADC_CONV = 0

ADC_MUXV = 0b100                # (GND to AIN0)
ADC_MUXI = 0b101                # (GND to AIN1)
ADC_PGAV = 0b001                # 4.096
ADC_PGAI = 0b001                # 4.096
ADC_MODE = 1                    # single-shot mode (default)
ADC_DR   = 0b111                # 860SPS (max)
ADC_COMP_QUE = (ADC_DR << 5) | 0b11 # disable comparator

ADC_CONFIGV_H = (1 << 7) | (ADC_MUXV << 4) | (ADC_PGAV << 1) | ADC_MODE
ADC_CONFIGV_L = ADC_COMP_QUE

ADC_CONFIGI_H = (1 << 7) | (ADC_MUXI << 4) | (ADC_PGAI << 1) | ADC_MODE
ADC_CONFIGI_L = ADC_COMP_QUE

ADC_TIMEOUT = 200 # set ~200*0.3=60ms as the maximum ADC conversion time

RA_TMP_CONV = 0                 # LM73 temp is read from address 0

load ADC_CONFIGV_H, R_ADC_CONFIGV, 1
load ADC_CONFIGV_L, R_ADC_CONFIGV
load ADC_CONFIGI_H, R_ADC_CONFIGI, 1
load ADC_CONFIGI_L, R_ADC_CONFIGI

goto start_l


wait_adc:

WAIT_ADC_TMP: int
load ADC_TIMEOUT, WAIT_ADC_TMP

wait_adc_loop:
dec WAIT_ADC_TMP
goto_jb1 wait_adc_end

read A_ADC, RA_ADC_CONFIG, R_ADC_READCONFIG
and 0x80, R_ADC_READCONFIG      # if zero still converting, sets jump bit
goto_jb1 wait_adc_loop          # if jump bit set, check again
wait_adc_end:
ret

err_set0:
or 0x01, ERR
ret

err_set1:
or 0x02, ERR
ret

err_set2:
or 0x04, ERR
ret

err_set3:
or 0x08, ERR
ret

err_set4:
or 0x10, ERR
ret

err_set5:
or 0x20, ERR
ret

err_set6:
or 0x40, ERR
ret

err_set7:
or 0x80, ERR
ret

# main loop
start_l:

load 0, ERR

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x01
call_jb1 err_set0
write_nf A_MUX2, 0x01              # switch to input 0
call_jb1 err_set0

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, V12    # result ready, read it

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, I12

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x02
call_jb1 err_set1
write_nf A_MUX2, 0x02              # switch to input 1
call_jb1 err_set1

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, V18

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, I18

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x04
call_jb1 err_set2
write_nf A_MUX2, 0x04              # switch to input 2
call_jb1 err_set2

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, V25

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, I25

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x08
call_jb1 err_set3
write_nf A_MUX2, 0x08              # switch to input 3
call_jb1 err_set3

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, V33

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, I33

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x10
call_jb1 err_set4
write_nf A_MUX2, 0x10              # switch to input 4
call_jb1 err_set4

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, VFMC

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, IFMC

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x20
call_jb1 err_set5
write_nf A_MUX2, 0x20              # switch to input 5
call_jb1 err_set5

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, VHMC

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, IHMC

############## read a V/I pair #################
write_nf A_MUX1, 0x00
write_nf A_MUX2, 0x00
write_nf A_MUX1, 0x40
call_jb1 err_set6
write_nf A_MUX2, 0x40              # switch to input 6
call_jb1 err_set6

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGV
call wait_adc
read A_ADC, RA_ADC_CONV, VDDS

write A_ADC, RA_ADC_CONFIG, R_ADC_CONFIGI
call wait_adc
read A_ADC, RA_ADC_CONV, IDDS

############### read the temp ##################

read A_TMP, RA_TMP_CONV, TMP
call_jb1 err_set7

load 0xff, TRIG
syncio
load 0x00, TRIG
syncio

wait_ms 130

goto start_l
