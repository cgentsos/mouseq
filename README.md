# Uncomplicated Monitoring Sequencer (MoUSeq)


## Functionality and general overview


This IP core is designed to facilitate the configuration and
repetitive (usually periodic) sequential reading or writing of
registers from a pre-determined list of peripherals connected to some
bus.

At first, an initialization sequence is ran to configure those
peripherals; then, a second sequence is looped to (typically) monitor
some values out of these peripherals. The two sequences are expressed
using a simple but flexible language. A python program is provided to
then translate this input to a VHDL package that will parametrise the
module; that way, the sequences get baked into the gateware.

As an example, assume that we need to monitor some temperature. First,
some values need to be written to some ADC registers to set it up;
then, periodically, two ADC registers (that make up the measured
value) are read out and assigned to specific bits in the output bus.

Registers are assigned to the output bits in a static (no logic
resources, like barrel shifters, are used for the assignments) but
configurable way: each register gets assigned a bit range in the input
or output buses. These assignments are checked at program assembly
time to give early feedback to the user in case of any double
assignment or unassigned port errors.

---

## Syntax

### Constants

The user can define constants to use in their sequences.

The syntax to define a constant is simple:

``` lua # the only reason for using lua is that it its syntax highliting happens to play nice with our syntax
CONST_NAME = const_val
```

### Expressions

A very restricted subset of the Python interpreter (ast.literal_eval)
is used to evaluate expressions. Hexadecimal and binary values in `0x`
and `0b` notation are OK to use. Constants can also be used in forming
expressions. Some examples that work fine (with Python 3.7, at least)
can be seen below:

``` lua
C16h = 0x10
C15b = 0b1111
Myconst2 = C16h + 2
myconst3 = MYCONST2 + Myconst2 - 16 + 1
myconst4 = c15b + 3*((myconst3+2)/2) + 2**3
```

In case any rounding has to be performed, a warning will be shown so
the user is aware:

``` lua
setting user constant "C16h" to 16
setting user constant "C15b" to 15
setting user constant "Myconst2" to 18
setting user constant "myconst3" to 21
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
(15) + 3*(((21)+2)/2) + 2**3.3
    ^^^ WARNING: There was some rounding here. ^^^
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
setting user constant "myconst4" to 59
```


#### Special constants

There are two special constants that configure the core itself: they
set the I/O port widths.

``` lua
OBITS = number_bits # default is 0
IBITS = number_bits # default is 0
```

### Registers

To be able to use the input and output ports, but also as places to
put data temporarily, the user can define registers.

A register can be either:
* connected to an input
* connected to an output
* internal (for temporarily storing data)

``` lua
# register definitions
R1: in 0        # input register
R2: out 0       # output register
R3: out 8, 3    # multi-byte output register
rmine: int      # internal register
```

Since many transactions involve data wider than one byte, multi-byte
registers can be declared and used. The syntax for this can be seen on
the example, above.

Using a multi-byte register is simple: a write command will write all
the bytes of the register, one by one; a read command will expect a
reply length equal to the size of the register.


### Comments

A comment starts with a #, and it can placed either at the end of a
line of code or as a whole line.

``` lua
# comment taking up a full line
MYCONST = 4 # comment at the end of a code line
```

### Labels

The user can set and jump to a label by using this (familiar) syntax:

``` lua
label_name:
# . . .
goto label_name
```

**note:** The label has to occupy a line on itself; it cannot be mixed
with code in the same line (although it can be followed by a comment).

**note:** If some command has an error, the assembler will continue,
omitting it. However, the program counter used to set values to the
labels will count it normally. A program that has code errors
shouldn't be expected to work – the labels would point to
unpredictable places.

### Commands

The following commands are supported:

**note:** The assembler is case insensitive.

#### Write

``` lua
write dev_addr, reg_addr, literal
write dev_addr, reg_addr, reg_name
```

The `write` command will write either a literal (const or some number)
or a register to a register addressed by `reg_addr`, inside a device
addressed by `dev_addr`.

**note:** If `reg_name` is a multi-byte register, the write
transaction will write all the bytes, one by one.

**note:** `reg_name` cannot be an "output" register, it can either be
"input" or "internal".


#### Write_ns

``` lua
write_ns dev_addr, reg_addr, literal
write_ns dev_addr, reg_addr, reg_name
```

The `write_ns` command will write either a literal (const or some
number) or a register to a register addressed by `reg_addr`, inside a
device addressed by `dev_addr`. The difference with the `write`
command is that it won't cause a `STOP` frame to be emitted; this
allows to support PMBus group commands (I2C repeated `START`
condition). The user needs to issue a `STOP` condition manually at the
end of the group command, using the `stop` command (below).

#### Write_nf

``` lua
write_nf dev_addr, literal
write_nf dev_addr, reg_name
```

The `write_nf` command will write either a literal (const or some
number) or a register directly to a device addressed by `dev_addr`; it
won't write the data to any specific register. This is mainly
implemented to support PMBus extended commands: the user can issue a
`write_nf dev_addr, 0xFF`, followed by the extended command write.

#### Write_na

``` lua
write_na literal
write_na reg_name
```

The `write_na` command will write either a literal (const or some
number) or a register directly to the bus, without any device address.
This is implemented to better support PMBus extended commands: the
user can issue a `write_na` command after the `write_ns dev_addr,
reg_addr, [literal | reg_name]`, to avoid writing the device address
twice. A `stop` command must be issued separately after this, as no
`STOP` frame is added with `write_na`.

#### Read

``` lua
read dev_addr, reg_addr, reg_name
```

The `read` command will read from a register addressed by `reg_addr`,
inside a device addressed by `dev_addr`, to the register indicated by
`reg_name`.

**note:** If `reg_name` is a multi-byte register, the read transaction
will expect an equal number of bytes as a reply.

**note:** `reg_name` cannot be an "input" register, it can either be
"output" or "internal".


#### Read_ns

``` lua
read_ns dev_addr, reg_addr, reg_name
```

The `read_ns` command will read from a register addressed by
`reg_addr`, inside a device addressed by `dev_addr`, to the register
indicated by `reg_name`. The difference with the `read` command is
that it won't cause a `STOP` frame to be emitted; this allows to
support PMBus group commands (I2C repeated `START` condition). The
user needs to issue a `STOP` condition manually at the end of the
group command, using the `stop` command (below).

#### Read_nf

``` lua
read_nf dev_addr, reg_name
```

The `read_ns` command will directly read from a device addressed by
`dev_addr` to the register indicated by `reg_name`. This is meant to
be used in conjunction with the `write_ns` command to implement the
SMBus block write - block read - process call protocol.

#### Stop

``` lua
stop
```

Issues a `STOP` condition. To be used in conjunction with the
`write_na`, `write_ns` and `read_ns` commands, (above).

#### Goto, goto_jb0, goto_jb1

``` lua
[goto, goto_jb0, goto_jb1] label_name
```

The `goto` command will cause a jump to the code at the label defined
by `label_name`. The `_jb0` and `_jb1` variants can be used to jump
conditionally: if the jump bit is set (`jb1`) or not set (`jb0`).

**note:** The primary reason labels were introduced is to be able to
have a separate "configuration section", to initialize any peripheral
devices; and a "loop section", to repeatedly query them for data.


#### Call, call_jb0, call_jb1

``` lua
[call, call_jb0, call_jb1] label_name
```

The `call` command will cause a jump to the code at the label defined
by `label_name` and the next instruction's address to be stored in the
"return pointer". The `_jb0` and `_jb1` variants can be used to jump
conditionally: if the jump bit is set (`jb1`) or not set
(`jb0`). This, combined with the `RET` function, implement some
limited but useful function calls (but one can't call a function from
another function as there's no stack).

#### Ret

``` lua
ret
```

The `ret` command will cause a jump to the address pointed to by the
"return pointer" register.

#### Wait_us

``` lua
wait_us literal
```

The `wait_us` command will wait for the determined amount of time,
expressed in microseconds.


#### Wait_ms

``` lua
wait_ms literal
```

The `wait_ms` command will wait for the determined amount of time,
expressed in milliseconds.


#### Wait_trig

``` lua
wait_trig
```

The `wait_trig` command will wait until the external trigger signal is
high.


#### Mov

``` lua
mov reg_src, reg_dst
```

The `mov` command will copy the contents of `reg_src` to `reg_dst`.

**note:** `reg_dst` cannot be an "input" register, it can either be
"output" or "internal".

**note:** Commands are issues such that the registers are copied
whole - thus, they have to be the same size.

#### Mov_b

``` lua
mov_b reg_src, reg_dst
mov_b reg_src, reg_dst, offset
```

The `mov_b` command will copy the byte marked by `offset` from
`reg_src` to `reg_dst`, or the first byte if the offset argument is
omitted.

#### Syncio

``` lua
syncio
```

The `syncio` command pulls the values from the input ports into the
input registers and propagates the value of the output registers to
the output ports.

#### Load

``` lua
load literal, reg_dst
load literal, reg_dst, offset
```

The `load` command copies the literal value to the byte of `reg_dst`
marked by `offset`, or to its first byte if the offset argument is
omitted.

**note:** `reg_dst` cannot be an "input" register, it can either be
"output" or "internal".

#### Add, sub, and, or

``` lua
[add,sub,and,or] literal, reg_dst
[add,sub,and,or] literal, reg_dst, offset
```

The two-operand ALU commands operate on the byte of `reg_dst` marked
by `offset` and the literal value provided, and store the result on
the same register.

**note:** `reg_dst` cannot be an "input" register, it can either be
"output" or "internal".

#### Inc, dec, shl, shr, rol, ror, not, minus

``` lua
[Inc, dec, shl, shr, rol, ror, not, minus] reg_dst
[Inc, dec, shl, shr, rol, ror, not, minus] reg_dst, offset
```

The unary ALU commands operate on the value of the byte of `reg_dst`
marked by `offset`, or the first byte if the offset argument is
omitted. The operations supported are: increment, decrement, logical
shift left, logical shift right, left rotation, right rotation,
bitwise negation and signed negation

**note:** `reg_dst` cannot be an "input" register, it can either be
"output" or "internal".

#### Nop

``` lua
nop
```

The `nop` command does nothing.

---

## Parameters

<table class="tg">
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Default</th>
    <th>Description</th>
  </tr>
  <tr>
    <td class="tg-left">IBITS</td>
    <td class="tg-left">natural range 0 to 255</td>
    <td class="tg-left">0</td>
    <td class="tg-left">width of the input port</td>
  </tr>
  <tr>
    <td class="tg-left">OBITS</td>
    <td class="tg-left">natural range 0 to 255</td>
    <td class="tg-left">0</td>
    <td class="tg-left">width of the output port</td>
  </tr>
  <tr>
    <td class="tg-left">I2C_CLKS</td>
    <td class="tg-left">natural range 0 to 65535</td>
    <td class="tg-left">0x80</td>
    <td class="tg-left">I2C clock prescale, SCL*4</td>
  </tr>
  <tr>
    <td class="tg-left">BIG_ENDIAN</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">1 for big-endian multi-byte transfers</td>
  </tr>
  <tr>
    <td class="tg-left">ERR_SET_JUMP</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">1</td>
    <td class="tg-left">i2c errors control jump bit</td>
  </tr>
  <tr>
    <td class="tg-left">ZERO_SET_JUMP</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">1</td>
    <td class="tg-left">ALU result controls jump bit</td>
  </tr>
  <tr>
    <td class="tg-left">BUFFERIO</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">1 registers I/O ports</td>
  </tr>
  <tr>
    <td class="tg-left">FAMILY</td>
    <td class="tg-left">string</td>
    <td class="tg-left">NX</td>
    <td class="tg-left">PROASIC3, NX, XILINX, ALTERA (affects SIMPLE_PROGMEM<sup>1</sup>)</td>
  </tr>
  <tr>
    <td class="tg-left">TMR</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">Sets syn_radhardlevel attributes to "tmr" or "none"<sup>2</sup></td>
  </tr>
  <tr>
    <td class="tg-left">SIMPLE_PROGMEM</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">1 selects plain program memory implementation<sup>1</sup></td>
  </tr>
  <tr>
    <td class="tg-left">SIMPLE_REGFILE</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">1 selects plain register file implementation</td>
  </tr>
  <tr>
    <td class="tg-left">ESSENTIALS_ONLY</td>
    <td class="tg-left">0 or 1</td>
    <td class="tg-left">0</td>
    <td class="tg-left">1 disables the STOP, WAIT_MS, LOAD, MOV, ALU and NOP commands</td>
  </tr>
  <tr>
    <td class="tg-left">WATCHDOG</td>
    <td class="tg-left">0 or >0</td>
    <td class="tg-left">0</td>
    <td class="tg-left">0 disables the watchdog, >0 sets the threshold</td>
  </tr>
  <tr>
    <td class="tg-left">WATCHDOG_RST_CYCLES</td>
    <td class="tg-left">>0</td>
    <td class="tg-left">10</td>
    <td class="tg-left">how many reset cycles does the watchdog hold</td>
  </tr>
  <tr>
    <td class="tg-left">RESET_START_FROM</td>
    <td class="tg-left">string</td>
    <td class="tg-left">''</td>
    <td class="tg-left">any reset after the first one will start execution at that label</td>
  </tr>
</table>

*1:* When the device family is not set to PROASIC3, SIMPLE_PROGMEM is
enabled.

*2:* When TMR is off, program memory scrubbing is also disabled.

---

## Registers / local address space

All registers are accessed by way of a 6-bit local address space (up
to 64 registers supported). In the lower addresses, the input
registers are stored, followed by the output registers; the internal
registers are placed at the end of this address space. Finally, each
register can be up to 15 bytes wide (which would mean that it would
occupy almost one quarter of the address space).

In the assembly code, the user doesn't need to specify anything about
the size of the registers, the information is inferred from the
register declaration.

---

## Instruction encoding

<table class="tg">
  <tr>
    <th>Name</th>
    <th colspan="16">Encoding</th>
  </tr>
  <tr>
    <td></td>
    <td>15</td>
    <td>14</td>
    <td>13</td>
    <td>12</td>
    <td>11</td>
    <td>10</td>
    <td>9</td>
    <td>8</td>
    <td>7</td>
    <td>6</td>
    <td>5</td>
    <td>4</td>
    <td>3</td>
    <td>2</td>
    <td>1</td>
    <td>0</td>
  </tr>
  <tr>
    <td rowspan="2" class="tg-left">WRITE<sup>1</sup></td>
    <td colspan="3">001</td>
    <td colspan="8">local_reg_addr_or_literal</td>
    <td colspan="1">S<sup>2</sup></td>
    <td colspan="4">reg_size-1<sup>3</sup></td>
  </tr>
  <tr>
    <td colspan="7">dev_addr</td>
    <td colspan="8">reg_addr</td>
    <td colspan="1">P<sup>4</sup></td>
  </tr>
  <tr>
    <td rowspan="2" class="tg-left">READ<sup>1</sup></td>
    <td colspan="3">010</td>
    <td colspan="6">local_reg_addr</td>
    <td colspan="1">–</td>
    <td colspan="1">T<sup>5</sup></td>
    <td colspan="1">S<sup>2</sup></td>
    <td colspan="4">reg_size-1</td>
  </tr>
  <tr>
    <td colspan="7">dev_addr<sup>5</sup></td>
    <td colspan="8">reg_addr</td>
    <td colspan="1">P<sup>4</sup></td>
  </tr>
  <tr>
    <td class="tg-left">STOP</td>
    <td colspan="4">0110</td>
    <td colspan="12">–</td>
  </tr>
  <tr>
    <td rowspan="2" class="tg-left">ALU2</td>
    <td colspan="4">1000</td>
    <td colspan="6">local_reg_addr</td>
    <td colspan="3">CMD<sup>12</sup></td>
    <td colspan="3">–</td>
  </tr>
  <tr>
    <td colspan="1">-</td>
    <td colspan="8">literal</td>
    <td colspan="7">-</td>
  </tr>
  <tr>
    <td class="tg-left">GOTO</td>
    <td colspan="4">1001</td>
    <td colspan="9">address</td>
    <td colspan="1">J0<sup>11</sup></td>
    <td colspan="1">J1<sup>11</sup></td>
    <td colspan="1">R<sup>13</sup></td>
  </tr>
  <tr>
    <td class="tg-left">WAIT</td>
    <td colspan="4">1010</td>
    <td colspan="10">time</td>
    <td>M<sup>6</sup></td>
    <td colspan="1">–</td>
  </tr>
  <tr>
    <td class="tg-left">WAIT_TRIG</td>
    <td colspan="4">1011</td>
    <td colspan="12">–</td>
  </tr>
  <tr>
    <td class="tg-left">MOV<sup>7</sup></td>
    <td colspan="4">1100</td>
    <td colspan="6">src_addr</td>
    <td colspan="6">dst_addr</td>
  </tr>
  <tr>
    <td class="tg-left">SYNCIO</td>
    <td colspan="4">1101</td>
    <td colspan="12">–</td>
  </tr>
  <tr>
    <td rowspan="2" class="tg-left">LOAD<sup>1,8</sup></td>
    <td colspan="4">1110</td>
    <td colspan="6">local_reg_addr</td>
    <td colspan="3">–</td>
    <td colspan="1">RP<sup>14</sup></td>
    <td colspan="2">–</td>
  </tr>
  <tr>
    <td colspan="9">literal</td>
    <td colspan="7">–</td>
  </tr>
  <tr>
    <td class="tg-left">ALU1<sup>9</sup></td>
    <td colspan="4">1111</td>
    <td colspan="6">local_reg_addr</td>
    <td colspan="3">CMD<sup>11</sup></td>
    <td colspan="1">0</td>
    <td colspan="2">–</td>
  </tr>
  <tr>
    <td class="tg-left">NOP</td>
    <td colspan="4">0000</td>
    <td colspan="12">–</td>
  </tr>
</table>

*1:* The `write`, `read` and `load` instructions are two words long.

*2:* S: skip writing command word, write device address and directly
transfer the data. When S=1 and P=0, it will skip writing the device
address altogether (for better PMBus extended command support).

*3:* `1111` signifies the literal value is used for the operation.

*4:* P: add stop condition.

*5:* T: get device address from register indicated by *dev_addr*.

*6:* M: wait for a millisecond.

*7:* Copies one byte, the assembler will issue more of those for a
whole multi-byte register.

*8:* Loads one byte, the assembler will issue more of those for a
whole multi-byte register.

*9:* Operates on a single byte.

*10:* Conditional jumps: if J1 is set, jump bit has to be high for the
jump to be performed; if J0 is set, the jump bit has to be low.

*11:* `000`: increment, `001` decrement, `010` shift left, `011` shift
right, `100` rotate left, `101` rotate right, `110` bitwise NOT, `111`
signed minus.

*12:* `000`: add, `001` subtract, `010` bitwise AND, `011` bitwise OR.

*13:* R: jump to the location stored in the return pointer.

*14:* RP: load value to the return pointer instead of a register.

---

## Examples

### FCM2 fan controller readout

``` bash
# parameter definitions
OWORDS = 21
IWORDS = 0

# register definitions
fan_status: out 0
temp_status: out 1
volt_status: out 2
fan_speeds: out 3, 12
temps: out 15, 6

# constant definitions
FCM2_ADDR = 0x38
FAN_CMD = 0
TEMP_CMD = 1
VOLT_CMD = 2
SPEEDS_CMD = 3
TEMPS_CMD = 4

# actual program
start_l:
read FCM2_ADDR, FAN_CMD, fan_status
read FCM2_ADDR, TEMP_CMD, temp_status
read FCM2_ADDR, VOLT_CMD, volt_status
read FCM2_ADDR, SPEEDS_CMD, fan_speeds
read FCM2_ADDR, TEMPS_CMD, temps
syncio
wait_ms 1
goto start_l
```

### Two redundant PMBus-compatible PSUs readout

``` bash
# parameter definitions
OWORDS = 18
IWORDS = 0

# register definitions
v1: out 0, 2
i1: out 2, 2
t1: out 4, 2
p1: out 6, 2
vmode1: out 8
v2: out 9, 2
i2: out 11, 2
t2: out 13, 2
p2: out 15, 2
vmode2: out 17

# constant definitions
PSU_ADDR1 = 0xB0
PSU_ADDR2 = 0xB2
VOUT_MODE_CMD = 0x20
READ_VOUT_CMD = 0x8B
READ_IOUT_CMD = 0x8C
READ_TEMP_CMD = 0x8D
READ_POUT_CMD = 0x96

# actual program
start_l:
read PSU_ADDR1, VOUT_MODE_CMD, vmode1
read PSU_ADDR1, READ_VOUT_CMD, v1
read PSU_ADDR1, READ_IOUT_CMD, i1
read PSU_ADDR1, READ_TEMP_CMD, t1
read PSU_ADDR1, READ_POUT_CMD, p1
read PSU_ADDR2, VOUT_MODE_CMD, vmode2
read PSU_ADDR2, READ_VOUT_CMD, v2
read PSU_ADDR2, READ_IOUT_CMD, i2
read PSU_ADDR2, READ_TEMP_CMD, t2
read PSU_ADDR2, READ_POUT_CMD, p2
syncio
wait_ms 1
goto start_l
```
