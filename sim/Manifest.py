action = "simulation"
sim_tool = "ghdl"
sim_top = "mouseq_tb"
ghdl_opt = "--std=08 -frelaxed-rules"
target = "igloo2"

modules = {'local': ['../tb']}

incl_makefiles = ['run_makemseq.mk']
