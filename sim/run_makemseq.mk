all: local

work/.mif_target: ../sw/makemseq.py ../sw/coeff_example.mseq
	python3 ../sw/makemseq.py ../sw/coeff_example.mseq ../hdl/mouseq_pkg_gen.vhd ../hdl/mouseq_mem.mif
	cp ../hdl/mouseq_mem.mif .
	@mkdir -p $(dir $@) && touch $@

work/mouseq_pkg_gen/.mouseq_pkg_gen_vhd: work/.mif_target
