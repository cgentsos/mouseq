# Microsemi Tcl Script
# libero
# Date: Thu Apr 25 17:46:32 2019
# Directory /home/chgentso/Projects/fw/diot-wic-gw/hdl/syn/diot_wic_demo_mysyn
# File /home/chgentso/Projects/fw/diot-wic-gw/hdl/syn/diot_wic_demo_mysyn/exported.tcl

# clean build directory
file delete -force ./mouseq_syn

# make the project
new_project -location {./mouseq_syn} -name {mouseq_syn} -instantiate_in_smartdesign 1 -hdl {VHDL} -family {ProASIC3}

create_links \
         -hdl_source {../hdl/i2c/i2c_master_bit_ctrl.vhd} \
         -hdl_source {../hdl/i2c/i2c_master_byte_ctrl.vhd} \
         -hdl_source {../hdl/mouseq_pkg_gen.vhd} \
         -hdl_source {../hdl/mouseq_progmem.vhd} \
         -hdl_source {../hdl/mouseq_sreg_progmem.vhd} \
         -hdl_source {../hdl/mouseq_simple_reg_file.vhd} \
         -hdl_source {../hdl/mouseq_sreg_reg_file.vhd} \
         -hdl_source {../hdl/mouseq_timer.vhd} \
         -hdl_source {../hdl/mouseq.vhd}

set_root -module {mouseq::work}

run_tool -name {SYNTHESIZE}

exec /bin/sed -i {/.*property.*\[/d} mouseq_syn/synthesis/mouseq.edn

# for PA3 this is not available, we have to launch the GUI to export the netlist
# run_tool -name {EXPORTNETLIST}

# run_tool -name {PLACEROUTE}

# run_tool -name {GENERATEPROGRAMMINGDATA}
